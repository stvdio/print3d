//
//  PrintersViewController.m
//  Print3r
//
//  Created by Vaibhav Aggarwal on 2/14/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "PrintersViewController.h"
#import "UITableView+Additions.h"
#import "UIView+Additions.h"
#import "UIView+Layout.h"
#import "DiskManager.h"
#import "PrinterCell.h"
#import "AnalyticsManager.h"
#import "ThemeSettingsViewController.h"
#import "NewPrinterViewController.h"
#import "GCodeVisualizer.h"
#import "CustomDrawingViewController.h"
#import "ParallaxViewController.h"
#import "FileExplorerViewController.h"
#import "LocalNotificationManager.h"
#import "CapabilitiesTableViewController.h"
#import "PrinterProgressTableViewCell.h"
#import "CuraProfileManager.h"
#import <MessageUI/MessageUI.h>
#import "Print3RUtilities.h"
#import "JFMinimalNotification.h"
#import "ControlPannelViewController.h"
#import "AppSettingsViewController.h"

@interface PrintersViewController() <UITableViewDataSource,UITableViewDelegate, UINavigationControllerDelegate, UITextFieldDelegate, AddPrinterViewDelegate,UIGestureRecognizerDelegate>
@property (nonatomic, strong)NSArray *printers;
@property (nonatomic, assign)BOOL isShowingPrintFarm;
@property (nonatomic, strong) UIBarButtonItem *farmButton;
@end

@implementation PrintersViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
  self.navigationController.navigationBar.topItem.title = @"";
  self.navigationController.navigationBar.hidden = NO;
  self.navigationItem.hidesBackButton = YES;

  self.title = @"Thirmensio";
  self.printers = [[DiskManager sharedManager] getAllPrinters];
  [self queryPrintersStatus];
  
  [[AnalyticsManager sharedManager] logEvent:AnalyticEventTypeSeeTutorial withParams:@{@"vaibhav":@"billion"}];
  [self.tableView registerNib:[UINib nibWithNibName:@"PrinterCell" bundle:nil] forCellReuseIdentifier:@"printerCell"];
  [self.tableView registerNib:[UINib nibWithNibName:@"PrinterProgressTableViewCell" bundle:nil] forCellReuseIdentifier:@"printerProgressCell"];
  
  UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                        initWithTarget:self action:@selector(handleLongPress:)];
  lpgr.minimumPressDuration = 2.0; //seconds
  lpgr.delegate = self;
  [self.tableView addGestureRecognizer:lpgr];
  
  UIBarButtonItem *addButton = [[UIBarButtonItem alloc]initWithTitle:@"Add" style:UIBarButtonItemStylePlain target:self action:@selector(addNewPrinter)];
  [addButton setImage:[UIImage imageNamed:@"Plus"]];
  
  self.farmButton = [[UIBarButtonItem alloc]initWithTitle:@"Farm" style:UIBarButtonItemStylePlain target:self action:@selector(showPrintFarm)];
  [self.farmButton setImage:[UIImage imageNamed:@"Details"]];

  
  // create a spacer
  UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:self action:nil];
  space.width = 10;
  
  NSMutableArray *buttons = [[NSMutableArray alloc] initWithArray:@[addButton, space]];
  if(self.printers.count > 0) {
    [buttons addObject:self.farmButton];
  }
  self.navigationItem.rightBarButtonItems = buttons;
  
  UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_more"] style:UIBarButtonItemStylePlain target:self action:@selector(menuPressed)];
  self.navigationItem.leftBarButtonItem = menuButton;
  self.tableView.backgroundColor = [Print3RUtilities darkGreenColor];
  self.tableView.separatorColor = [UIColor clearColor];
   //If there is just one printer, push for that printer
   //user can always come back and add more printers
//    if(self.printers.count == 1) {
//      Printer *printer = [self.printers objectAtIndex:0];
//      FileExplorerViewController *vc = [[FileExplorerViewController alloc] initWithPrinter:printer];
//      [self.navigationController pushViewController:vc animated:NO];
//    }
  
  //  CuraProfileManager *obj = [[CuraProfileManager alloc] init];
  //  NSBundle *bundle = [NSBundle bundleForClass:[self class]];
  //  NSString *filePath = [bundle pathForResource:@"Wall_warp" ofType:@"gcode"];
  //
  //  NSFileManager *defaultManager = [NSFileManager defaultManager];
  //  if ([defaultManager fileExistsAtPath:filePath]) {
  //    NSError *error = NULL;
  //
  //    NSData *data = [NSData dataWithContentsOfFile:filePath options:NSDataReadingUncached error:&error];
  //    NSString* newStr = [NSString stringWithUTF8String:[data bytes]];
  //
  //  NSString *hash = [obj parseCuraProfileStringFromGCode:newStr];
  //
  //    NSString *profile = [obj createProfileFromProfileString:hash];
  //[self yo:profile];
  //}
  
}

- (void)viewWillLayoutSubviews {
  [super viewWillLayoutSubviews];
  
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  self.printers = [[DiskManager sharedManager] getAllPrinters];
  self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
  [self queryPrintersStatus];
  [self.tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:YES];

}

- (void)viewWillDisappear:(BOOL)animated {
  [self.navigationController setNavigationBarHidden:NO];
  [super viewWillDisappear:animated];
}

- (void)menuPressed {
  AppSettingsViewController *vc = [AppSettingsViewController new];
  UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
  [nav
   setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
  
  [self presentViewController:nav animated:YES completion:nil];
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer {
  
  CGPoint p = [gestureRecognizer locationInView:self.tableView];
  
  NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
  if (indexPath == nil) {
    
  } else if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
    if(self.printers.count > indexPath.row) {
      [[DiskManager sharedManager] deletePrinter:[self.printers objectAtIndex:indexPath.row]];
      self.printers = [[DiskManager sharedManager] getAllPrinters];
      if(self.printers.count == 0) {
        NSMutableArray *barButtonItems = [[ NSMutableArray alloc] initWithArray:self.navigationItem.rightBarButtonItems];
        [barButtonItems removeLastObject];
        self.navigationItem.rightBarButtonItems = barButtonItems;
      }
      
      [self.tableView reloadData];
    }
    
  }
}

- (void)queryPrintersStatus {
  
  for(Printer *printer in self.printers) {
    [printer getVersionInfoWithCompletion:^(NSDictionary *version) {
      if(version) {
        printer.isOnline = YES;
      }
      else {
        printer.isOnline = NO;
      }
    }];
  }
  
}

- (void)showPrintFarm {
  
  self.isShowingPrintFarm = !self.isShowingPrintFarm;
  if(self.printers.count < 1) {
    return;
  }
  if(self.isShowingPrintFarm) {
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationRight];
    for (Printer *printer in self.printers) {
      __weak typeof(self) weakSelf = self;
      [printer getPrinterCurrentState:^(PrinterState *state) {
        __strong typeof(self) strongSelf = weakSelf;
        printer.didAskForPrinterState = YES;
        printer.printerState = state;
        if(state.printing || state.paused) {
          [printer getCurrentJobInfo:^(PrinterJobInfo *jobInfo) {
            if(jobInfo) {
              printer.jobInfo = jobInfo;
              // can be optimized for one row
              [strongSelf.tableView reloadData];
            }
          }];
        }
        else {
          [strongSelf.tableView reloadData];
        }
      }];
    }
    
  }
  else {
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationLeft];
  }
  
}

#pragma tableView datasource and delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return self.printers.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
  
  if(self.printers.count < 1) {
    return 60;
  }
  return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
  if(self.printers.count < 1) {
    UIView *headerView = [tableView headerViewForTableView:tableView text:@"You have not added any printers yet \ue058\nClick above to add a new printer" height:60];
    headerView.backgroundColor = [UIColor grayColor];
    return headerView;
  }
  return nil;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
  return [[UIView alloc] initWithFrame:CGRectZero];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
  return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  return 80;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  
  PrinterCell *cell;
  if(self.isShowingPrintFarm) {
    cell = (PrinterProgressTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"printerProgressCell"];
    if (cell == nil) {
      NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PrinterProgressTableViewCell" owner:self options:nil];
      cell = [nib objectAtIndex:0];
    }
  }
  else {
    cell = (PrinterCell *)[tableView dequeueReusableCellWithIdentifier:@"printerCell"];
    if (cell == nil) {
      NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PrinterCell" owner:self options:nil];
      cell = [nib objectAtIndex:0];
    }
  }
  
  [cell configForPrinter:(Printer *)[self.printers objectAtIndex:indexPath.row]];
  
  return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  [tableView deselectRowAtIndexPath:indexPath animated:NO];
  Printer *printer = [self.printers objectAtIndex:indexPath.row];
  
  if(!printer.isOnline && ![printer isDemoPrinter]) {
    [Print3RUtilities showAlertWithTitle:kErrorTitle message:@"This printer is currently offline" sender:nil];
    //return;
  }
  
  ControlPannelViewController *controlPannelVC = [[ControlPannelViewController alloc] initWithPrinter:printer];
  [self.navigationController pushViewController:controlPannelVC animated:YES];
  
}


- (void)addNewPrinter {
  
  NewPrinterViewController *vc = [[NewPrinterViewController alloc] initWithPrinter:nil];
  vc.addPrintViewDelegate = self;
  UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
  [nav
   setModalTransitionStyle:UIModalTransitionStyleCoverVertical];

  [self presentViewController:nav animated:YES completion:nil];
  
}


#pragma AddPrinterViewDelegate


- (void)savePrinter:(Printer *)printer {
  BOOL didSave = [[DiskManager sharedManager] savePrinter:printer];
  if(!didSave) {
    [Print3RUtilities showAlertWithTitle:kErrorTitle message:@"A printer with the same host already exists" sender:self];
  }
  
  self.printers = [[DiskManager sharedManager] getAllPrinters];
  [self queryPrintersStatus];
  NSArray *barButtonItems = self.navigationItem.rightBarButtonItems;

  if(self.printers.count == 1 && barButtonItems.count == 2) {
    NSMutableArray *newBarButtonItems = [[NSMutableArray alloc] initWithArray:barButtonItems];
    [newBarButtonItems addObject:self.farmButton];
    self.navigationItem.rightBarButtonItems = newBarButtonItems;
  }
  [self.tableView reloadData];
}

@end
