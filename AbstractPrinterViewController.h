//
//  AbstractPrinterViewController.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 6/6/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Printer.h"

@interface AbstractPrinterViewController : UIViewController

@property (nonatomic, strong)Printer *printer;

- (instancetype)initWithPrinter:(Printer *)printer;

@end
