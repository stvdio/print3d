//
//  GCodeParserTest.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/26/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "GCodeParser.h"

@interface GCodeParser()

@end

@interface GCodeParserTest : XCTestCase

@end

@implementation GCodeParserTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (NSArray *)readMockData {
  NSBundle *bundle = [NSBundle bundleForClass:[self class]];
  NSString *filePath = [bundle pathForResource:@"Wall_warp" ofType:@"gcode"];
  
  NSFileManager *defaultManager = [NSFileManager defaultManager];
  NSArray *stringArray;
  if ([defaultManager fileExistsAtPath:filePath]) {
    NSError *error = NULL;
    
    NSData *data = [NSData dataWithContentsOfFile:filePath options:NSDataReadingUncached error:&error];
    NSString* newStr = [NSString stringWithUTF8String:[data bytes]];
    stringArray = [newStr componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];

  } else {
    XCTAssert(NO);
  }
  return stringArray;
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
  NSArray *stringArray = [self readMockData];
  [GCodeParser parseGCodeFromStringArray:stringArray];
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
