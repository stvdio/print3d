//
//  AbstractPrinterViewController.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 6/6/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "AbstractPrinterViewController.h"

@interface AbstractPrinterViewController ()

@end

@implementation AbstractPrinterViewController

- (instancetype)initWithPrinter:(Printer *)printer {
  self = [super init];
  if(self) {
    self.printer = printer;
  }
  return self;
}
- (void)viewDidLoad {
  [super viewDidLoad];
  self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
  

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
