//
//  PrinterCell.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/14/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Printer.h"

@interface PrinterCell : UITableViewCell

- (void)configForPrinter:(Printer *)printer;

@end
