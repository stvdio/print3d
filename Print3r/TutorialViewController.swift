//
//  TutorialViewController.swift
//  Print3D
//
//  Created by Vaibhav Aggarwal on 6/20/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

import Foundation

class TutorialViewController : UIViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = UIColor.whiteColor()
    let cancel = UIBarButtonItem(image: UIImage(named: "Delete"), style: UIBarButtonItemStyle.Plain, target: self, action: "cancelPressed")
    navigationItem.leftBarButtonItem = cancel
    
    addTutorial()
    
  }
  
  func cancelPressed() {
      dismissViewControllerAnimated(true, completion: nil)
  }
  
  func addTutorial() {
    
    let page1 = EAIntroPage()
    page1.title = "Hello world"
    page1.desc = "Sample Description"
    page1.bgColor = Print3RUtilities.defaultThemeColor()
    //page1.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title1"]];
    page1.titleIconView = UIImageView(image: UIImage(named: "title1"))
    
    let page2 = EAIntroPage()
    page2.title = "This is page 2"
    page2.titlePositionY = self.view.bounds.size.height/2 - 10;
    page2.desc = "Sample Description"
    page2.descPositionY = self.view.bounds.size.height/2 - 50;
    //page2.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title2"]];
    page2.titleIconPositionY = 70;
    page2.bgColor = Print3RUtilities.defaultOrangeColor()

    let page3 = EAIntroPage()
    page3.title = "This is page 3"
    page3.titleFont = UIFont(name: "Georgia-BoldItalic", size: 20.0)
    page3.titlePositionY = 220;
    page3.desc = "Sample Description"
    page3.descFont = UIFont(name: "Georgia-Italic", size: 20.0)
    page3.descPositionY = 200;
    //page3.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title3"]];
    page3.titleIconPositionY = 100;
    page3.bgColor = Print3RUtilities.defaultThemeColor()

    let page4 = EAIntroPage()
    page4.title = "This is page 4"
    page4.desc = "Sample Description"
    //pa bge4.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title4"]];
    page4.bgColor = Print3RUtilities.defaultYellowColor()

    
    let intro = EAIntroView(frame: view.bounds, andPages: [page1,page2,page3,page4])
    
    //intro.bgImage = [UIImage imageNamed:@"bg2"];
    intro.swipeToExit = false
    intro.pageControlY = 20
    intro.skipButton.hidden = true
    //[intro setDelegate:self];
    intro.showInView(view, animateDuration: 0.0)
  }
}