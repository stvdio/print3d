//
//  Printer.m
//  Print3r
//
//  Created by Vaibhav Aggarwal on 2/13/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "Printer.h"
#import "NetworkManager.h"
#import "Print3RUtilities.h"

static NSString * const kPrinterAlreadyPrintingOrNotWorking = @"Looks like printer is already printing or is not operational";
static NSString * const kNoCurrentActiveJob = @"There are no current active printing job";
static NSString * const kCouldNotUpdateFile = @"The File could not be updated";
static NSString * const kResourceConflict = @"There is a conflict as printer is accessing files or there is an active print";


@implementation Printer 

- (Printer *)initWithData:(NSDictionary *)data {
  self = [super init];
  if(self) {
    if([data objectForKey:kHostKey]) {
      _hostURL = [data objectForKey:kHostKey];
    }
    if([data objectForKey:kNickNameKey]) {
      _nickName = [data objectForKey:kNickNameKey];
    }
    if([data objectForKey:kApiKey]) {
      _apiKey = [data objectForKey:kApiKey];
    }
    if([data objectForKey:kImageKey]) {
      _image = [data objectForKey:kImageKey];
    }
    if([data objectForKey:kLengthKey]) {
      _length = [data objectForKey:kLengthKey];
    }
    if([data objectForKey:kBreadthKey]) {
      _breadth = [data objectForKey:kBreadthKey];
    }
  }
  return self;
}

- (instancetype)initDemoPrinter {
  self = [super init];
  if (self) {
    _hostURL = kDemoPrinterHost;
    _nickName = kDemoPrinterNickName;
    _apiKey = kDemoPrinterHost;
  }
  return self;
}

- (BOOL)isDemoPrinter {
  if ([self.hostURL isEqualToString:kDemoPrinterHost] && [self.nickName isEqualToString:kDemoPrinterNickName]) {
    return YES;
  }
  return NO;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
  if(_hostURL) {
    [aCoder encodeObject:_hostURL forKey:kHostKey];
  }
  if(_nickName) {
    [aCoder encodeObject:_nickName forKey:kNickNameKey];
  }
  if(_apiKey) {
    [aCoder encodeObject:_apiKey forKey:kApiKey];
  }
  if(_image) {
    [aCoder encodeObject:_image forKey:kImageKey];
  }
  if(_length && ![_length isEqual:[NSNull null]]) {
    [aCoder encodeObject:_length forKey:kLengthKey];
  }
  if(_breadth && ![_breadth isEqual:[NSNull null]]) {
    [aCoder encodeObject:_breadth forKey:kBreadthKey];
  }
}
- (id)initWithCoder:(NSCoder *)aDecoder {
  self = [super init];
  if(self){
    _hostURL = [aDecoder decodeObjectForKey:kHostKey];
    _nickName = [aDecoder decodeObjectForKey:kNickNameKey];
    _apiKey = [aDecoder decodeObjectForKey:kApiKey];
    _image = [aDecoder decodeObjectForKey:kImageKey];
    _length = [aDecoder decodeObjectForKey:kLengthKey];
    _breadth = [aDecoder decodeObjectForKey:kBreadthKey];
  }
  return self;
}

- (void)getVersionInfoWithCompletion:(void (^)(NSDictionary *version))completion {
  
  [[NetworkManager sharedManager]updateManagerForPrinter:self];
  NSString *url = @"/version";
  [[NetworkManager sharedManager] performGETOperationForPath:url params:nil success:^(id responseObject){
    if(completion) {
      completion((NSDictionary *)responseObject);
    }
    
  } failure:^(NSError *error, AFHTTPRequestOperation *operation){
    if(completion) {
      completion(nil);
    }
  }];
}

- (void)returnDemoFileWithCompletion:(void (^) (NSArray *files))completion{
  PrinterFile *demoFile = [[PrinterFile alloc] initDemoFile];
  if(completion) {
    completion(@[demoFile]);
  }
}

- (void)getFilesWithLocation:(NSString *)location completion:(void (^)(NSArray *files))completion {
  
  if ([self isDemoPrinter]) {
    [self returnDemoFileWithCompletion:completion];
    return;
  }
  
  [[NetworkManager sharedManager]updateManagerForPrinter:self];
  NSString *url = @"/files";
  if(location) {
    url = [NSString stringWithFormat:@"%@/%@",url,location];
  }
  [[NetworkManager sharedManager] performGETOperationForPath:url params:nil success:^(id responseObject){
    NSMutableArray *printerFiles = [[NSMutableArray alloc] init];
    if(responseObject[@"files"]) {
      for(NSDictionary *file in responseObject[@"files"]) {
        PrinterFile *printerFile = [[PrinterFile alloc] initWithData:file];
        if(printerFile) {
          [printerFiles addObject:printerFile];
        }
      }
    }
    if(completion) {
      completion(printerFiles);
    }
  } failure:^(NSError *error, AFHTTPRequestOperation *operation){
    if(completion) {
      completion(nil);
    }

    }];
}

- (void)postCommandWithParams:(NSDictionary *)dict {
  [[NetworkManager sharedManager]updateManagerForPrinter:self];
  NSString *url = @"/printer/command";
  [[NetworkManager sharedManager] performPOSTOperationForPath:url params:dict success:^(id responseObject){
    NSLog(@"%@",responseObject);
  } failure:^(NSError *error, AFHTTPRequestOperation *operation){
    NSLog(@"%@",error);
  }];
  
}

- (void)postToolCommandWithParams:(NSDictionary *)dict {
  [[NetworkManager sharedManager]updateManagerForPrinter:self];
  NSString *url = @"/printer/tool";
  [[NetworkManager sharedManager] performPOSTOperationForPath:url params:dict success:^(id responseObject){
    [Print3RUtilities showAlertWithTitle:kSuccessTitle message:@"Done" sender:self];
  } failure:^(NSError *error, AFHTTPRequestOperation *operation){
    [Print3RUtilities showAlertWithTitle:kErrorTitle message:@"There was an error." sender:nil];
  }];
}

- (void)printFile:(PrinterFile *)printerFile completion:(void (^)(void))completion {
  [[NetworkManager sharedManager] updateManagerForPrinter:self];
  NSString *url = [NSString stringWithFormat:@"/files/%@/%@",printerFile.origin,printerFile.name];
  NSDictionary *params = @{
                           @"command":@"select",
                           @"print" : @true
                           };
  if (!printerFile.origin || !printerFile.name) {
    return;
  }
  [[NetworkManager sharedManager] performPOSTOperationForPath:url params:params success:^(id responseObject) {
  // success - 204 No content - Printer has started printing the requested file
    if(completion) {
      completion();
    }
  } failure:^(NSError *error, AFHTTPRequestOperation *operation) {
      // Printer is not operational
    if(operation.response.statusCode == 409) {
      [Print3RUtilities showAlertWithTitle:kErrorTitle message:@"Printer is not operational" sender:nil];
    }
    else {
    // 404, file not found, 400 bad request
    [Print3RUtilities showAlertWithTitle:kErrorTitle message:@"Unexpected error ocurred with printer" sender:nil];
    }
    if(completion) {
      completion();
    }
  }];
}

- (void)deleteFile:(PrinterFile *)printerFile success:(void (^)(void))success failure:(void (^)(void))failure {
  [[NetworkManager sharedManager] updateManagerForPrinter:self];
  if (!printerFile.origin) {
    return;
  }
  NSString *url = [NSString stringWithFormat:@"/files/%@/%@",printerFile.origin,printerFile.name];
  NSDictionary *params = @{
                           @"command":@"select",
                           @"print" : @true
                           };
  
  [[NetworkManager sharedManager] performDeleteOperationForPath:url params:params success:^(id responseObject) {
    // success - 204 No content - File successfully deleted
    if(success) {
      success();
    }
    
  } failure:^(NSError *error, AFHTTPRequestOperation *operation) {
    // Requested file is being printed
    if(operation.response.statusCode == 409) {
      [Print3RUtilities showAlertWithTitle:kErrorTitle message:[NSString stringWithFormat:@"%@ is currently being printed",[printerFile.name stringByDeletingPathExtension]] sender:nil];

    }
    else {
      [Print3RUtilities showAlertWithTitle:kErrorTitle message:@"Unexpected error ocurred with printer" sender:nil];
    }
    // 404, file not found, 400 bad request
    if(failure) {
      failure();
    }
  }];

}

- (void)getConnectionSettingsWithCompletion:(void (^) (PrinterConnectionOptions *connectionOptions))completion {
  [[NetworkManager sharedManager] updateManagerForPrinter:self];
  NSString *url = [NSString stringWithFormat:@"/connection"];
  
  [[NetworkManager sharedManager] performGETOperationForPath:url params:nil success:^(id responseObject) {
  
    PrinterConnectionOptions *connectionOptions = [[PrinterConnectionOptions alloc] initWithData:responseObject];
    if(completion) {
      completion(connectionOptions);
    }
  } failure:^(NSError *error, AFHTTPRequestOperation *operation) {
    
  }];

}

- (void)postConnectionCommandWithParams:(NSDictionary *)params {
  [[NetworkManager sharedManager] updateManagerForPrinter:self];
  
  NSString *url = @"/connection";
  
  [[NetworkManager sharedManager] performPOSTOperationForPath:url params:params success:^(id responseObject) {
  
  } failure:^(NSError *error, AFHTTPRequestOperation *operation){
  
  }];
}

- (void)postPrinterHeadCommandWithParams:(NSDictionary *)params {
  [[NetworkManager sharedManager] updateManagerForPrinter:self];
  
  NSString *url = @"/printer/printhead";
  
  //TODO: check the x,y,z values if present and make sure they never exceed the max allowed movement values
  // to avoid potential colission
  [[NetworkManager sharedManager] performPOSTOperationForPath:url params:params success:^(id responseObject) {
    
  } failure:^(NSError *error, AFHTTPRequestOperation *operation){
    // Printer is not operational or currently printing
    if(operation.response.statusCode == 409) {
      [Print3RUtilities showAlertWithTitle:kErrorTitle message:kPrinterAlreadyPrintingOrNotWorking sender:nil];
    }
  }];
}

+ (NSString *)valueForPrintJobType:(PrinterJobType)type {
  switch (type) {
    case PrinterJobTypeStart:
      return @"start";
      break;
    case PrinterJobTypePause:
      return @"pause";
      break;
    case PrinterJobTypeRestart:
      return @"restart";
      break;
    case PrinterJobTypeCancel:
      return @"cancel";
      break;
    default:
      return @"";
  }
}
- (void)postPrinterJobCommandWithType:(PrinterJobType)type {
  [[NetworkManager sharedManager] updateManagerForPrinter:self];
  
  NSString *url = @"/job";
  NSDictionary *params = @{@"command":[Printer valueForPrintJobType:type]};
  
  NSString *errorMessage = @"";
  switch (type) {
    case PrinterJobTypeStart:
      errorMessage = @"Another Print Job is Already Active.";
      break;
    case PrinterJobTypePause:
    case PrinterJobTypeCancel:
      errorMessage = @"No Current Active Print Job.";
      break;
    case PrinterJobTypeRestart:
      errorMessage = @"No Current Active Print Job or Active Print Job is Not Paused.";
      break;
    default:
      break;
  }
  
  [[NetworkManager sharedManager] performPOSTOperationForPath:url params:params success:^(id responseObject) {
    
  } failure:^(NSError *error, AFHTTPRequestOperation *operation){
    // Printer is not operational or currently printing
    if(operation.response.statusCode == 409) {
      [Print3RUtilities showAlertWithTitle:kErrorTitle message:kPrinterAlreadyPrintingOrNotWorking sender:nil];
    }
  }];
}


- (void)getCurrentJobInfo:(void (^)(PrinterJobInfo *))completion {
  [[NetworkManager sharedManager] updateManagerForPrinter:self];
  NSString *url = @"/job";
  
  [[NetworkManager sharedManager] performGETOperationForPath:url params:nil success:^(id responseObject) {
    
    PrinterJobInfo *jobInfo = [[PrinterJobInfo alloc] initWithData:responseObject];
    if(completion) {
      completion(jobInfo);
    }
    
  } failure:^(NSError *error, AFHTTPRequestOperation *operation){
    // no current active job
    [Print3RUtilities showAlertWithTitle:kErrorTitle message:kNoCurrentActiveJob sender:nil];

  }];
  
}

- (void)uploadFileWithData:(NSData *)fileData toPrinterDirectory:(NSString *)printerDirectory withName:(NSString *)withName withCompletion:(void (^)(void))completion {
  
  NSAssert(fileData, @"File data must be present");
  [[NetworkManager sharedManager] updateManagerForPrinter:self];
  NSString *url = [NSString stringWithFormat:@"/files/%@",printerDirectory];
  
  [[NetworkManager sharedManager] uploadFileWithData:fileData atPath:url withName:withName success:^(id responseObject) {
    NSDictionary *data = (NSDictionary *)responseObject;
    if([data valueForKeyPath:[NSString stringWithFormat:@"files.%@",printerDirectory]]) {
      //  PrinterFile *file = [[PrinterFile alloc] initWithData:[data valueForKeyPath:[NSString stringWithFormat:@"files.%@",printerDirectory]]];
      if(completion) {
        completion();
      }
    }
    
  } failure:^(NSError *error, AFHTTPRequestOperation *operation) {
    // TODO: failureBlock
    if(operation.response.statusCode == 409) {
      [Print3RUtilities showAlertWithTitle:kErrorTitle message:kResourceConflict sender:nil];
    }
    else {
      [Print3RUtilities showAlertWithTitle:kErrorTitle message:kCouldNotUpdateFile sender:nil];
    }
    if(completion) {
      completion();
    }
  }];
}

- (void)uploadFileAtPath:(NSString *)filePath toPrinterDirectory:(NSString *)printerDirectory withName:(NSString *)name withCompletion:(void (^)(void))completion {
  [[NetworkManager sharedManager] updateManagerForPrinter:self];
   NSString *url = [NSString stringWithFormat:@"/files/%@",printerDirectory];
  
  [[NetworkManager sharedManager] uploadFile:filePath atPath:url WithName:name success:^(id responseObject) {
    
    NSDictionary *data = (NSDictionary *)responseObject;
    if([data valueForKeyPath:[NSString stringWithFormat:@"files.%@",printerDirectory]]) {
      PrinterFile *file = [[PrinterFile alloc] initWithData:[data valueForKeyPath:[NSString stringWithFormat:@"files.%@",printerDirectory]]];
      
      // TODO: completionBlock
      [self printFile:file completion:nil];

    }
    
  } failure:^(NSError *error, AFHTTPRequestOperation *operation) {
    // TODO: failureBlock
    
  }];
  
}

- (void)getPrinterCurrentState:(void (^)(PrinterState *state))completion {
  [[NetworkManager sharedManager] updateManagerForPrinter:self];
  
  NSString *url = @"/printer";
  [[NetworkManager sharedManager] performGETOperationForPath:url params:nil success:^(id responseObject){
    PrinterState *printerState = [[PrinterState alloc] initWithData:responseObject];
    if(completion) {
      completion(printerState);
    }
    
  } failure:^(NSError *error, AFHTTPRequestOperation *operation){
    if(completion) {
      completion(nil);
    }
  }];

}

@end
