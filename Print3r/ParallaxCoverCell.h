//
//  ParallaxCoverCell.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 3/8/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kParallaxCoverCellIdent @"ParallaxCoverCellIdent"

@interface ParallaxCoverCell : UICollectionViewCell

@property (nonatomic, assign) CGFloat parallaxAmount;
@property (nonatomic, strong) NSArray *imageViews;

- (void)setParallaxPosition:(CGFloat)position;

@end
