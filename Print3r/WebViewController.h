//
//  WebViewController.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 4/16/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController

- (instancetype)initWithURL:(NSString *)URL;

@end
