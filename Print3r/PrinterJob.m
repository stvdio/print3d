//
//  PrinterJob.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/19/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "PrinterJob.h"

@implementation PrinterJob

-(PrinterJob *)initWithData:(NSDictionary *)data {
  
  self = [super init];
  if(self) {
    if(data[@"file"]) {
      self.file = [[PrinterFile alloc] initWithData:data[@"file"]];
    }
    if(data[@"estimatedPrintTime"]) {
      self.estimatedPrintTime = [data valueForKey:@"estimatedPrintTime"];
    }
    if(data[@"filament"]) {
      self.filament = [[Filament alloc] initWithData:data[@"filament"]];
    }
  }
  return self;
}

- (instancetype)initDemoPrinterJob {
  self = [super init];
  if (self) {
    _file = [[PrinterFile alloc] initDemoFile];
    _estimatedPrintTime = [NSNumber numberWithInteger:4000];
  }
  return self;
}
- (NSString *)humanReadableEstimatedPrintTime {
  NSString *time = @" -";
  if(self.estimatedPrintTime) {
    NSUInteger h, m, s;
    h = ([self.estimatedPrintTime intValue] / 3600);
    m = ((NSUInteger)([self.estimatedPrintTime intValue] / 60)) % 60;
    s = ((NSUInteger) [self.estimatedPrintTime intValue]) % 60;
    time = [NSString stringWithFormat:@" %lu:%02lu:%02lu", (unsigned long)h, (unsigned long)m, (unsigned long)s];
  }
  return time;
}
@end
