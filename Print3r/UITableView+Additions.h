//
//  UITableView+Additions.h
//  Print3r
//
//  Created by Vaibhav Aggarwal on 2/14/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (Additions)
- (UIView *)headerViewForTableView:(UITableView *)tableView text:(NSString *)text height:(CGFloat)height;
@end
