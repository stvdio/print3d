//
//  ViewController.m
//  Print3r
//
//  Created by Vaibhav Aggarwal on 2/13/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "WelcomeViewController.h"
#import "UIView+Additions.h"
#import "UserDefaultsManager.h"
#import "UIColor+HexColors.h"
#import "Print3RUtilities.h"

@interface WelcomeViewController () <UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *captionLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *captionYConstraint;
@property (strong, nonatomic) UIImageView *background;
@end

@implementation WelcomeViewController


- (void)viewDidLoad {
  [super viewDidLoad];
  self.navigationController.navigationBar.hidden = YES;
  self.captionLabel.hidden = YES;
  self.titleLabel.font = [Print3RUtilities themeBoldFontWithSize:29.0f];
  [self performSelector:@selector(animateComponents) withObject:nil afterDelay:0.5];
}

- (void)viewWillLayoutSubviews {
  [super viewWillLayoutSubviews];
  self.background.frame = self.view.frame;

}
- (void)animateComponents {
  
  [self.captionLabel animateToCurrentConstraint:self.captionYConstraint WithDuration:1.0f completion:^{
    self.background = [[UIImageView alloc] initWithFrame:CGRectZero];
    self.background.image = [UIImage imageNamed:@"bkg"];
    [self.view addSubview:self.background];
    [self.view sendSubviewToBack:self.background];
    self.background.center = self.view.center;
    self.background.backgroundColor = [Print3RUtilities defaultThemeColor];
    self.background.alpha = 0.0;
    
    [UIView animateWithDuration:0.8f delay:0.0 usingSpringWithDamping:1.0 initialSpringVelocity:1.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
      self.background.frame = self.view.frame;
      self.background.alpha = 1.0;
    } completion:^(BOOL completed){
      
      if([[UserDefaultsManager sharedManager] boolValueForKey:kHasSeenWelcomeScreen]) {
        PrintersViewController *vc = [[PrintersViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
      }
      else {
        [self performSegueWithIdentifier:@"getStartedSegue" sender:nil];
      }
    }];
    
  }];
  
  
}

- (UIInterfaceOrientationMask)navigationControllerSupportedInterfaceOrientations:(UINavigationController *)navigationController {
  return UIInterfaceOrientationMaskPortrait;

}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
}

@end
