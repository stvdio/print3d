//
//  PrinterVersion.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 4/12/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PrinterVersion : NSObject

@property (strong, nonatomic)NSString *apiVersion;
@property (strong, nonatomic)NSString *serverVersion;

- (instancetype)initWithData:(NSDictionary *)data;

@end

