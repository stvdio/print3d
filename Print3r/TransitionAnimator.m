//
//  CircleTransitionAnimator.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/23/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "TransitionAnimator.h"
#import "UIView+Layout.h"

@interface TransitionAnimator ()
@property (nonatomic, weak) id <UIViewControllerContextTransitioning> transitionContext;
@end

@implementation TransitionAnimator 

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
  return 0.5f;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
  self.transitionContext = transitionContext;
  UIView *containerView = [transitionContext containerView];
  
  UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
  UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];

  if(self.isPush) {
    [self presentAnimationFromView:fromVC toView:toVC container:containerView context:transitionContext];
  }
  else {
    [self dismissAnimationFromView:fromVC toView:toVC container:containerView context:transitionContext];
  }
  
}

- (void)dismissAnimationFromView:(UIViewController *)fromView toView:(UIViewController *)toVC container:(UIView *)containerView context:(id<UIViewControllerContextTransitioning>)transitionContext {
 
  
  UIView *toSnapShot = [toVC.view snapshotViewAfterScreenUpdates:YES];
  [containerView addSubview:toSnapShot];
  
  UIView *snapShot = [fromView.view snapshotViewAfterScreenUpdates:YES];
  [containerView addSubview:snapShot];
  
  [UIView animateWithDuration:0.4 delay:0.0 usingSpringWithDamping:0.5 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
    snapShot.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.6, 0.6);

  } completion:^(BOOL finished) {
    
    [UIView animateWithDuration:0.5 delay:0.0 usingSpringWithDamping:0.5 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
      snapShot.x += containerView.width;

    } completion:^(BOOL completed) {
      [snapShot removeFromSuperview];
      [containerView addSubview:toVC.view];
      [transitionContext completeTransition:completed];
    }];
  }];
  

}

- (void)presentAnimationFromView:(UIViewController *)fromView toView:(UIViewController *)toVC container:(UIView *)containerView context:(id<UIViewControllerContextTransitioning>)transitionContext {
  
  UIView *snapShot = [toVC.view snapshotViewAfterScreenUpdates:YES];
  snapShot.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.6, 0.6);
  snapShot.x += containerView.width;
  [containerView addSubview:snapShot];
  
  [UIView animateWithDuration:0.3 animations:^{
    snapShot.x -=containerView.width;
    
  } completion:^(BOOL finished) {
    
    [UIView animateWithDuration:0.8 delay:0.0 usingSpringWithDamping:0.5 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
      snapShot.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
      
    } completion:^(BOOL completed) {
      [snapShot removeFromSuperview];
      [containerView addSubview:toVC.view];
      [transitionContext completeTransition:completed];
    }];
  }];

}

@end
