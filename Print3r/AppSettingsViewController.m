//
//  AppSettingsViewController.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 6/14/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "AppSettingsViewController.h"
#import <MessageUI/MessageUI.h>
#import "Print3RUtilities.h"
#import "AboutUsViewController.h"
#import "GetStartedViewController.h"
#import "Print3D-Swift.h"
#import "Printer.h"
#import "DiskManager.h"

static NSString * const kEmailSubject = @"Thermensio Feedback";
static NSString * const kEmailRecipient = @"vaibhavaggarwal@live.in";
static NSString * const kFeaturesEmailSubject = @"Request Features";

@interface AppSettingsViewController () <MFMailComposeViewControllerDelegate>

@end

@implementation AppSettingsViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Delete"] style:UIBarButtonItemStylePlain target:self action:@selector(cancelPressed)];
  self.navigationItem.leftBarButtonItem = cancel;
  [self.tableView setSeparatorColor:[Print3RUtilities lighterGreenColor]];
  self.tableView.backgroundColor = [Print3RUtilities darkGreenColor];
  if([self.tableView respondsToSelector:@selector(setCellLayoutMarginsFollowReadableWidth:)])
  {
   self.tableView.cellLayoutMarginsFollowReadableWidth = NO;
  }
  self.title = @"Settings";
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void)cancelPressed {
  [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return 7;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  if (indexPath.row == 4) {
    return 0;
  }
  return 80.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"settingsCell"];
  if(!cell) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"settingsCell"];
  }
  switch (indexPath.row) {
    case 0:
      cell.textLabel.text = @"About us";
      break;
    case 1:
      cell.textLabel.text = @"Contact us";
      break;
    case 2:
      cell.textLabel.text = @"Tutorial";
      break;
    case 3:
      cell.textLabel.text = @"Request Features";
      break;
    case 4:
      cell.textLabel.text = @"Rate us";
      break;
    case 5:
      cell.textLabel.text = @"Buy us Beer!";
      break;
    case 6:
      cell.textLabel.text = @"Add Demo Printer";
      break;
    default:
      break;
  }

  cell.backgroundColor = [Print3RUtilities darkGreenColor];
  cell.textLabel.textColor = [UIColor whiteColor];
  UIView * selectedBackgroundView = [[UIView alloc] init];
  [selectedBackgroundView setBackgroundColor:[Print3RUtilities lighterGreenColor]];
  [cell setSelectedBackgroundView:selectedBackgroundView];
  return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  [tableView deselectRowAtIndexPath:indexPath animated:YES];
  switch (indexPath.row) {
    case 0:
      [self aboutUsClicked];
      break;
    case 1:
      [self contactUsClicked];
      break;
    case 2:
      [self tutorailClicked];
      break;
    case 3:
      [self requestFeaturesClicked];
      break;
    case 4:
      break;
    case 5:
      [self buyUsBeerClicked];
      break;
    case 6:
      [self addDemoPrinter];
      break;
    default:
      break;
  }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)contactUsClicked {
  
  if(![MFMailComposeViewController canSendMail]) {
    [Print3RUtilities showAlertWithTitle:kErrorTitle message:@"Your device is not configured to send emails!" sender:self];
    return;
  }
  
  MFMailComposeViewController *mailVc = [[MFMailComposeViewController alloc] init];
  mailVc.mailComposeDelegate = self;
  [mailVc setSubject:kEmailSubject];
  [mailVc setToRecipients:@[kEmailRecipient]];
  [mailVc setMessageBody:@"" isHTML:NO];
  [self presentViewController:mailVc animated:YES completion:nil];
}

- (void)aboutUsClicked {
  AboutUsViewController *vc = [AboutUsViewController new];
  [self.navigationController pushViewController:vc animated:YES];
  
}

- (void)tutorailClicked {
  TutorialViewController *vc = [TutorialViewController new];
  UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
  [nav setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
  [self presentViewController:nav animated:YES completion:nil];
}

- (void)requestFeaturesClicked {
  if(![MFMailComposeViewController canSendMail]) {
    [Print3RUtilities showAlertWithTitle:kErrorTitle message:@"Your device is not configured to send emails!" sender:self];
    return;
  }
  
  MFMailComposeViewController *mailVc = [[MFMailComposeViewController alloc] init];
  mailVc.mailComposeDelegate = self;
  [mailVc setSubject:kFeaturesEmailSubject];
  [mailVc setToRecipients:@[kEmailRecipient]];
  [mailVc setMessageBody:@"" isHTML:NO];
  [self presentViewController:mailVc animated:YES completion:nil];
}

- (void)buyUsBeerClicked {
  [Print3RUtilities showAlertWithTitle:@"🍺" message:@"It's just the thought that matters! You owe yourself a beer now." sender:self];
}

- (void)addDemoPrinter {
  Printer *demoPrinter = [[Printer alloc] initDemoPrinter];
  if (demoPrinter) {
   BOOL didAdd = [[DiskManager sharedManager] savePrinter:demoPrinter];
    if (didAdd) {
      [Print3RUtilities showAlertWithTitle:kSuccessTitle message:@"Demo Printer Added Successfully." sender:self];
    }
    else {
      [Print3RUtilities showAlertWithTitle:kErrorTitle message:@"Demo Printer already exists." sender:self];

    }
  }
}
@end
