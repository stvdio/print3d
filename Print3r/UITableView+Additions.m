//
//  UITableView+Additions.m
//  Print3r
//
//  Created by Vaibhav Aggarwal on 2/14/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "UITableView+Additions.h"
#import "UIView+Additions.h"
#import "UIView+Layout.h"


@implementation UITableView (Additions)

- (UIView *)headerViewForTableView:(UITableView *)tableView text:(NSString *)text height:(CGFloat)height{
  UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.width, height)];
  headerView.backgroundColor = [[UIColor orangeColor] colorWithAlphaComponent:0.6];
  UILabel *label = [UILabel new];
  [headerView addSubview:label];
  label.numberOfLines = 0;
  label.text = text;
  label.textColor = [UIColor whiteColor];
  label.size = [label sizeThatFits:CGSizeMake(headerView.width - 16.0f, 999.0f)];
  label.x = 8.0f;
  [label moveToVerticalCenterOfView:headerView];
  return headerView;
}
@end
