//
//  PrinterFile.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/16/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GCodeAnalysis.h"
#import "PrintStats.h"

@interface PrinterFile : NSObject
@property(nonatomic, strong)NSString *name;
@property(nonatomic, strong)NSNumber *size;
@property(nonatomic, strong)NSNumber *date;
@property(nonatomic, strong)NSString *origin;
@property(nonatomic, strong)NSString *resourceLocation;
@property(nonatomic, strong)NSString *downloadLocation;
@property(nonatomic, strong)GCodeAnalysis *gcodeAnalysis;
@property(nonatomic, strong)PrintStats *printStats;

- (PrinterFile *)initWithData:(NSDictionary *)data;
- (instancetype)initDemoFile;
- (NSString *)descriptionForPopUp;
@end
