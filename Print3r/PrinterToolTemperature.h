//
//  PrinterStateTemperature.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 4/12/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PrinterToolTemperature : NSObject

@property (nonatomic, strong) NSString *actual;
@property (nonatomic, strong) NSString *target;
@property (nonatomic, strong) NSString *offset;

- (instancetype)initWithData:(NSDictionary *)data;
- (instancetype)initDemoToolTemperature;
@end
