//
//  MailComposerManager.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 4/10/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "MailComposerManager.h"

@implementation MailComposerManager

static MailComposerManager *sharedManager;

+(instancetype)sharedManager {

  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedManager = [[MailComposerManager alloc] init];
  });
  return sharedManager;
}

- (BOOL)canSendEmail {
  return [MFMailComposeViewController canSendMail];
}

- (MFMailComposeViewController *)mailComposerControllerForSubject:(NSString *)subject body:(NSString *)body {
  
  if([MFMailComposeViewController canSendMail]) {
    MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
    [mailController setSubject:subject];
    [mailController setMessageBody:body isHTML:NO];
    return mailController;
  }
  return nil;
}

- (MFMailComposeViewController *)mailComposerControllerForSubject:(NSString *)subject body:(NSString *)body attachment:(NSData *)attachmentData attachmentName:(NSString *)attachmentName mimeType:(NSString *)mimeType{
  if([MFMailComposeViewController canSendMail]) {
    MFMailComposeViewController *mailController = [self mailComposerControllerForSubject:subject body:body];
    [mailController addAttachmentData:attachmentData mimeType:mimeType fileName:attachmentName];
    return mailController;
  }
  return nil;
}
@end
