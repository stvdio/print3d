//
//  PrintStartingViewController.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 5/3/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "PrintStartingViewController.h"
#import "UIView+Layout.h"
#import "UIColor+HexColors.h"
#import "Print3RUtilities.h"

@interface PrintStartingViewController ()
@property (weak, nonatomic) IBOutlet UIView *progressContainer;
@property (weak, nonatomic) IBOutlet UIView *progressBar;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) UIView *progress;
@property (weak, nonatomic) IBOutlet UILabel *fileName;
@property (weak, nonatomic) IBOutlet UIButton *checkButton;
@property (assign, nonatomic)BOOL didTouchAnyButton;
@property (strong, nonatomic)PrinterFile *file;
@end

@implementation PrintStartingViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
  [self.view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.4f]];
  self.progressContainer.backgroundColor = [Print3RUtilities darkGreenColor];
  self.progressBar.layer.cornerRadius = 2.0f;
  self.progressBar.layer.masksToBounds = YES;
  self.progressBar.clipsToBounds = YES;
  self.progress = [UIView newWithSuperview:self.progressBar];
  [self.progress setBackgroundColor:[Print3RUtilities defaultThemeColor]];
  
  [self.cancelButton addTarget:self action:@selector(cancelPrint) forControlEvents:UIControlEventTouchUpInside];
  [self.checkButton addTarget:self action:@selector(startPrint) forControlEvents:UIControlEventTouchUpInside];
  self.fileName.textColor = [UIColor whiteColor];
}

- (void)viewWillLayoutSubviews {
  [super viewWillLayoutSubviews];
  self.progress.x = 0.0f;
  self.progress.height = self.progressBar.height;
}

- (void)showForFileName:(PrinterFile *)file {
  self.file = file;
  self.fileName.text = [NSString stringWithFormat:@"Starting Print: %@",file.name];
  [self performSelector:@selector(animateProgressForFile:) withObject:file afterDelay:0.5f];
}

- (void)startPrint {
  self.didTouchAnyButton = YES;
  [self removeAndClearOut];
  if([self.delegate respondsToSelector:@selector(printFile:)]) {
    [self.delegate printFile:self.file];
  }

}

- (void)cancelPrint {
  self.didTouchAnyButton = YES;
  [self removeAndClearOut];
}

- (void)removeAndClearOut {
  [self willMoveToParentViewController:nil];
  [[self view] removeFromSuperview];
  [self removeFromParentViewController];
}

- (void)animateProgressForFile:(PrinterFile *)file {
  [UIView animateWithDuration:5.0f animations:^{
    self.progress.width = self.progressBar.width;
  } completion:^(BOOL finished) {
    if(!self.didTouchAnyButton) {
      [self startPrint];
    }
  }];
}


- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}


@end
