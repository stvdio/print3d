//
//  FileExplorerCell.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 3/14/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "FileExplorerCell.h"
#import "PrinterFile.h"

@interface FileExplorerCell()
@property (weak, nonatomic) IBOutlet UILabel *name;

@end

@implementation FileExplorerCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)configForPrinter:(PrinterFile *)file {
  self.name.text = file.name;
  [self.name sizeToFit];
}

- (void)layoutSubviews {
  [super layoutSubviews];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
