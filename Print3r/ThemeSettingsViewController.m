//
//  ThemeSettingsViewController.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/21/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "ThemeSettingsViewController.h"

@interface ThemeSettingsViewController ()
@property (weak, nonatomic) IBOutlet UISlider *colorSlider;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;
- (IBAction)sliderValueChanged:(id)sender;
@property(strong, nonatomic) UIView *colorView;
@end

@implementation ThemeSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
  self.view.backgroundColor = [UIColor blackColor];
  self.colorView = [[UIView alloc] init];
  [self.view addSubview:self.colorView];
  self.colorView.userInteractionEnabled = NO;
  self.colorView.alpha = 0.5;
  self.colorView.backgroundColor = [UIColor clearColor];
  self.valueLabel.textColor = [UIColor whiteColor];
  [self stepSetSlider];
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillLayoutSubviews {
  self.colorView.frame = self.view.frame;
}


- (IBAction)sliderValueChanged:(id)sender {
  
  [self stepSetSlider];
}

- (void)stepSetSlider {
  //self.colorSlider.value = round(self.colorSlider.value);
  
  switch ((int)self.colorSlider.value) {
    case 1:
      self.colorView.backgroundColor = [UIColor redColor];
      self.valueLabel.text = @"red";
      break;
    case 2:
      self.colorView.backgroundColor = [UIColor greenColor];
      self.valueLabel.text = @"green";
      break;
    case 3:
      self.colorView.backgroundColor = [UIColor yellowColor];
      self.valueLabel.text = @"yellow";
      break;
    case 4:
      self.colorView.backgroundColor = [UIColor orangeColor];
      self.valueLabel.text = @"orange";
      break;
    case 5:
      self.colorView.backgroundColor = [UIColor blueColor];
      self.valueLabel.text = @"blue";
      break;
    default:
      break;
  }
  [self.valueLabel sizeToFit];
}
@end
