//
//  GCodeAnalysis.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/16/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Filament.h"

@interface GCodeAnalysis : NSObject
@property(nonatomic, strong)NSNumber *estimatedPrintTime;
@property(nonatomic, strong)Filament *filament;

- (GCodeAnalysis *)initWithData:(NSDictionary *)data;
@end
