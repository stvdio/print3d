//
//  PrinterState.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 4/11/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PrinterToolTemperature.h"
#import "UIColor+HexColors.h"

@interface PrinterState : NSObject

@property (nonatomic, strong) NSString *stateString;
@property (nonatomic, assign) BOOL operational;
@property (nonatomic, assign) BOOL paused;
@property (nonatomic, assign) BOOL printing;
@property (nonatomic, assign) BOOL sdReady;
@property (nonatomic, assign) BOOL error;
@property (nonatomic, assign) BOOL ready;
@property (nonatomic, assign) BOOL closedOrError;
@property (nonatomic, strong) PrinterToolTemperature *tool0;
@property (nonatomic, strong) PrinterToolTemperature *tool1;

- (instancetype)initWithData:(NSDictionary *)data;
- (instancetype)initDemoState;
- (UIColor *)colorForCurrentState;

@end
