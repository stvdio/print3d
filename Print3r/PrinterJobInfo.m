//
//  PrinterJobInfo.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/19/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "PrinterJobInfo.h"

@implementation PrinterJobInfo

- (PrinterJobInfo *)initWithData:(NSDictionary *)data {
  self = [super init];
  
  if(self) {
    if(data[@"job"]) {
      self.job = [[PrinterJob alloc] initWithData:data[@"job"]];
    }
    
    if(data[@"progress"]) {
      self.progress = [[PrinterJobProgress alloc] initWithData:data[@"progress"]];
    }
    
    if(data[@"state"]) {
      self.state = data[@"state"];
    }
  }
  
  return self;
}

- (instancetype)initDemoPrinterJobInfo {
  self = [super init];
  if (self) {
    self.job = [[PrinterJob alloc] initDemoPrinterJob];
    self.progress = [[PrinterJobProgress alloc] initDemoJobProgress];
    self.state = @"Printing";
  }
  return self;
}
@end
