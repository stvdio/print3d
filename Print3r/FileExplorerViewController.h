//
//  FileExplorerViewController.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 3/14/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Printer.h"

@interface FileExplorerViewController : UITableViewController

- (instancetype)initWithPrinter:(Printer *)printer;

@end
