//
//  main.m
//  Print3r
//
//  Created by Vaibhav Aggarwal on 2/13/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
