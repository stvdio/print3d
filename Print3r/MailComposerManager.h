//
//  MailComposerManager.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 4/10/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>

@interface MailComposerManager : NSObject


/*
 * returns a shared manager for creating MFMailComposeViewController objects
 * NOTE - Make sure to set the delegate for the MFMailComposeViewController to receive call backs
 */

+ (instancetype)sharedManager;

/*
 * Specifies weather current device is capable of sending emails or not
 * @return BOOL representing device's capability of sending emails
 */
- (BOOL)canSendEmail;


/*
 * Creates and returns an instance of MFMailComposeViewController with given attributes returns nil if the
 * current device is incapable of sending emails.
 *
 * @param NSString * subject of the email
 * @param NSString * body of the email - ex- [fileString dataUsingEncoding:NSUTF8StringEncoding]
 *
 * @return An instance of MFMailComposeViewController
 */
- (MFMailComposeViewController *)mailComposerControllerForSubject:(NSString *)subject body:(NSString *)body;


/*
 * Creates and returns an instance of MFMailComposeViewController with given attributes and a file attachment, returns nil if the
 * current device is incapable of sending emails.
 * 
 * @param NSString * subject of the email
 * @param NSString * body of the email - ex- [fileString dataUsingEncoding:NSUTF8StringEncoding]
 * @param NSData * attachment data to be attached to the email
 * @param NSString * name of the attachment file
 * @param NSString * mimeType to be used for the attachment
 *
 * @return An instance of MFMailComposeViewController
 */
- (MFMailComposeViewController *)mailComposerControllerForSubject:(NSString *)subject body:(NSString *)body attachment:(NSData *)attachmentData attachmentName:(NSString *)attachmentName mimeType:(NSString *)mimeType;

@end
