//
//  NetworkManager.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/14/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "NetworkManager.h"

@interface NetworkManager()

@property (strong, nonatomic) NSString *baseUrl;
@property (strong, nonatomic) NSString *apiKey;
@property (strong, nonatomic) AFHTTPRequestOperationManager *requestManager;

@end

@implementation NetworkManager

static NetworkManager *sharedManager;

+ (NetworkManager *)sharedManager{
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedManager = [[NetworkManager alloc] init];
  });
  
  return sharedManager;
}

- (instancetype)init{
  self = [super init];
  if(self){
    _requestManager = [AFHTTPRequestOperationManager manager];
    _requestManager.responseSerializer = [AFJSONResponseSerializer serializer];
    _requestManager.requestSerializer = [AFJSONRequestSerializer serializer];
    [_requestManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [_requestManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
  }
  return self;
}

- (void)updateManagerForPrinter:(Printer *)printer {
  self.baseUrl = printer.hostURL;
  self.apiKey = printer.apiKey;
  
}

- (void)setHTTPResponseSerializer:(BOOL)shouldSet {
  if(shouldSet) {
    self.requestManager.responseSerializer = [AFHTTPResponseSerializer serializer];
  }
  else {
    self.requestManager.responseSerializer = [AFJSONResponseSerializer serializer];

  }
}

- (void)downloadFileFor:(NSString *)path
                            params:(NSDictionary *)params
                           success:(ServiceSuccessHandler)completion
                           failure:(ServiceFailureBlock)failure{
  if(!path) {
    return;
  }
  [self setHTTPResponseSerializer:YES];
  __weak NetworkManager *weakSelf = self;
  [self.requestManager GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject){
    __strong NetworkManager *strongSelf = weakSelf;
    [strongSelf setHTTPResponseSerializer:YES];
    if(completion) {
      completion(responseObject);
    }
    [strongSelf setHTTPResponseSerializer:NO];
  }
                   failure:^(AFHTTPRequestOperation *operation, NSError *error){
                     __strong NetworkManager *strongSelf = weakSelf;
                     if(failure) {
                       failure(error, operation);
                     }
                     [strongSelf setHTTPResponseSerializer:NO];
                     
                   }];
  [self setHTTPResponseSerializer:NO];

  
}

- (void)performGETOperationForPath:(NSString *)path
                            params:(NSDictionary *)params
                           success:(ServiceSuccessHandler)completion
                           failure:(ServiceFailureBlock)failure{
  
  NSString *fullRequestPath = [NSString stringWithFormat:@"http://%@/api%@?apikey=%@",self.baseUrl,path,self.apiKey];
  
  [self.requestManager GET:fullRequestPath parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject){
    if(completion) {
    completion(responseObject);
    }
  }
                   failure:^(AFHTTPRequestOperation *operation, NSError *error){
                     if(failure) {
                       failure(error, operation);
                     }
                   }];
  
}

- (void)performPOSTOperationForPath:(NSString *)path
                             params:(NSDictionary *)params
                            success:(ServiceSuccessHandler)completion
                            failure:(ServiceFailureBlock)failure{
  
  NSString *fullRequestPath = [NSString stringWithFormat:@"http://%@/api%@?apikey=%@",self.baseUrl,path,self.apiKey];
  [self.requestManager POST:fullRequestPath parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject){
    if(completion) {
      completion(responseObject);
    }
  }
                    failure:^(AFHTTPRequestOperation *operation, NSError *error){
                      if(failure) {
                        failure(error, operation);
                      }
                    }];
  
}

- (void)clearHeaderForKey:(NSString *)key {
  [self.requestManager.requestSerializer setValue:nil forHTTPHeaderField:key];
}


- (void)performGEToperationForPath:(NSString *)path
                            params:params
                         headerKey:(NSString *)headerKey
                       headerValue:(NSString *)headerValue
                           success:(ServiceSuccessHandler)completion
                           failure:(ServiceFailureBlock)failure{
  [self.requestManager.requestSerializer setValue:headerValue forHTTPHeaderField:headerKey];
  [self performGETOperationForPath:path params:params success:completion failure:failure];
  
}

- (void)performDeleteOperationForPath:(NSString *)path
                               params:(NSDictionary *)params
                              success:(ServiceSuccessHandler)completion
                              failure:(ServiceFailureBlock)failure {
  NSString *fullRequestPath = [NSString stringWithFormat:@"http://%@/api%@?apikey=%@",self.baseUrl,path,self.apiKey];
  [self.requestManager DELETE:fullRequestPath parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
    if(completion) {
      completion(responseObject);
    }
  } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    if(failure) {
      failure(error, operation);
    }
  }];
}

- (void)uploadFileWithData:(NSData *)fileData atPath:(NSString *)path withName:(NSString *)name
           success:(ServiceSuccessHandler)completion
           failure:(ServiceFailureBlock)failure {

  NSString *fullRequestPath = [NSString stringWithFormat:@"http://%@/api%@?apikey=%@",self.baseUrl,path,self.apiKey];
  [self.requestManager POST:fullRequestPath parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData){
    [formData appendPartWithFileData:fileData
                                name:@"file"
                            fileName:name mimeType:@"text/rtf"];
    BOOL trueBool = YES;
    BOOL falseBOOL = FALSE;
    [formData appendPartWithFormData:[NSData dataWithBytes:&falseBOOL length:sizeof(trueBool)]
                                name:@"select"];
    
    [formData appendPartWithFormData:[NSData dataWithBytes:&trueBool length:sizeof(falseBOOL)]
                                name:@"print"];
    
    
  } success:^(AFHTTPRequestOperation *operation, id responseObject) {
    if(completion) {
      completion(responseObject);
    }
    
  } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    if(failure) {
      failure(error, operation);
    }
  }];

  
  
}
- (void)uploadFile:(NSString *)filePath atPath:(NSString *)path WithName:(NSString *)name
                       success:(ServiceSuccessHandler)completion
                       failure:(ServiceFailureBlock)failure {
  
  NSString *fullRequestPath = [NSString stringWithFormat:@"http://%@/api%@",self.baseUrl,path];
  NSError *error;
  NSData *data = [NSData dataWithContentsOfFile:filePath options:NSDataReadingUncached error:&error];
  
  
  [self.requestManager POST:fullRequestPath parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData){
    [formData appendPartWithFileData:data
                                name:@"file"
                            fileName:name mimeType:@"text/rtf"];
    BOOL trueBool = YES;
    BOOL falseBOOL = FALSE;
    [formData appendPartWithFormData:[NSData dataWithBytes:&falseBOOL length:sizeof(trueBool)]
                                name:@"select"];
    
    [formData appendPartWithFormData:[NSData dataWithBytes:&trueBool length:sizeof(falseBOOL)]
                                name:@"print"];


  } success:^(AFHTTPRequestOperation *operation, id responseObject) {
    if(completion) {
      completion(responseObject);
    }
    
  } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    if(failure) {
      failure(error, operation);
    }
  }];
  
}

@end
