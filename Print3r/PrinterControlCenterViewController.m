//
//  DemoViewController.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 4/4/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "PrinterControlCenterViewController.h"
#import "UIView+Layout.h"
#import "ASValueTrackingSlider.h"
#import "Print3RUtilities.h"

static CGFloat const kPadding = 16.0f;

@interface PrinterControlCenterViewController () <UITextFieldDelegate>
@property (strong, nonatomic) UIView *firstContainer;
@property (strong, nonatomic) UIView *secondContainer;
@property (strong, nonatomic) UIView *thirdContainer;
@property (strong, nonatomic) UIView *fourthContianer;
@property (strong, nonatomic) UIView *fifthhContianer;

@property (strong, nonatomic) UIButton *startButton;
@property (strong, nonatomic) UIButton *pauseButton;
@property (strong, nonatomic) UIButton *stopButton;

@property (strong, nonatomic) UILabel *hotEndLabel;
@property (strong, nonatomic) ASValueTrackingSlider *hotEndSlider;
@property (strong, nonatomic) UIButton *hotEndButton;

@property (strong, nonatomic) UILabel *bedLabel;
@property (strong, nonatomic) ASValueTrackingSlider *bedSlider;
@property (strong, nonatomic) UIButton *bedButton;

@property (strong, nonatomic) UILabel *homeLabel;
@property (strong, nonatomic) UIButton *xHomeButton;
@property (strong, nonatomic) UIButton *yHomeButton;
@property (strong, nonatomic) UIButton *zHomeButton;

@property (strong, nonatomic) UILabel *customCommandLabel;
@property (strong, nonatomic) UITextField *customCommandTextField;
@property (strong, nonatomic) UIButton *customCommandButton;

@property (nonatomic, strong) Printer *printer;
@property (nonatomic, strong) UIView *backGround;
@property (nonatomic, strong) UISegmentedControl *segmentedControl;
@property (nonatomic, strong) UIView *temperatureContainer;
@property (nonatomic, strong) UIView *printHeadContainer;

@end

@implementation PrinterControlCenterViewController

- (instancetype)initWithPrinter:(Printer *)printer {
  self = [super init];
  if(self) {
    self.printer = printer;
  }
  return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
  self.backGround = [UIView new];
  self.backGround.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.3f];
  [self.view addSubview:self.backGround];
  [self.view sendSubviewToBack:self.backGround];
  self.view.backgroundColor = [UIColor whiteColor];
  
  
  self.segmentedControl = [[UISegmentedControl alloc] initWithItems:@[@"Temperature", @"Print Head"]];
  self.navigationItem.titleView = self.segmentedControl;
  [self.segmentedControl addTarget:self action:@selector(segmentChanged:) forControlEvents:UIControlEventValueChanged];
  self.segmentedControl.selectedSegmentIndex = 0;
  
  [self createTemperatureView];
  
  self.firstContainer = [UIView newWithSuperview:self.view];
  self.firstContainer.backgroundColor = [UIColor whiteColor];

  // start/stop/pause buttons
  self.startButton = [UIButton newWithSuperview:self.firstContainer];
  [self.startButton setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
  [self.startButton addTarget:self action:@selector(startButtonClicked) forControlEvents:UIControlEventTouchUpInside];
  self.pauseButton = [UIButton newWithSuperview:self.firstContainer];
  [self.pauseButton setImage:[UIImage imageNamed:@"pause"] forState:UIControlStateNormal];
  [self.pauseButton addTarget:self action:@selector(pauseButtonClicked) forControlEvents:UIControlEventTouchUpInside];
  self.stopButton = [UIButton newWithSuperview:self.firstContainer];
  [self.stopButton setImage:[UIImage imageNamed:@"stop"] forState:UIControlStateNormal];
  [self.stopButton addTarget:self action:@selector(stopButtonClicked) forControlEvents:UIControlEventTouchUpInside];
  [self.startButton sizeToFit];
  [self.pauseButton sizeToFit];
  [self.stopButton sizeToFit];
}

- (void)createTemperatureView {
  self.temperatureContainer = [UIView newWithSuperview:self.view];
  self.temperatureContainer.backgroundColor = [UIColor clearColor];
  
  self.secondContainer = [UIView newWithSuperview:self.temperatureContainer];
  self.secondContainer.backgroundColor = [UIColor whiteColor];
  self.thirdContainer = [UIView newWithSuperview:self.temperatureContainer];
  self.thirdContainer.backgroundColor = [UIColor whiteColor];
  
  // Hot End - label, slider, button
  
  self.hotEndLabel = [UILabel newWithSuperview:self.secondContainer];
  self.hotEndLabel.text = @"Hot End Temperature";
  self.hotEndLabel.font = [Print3RUtilities themeBoldFontWithSize:18.0f];
  
  self.hotEndSlider = [ASValueTrackingSlider newWithSuperview:self.secondContainer];
  self.hotEndSlider.maximumValue = 300.0;
  self.hotEndSlider.popUpViewCornerRadius = 12.0;
  [self.hotEndSlider setMaxFractionDigitsDisplayed:0];
  self.hotEndSlider.popUpViewColor = [Print3RUtilities lightGreenColor];
  self.hotEndSlider.font = [UIFont fontWithName:@"GillSans-Bold" size:22];
  self.hotEndSlider.textColor = [Print3RUtilities defaultYellowColor];
  [self.hotEndSlider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
  self.hotEndButton = [UIButton newWithSuperview:self.secondContainer];
  [self.hotEndButton setTitle:@"Set to - ℃" forState:UIControlStateNormal];
  self.hotEndButton.backgroundColor = [Print3RUtilities lightGreenColor];
  [self.hotEndButton setTitleColor:[Print3RUtilities defaultYellowColor] forState:UIControlStateNormal];
  [self.hotEndButton.titleLabel setFont:[UIFont fontWithName:@"GillSans-Bold" size:18]];
  [self.hotEndButton addTarget:self action:@selector(hotEndButtonClicked) forControlEvents:UIControlEventTouchUpInside];
  self.hotEndButton.enabled = NO;
  self.hotEndButton.alpha = 0.4f;
  
  // Bed - label, slider, button
  
  self.bedLabel = [UILabel newWithSuperview:self.thirdContainer];
  self.bedLabel.text = @"Bed Temperature";
  self.bedLabel.font = [Print3RUtilities themeBoldFontWithSize:18.0f];
  
  self.bedSlider = [ASValueTrackingSlider newWithSuperview:self.thirdContainer];
  self.bedSlider.maximumValue = 300.0;
  self.bedSlider.popUpViewCornerRadius = 12.0;
  [self.bedSlider setMaxFractionDigitsDisplayed:0];
  self.bedSlider.popUpViewColor = [Print3RUtilities lightGreenColor];
  self.bedSlider.font = [UIFont fontWithName:@"GillSans-Bold" size:22];
  self.bedSlider.textColor = [Print3RUtilities defaultYellowColor];
  [self.bedSlider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
  
  self.bedButton = [UIButton newWithSuperview:self.thirdContainer];
  [self.bedButton setTitle:@"Set to - ℃" forState:UIControlStateNormal];
  self.bedButton.backgroundColor = [Print3RUtilities lightGreenColor];
  [self.bedButton setTitleColor:[Print3RUtilities defaultYellowColor] forState:UIControlStateNormal];
  [self.bedButton.titleLabel setFont:[UIFont fontWithName:@"GillSans-Bold" size:18]];
  [self.bedButton addTarget:self action:@selector(bedButtonClicked) forControlEvents:UIControlEventTouchUpInside];
  self.bedButton.enabled = NO;
  self.bedButton.alpha = 0.4f;
  
  if(self.printer.printerState.tool0.actual) {
    self.hotEndSlider.value = [self.printer.printerState.tool0.actual floatValue];
  }
  if(self.printer.printerState.tool1.actual) {
    self.bedSlider.value = [self.printer.printerState.tool1.actual floatValue];
  }
  
  if (!self.printer.printerState.tool1) {
    self.bedSlider.alpha = 0.2;
    self.bedSlider.userInteractionEnabled = NO;
    self.bedLabel.text = @"Bed Temperature (Unavailable)";
  }
}

- (void)createPrintHeadView {

  self.printHeadContainer = [UIView newWithSuperview:self.view];
  self.printHeadContainer.backgroundColor = [UIColor clearColor];

  self.fourthContianer = [UIView newWithSuperview:self.printHeadContainer];
  self.fourthContianer.backgroundColor = [UIColor whiteColor];
  
  self.homeLabel = [UILabel newWithSuperview:self.fourthContianer];
  self.homeLabel.text = @"Print Head Home";
  self.homeLabel.font = [Print3RUtilities themeBoldFontWithSize:18.0f];
  [self.homeLabel sizeToFit];
  
  self.xHomeButton = [UIButton newWithSuperview:self.fourthContianer];
  [self.xHomeButton setTitle:@" X " forState:UIControlStateNormal];
  self.yHomeButton = [UIButton newWithSuperview:self.fourthContianer];
  [self.yHomeButton setTitle:@" Y " forState:UIControlStateNormal];
  self.zHomeButton = [UIButton newWithSuperview:self.fourthContianer];
  [self.zHomeButton setTitle:@" Z " forState:UIControlStateNormal];
  
  NSArray *buttons = @[self.xHomeButton,self.yHomeButton, self.zHomeButton];
  for (UIButton *button in buttons) {
    [button.titleLabel setFont:[UIFont fontWithName:@"GillSans-Bold" size:18]];;
    [button setTitleColor:[Print3RUtilities defaultYellowColor] forState:UIControlStateNormal];
    button.backgroundColor = [Print3RUtilities lightGreenColor];
    [button sizeToFit];
    [button addTarget:self action:@selector(homeClicked:) forControlEvents:UIControlEventTouchUpInside];
  }
  
  self.fifthhContianer = [UIView newWithSuperview:self.printHeadContainer];
  self.fifthhContianer.backgroundColor = [UIColor whiteColor];
 
  self.customCommandLabel = [UILabel newWithSuperview:self.fifthhContianer];
  self.customCommandLabel.text = @"Custom Command";
  self.customCommandLabel.font = [Print3RUtilities themeBoldFontWithSize:18.0f];
  [self.customCommandLabel sizeToFit];
  
  self.customCommandTextField = [UITextField newWithSuperview:self.fifthhContianer];
  self.customCommandTextField.autocorrectionType = UITextAutocorrectionTypeNo;
  self.customCommandTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
  self.customCommandTextField.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.3f];
  self.customCommandTextField.delegate = self;
  self.customCommandTextField.returnKeyType = UIReturnKeyDone;
  
  self.customCommandButton = [UIButton newWithSuperview:self.fifthhContianer];
  [self.customCommandButton setTitle:@" Send " forState:UIControlStateNormal];
  [self.customCommandButton.titleLabel setFont:[UIFont fontWithName:@"GillSans-Bold" size:18]];
  [self.customCommandButton setTitleColor:[Print3RUtilities defaultYellowColor] forState:UIControlStateNormal];
  self.customCommandButton.backgroundColor = [Print3RUtilities lightGreenColor];
  [self.customCommandButton sizeToFit];
  [self.customCommandButton addTarget:self action:@selector(sendCustomCommandClicked) forControlEvents:UIControlEventTouchUpInside];
}

- (void)viewWillLayoutSubviews {
  [super viewWillLayoutSubviews];

  self.backGround.frame = self.view.frame;

  self.firstContainer.width = self.view.width;
  self.firstContainer.y = kPadding/2 + 64;
  self.firstContainer.height = kPadding * 3;

  // start/stop/pause buttons
  [self.pauseButton moveToCenterOfView:self.firstContainer];
  [self.startButton moveToLeftOfSiblingView:self.pauseButton margin:self.firstContainer.width/4];
  [self.startButton moveToVerticalCenterOfView:self.firstContainer];
  [self.stopButton moveToRightOfSiblingView:self.pauseButton margin:self.firstContainer.width/4];
  [self.stopButton moveToVerticalCenterOfView:self.firstContainer];
  
  if (self.segmentedControl.selectedSegmentIndex == 0) {
    [self layoutTemperatureViews];
  }
  else {
    [self layoutPrintHeadViews];
  }
}

- (void)layoutTemperatureViews {
  self.temperatureContainer.frame = self.view.frame;
  self.temperatureContainer.y = self.firstContainer.maxY;
  self.temperatureContainer.height = self.view.height - self.firstContainer.maxY;
  
  self.secondContainer.y = kPadding/2;
  self.secondContainer.width = self.view.width - kPadding * 2;
  self.secondContainer.x = kPadding;
  self.secondContainer.height = kPadding * 7.5;
  
  [self.hotEndSlider moveToCenterOfView:self.secondContainer];
  self.hotEndSlider.width = self.secondContainer.width -kPadding *2;
  self.hotEndSlider.x = kPadding;
  
  [self.hotEndLabel sizeToFit];
  [self.hotEndLabel moveAboveSiblingView:self.hotEndSlider margin:kPadding/4];
  self.hotEndLabel.x = kPadding;
  
  [self.hotEndButton sizeToFit];
  [self.hotEndButton moveBelowSiblingView:self.hotEndSlider margin:kPadding/4];
  self.hotEndButton.width = self.secondContainer.width - kPadding * 4 ;
  [self.hotEndButton moveToHorizontalCenterOfView:self.secondContainer];
  
  [self.thirdContainer moveBelowSiblingView:self.secondContainer margin:kPadding/2];
  self.thirdContainer.width = self.view.width - kPadding * 2;
  self.thirdContainer.x = kPadding;
  self.thirdContainer.height = kPadding * 7.5;
  
  [self.bedSlider moveToCenterOfView:self.thirdContainer];
  self.bedSlider.width = self.thirdContainer.width -kPadding *2;
  self.bedSlider.x = kPadding;
  
  [self.bedLabel sizeToFit];
  [self.bedLabel moveAboveSiblingView:self.bedSlider margin:kPadding/4];
  self.bedLabel.x = kPadding;
  
  [self.bedButton sizeToFit];
  [self.bedButton moveBelowSiblingView:self.bedSlider margin:kPadding/4];
  self.bedButton.width = self.thirdContainer.width - kPadding * 4 ;
  [self.bedButton moveToHorizontalCenterOfView:self.thirdContainer];
}

- (void)layoutPrintHeadViews {
  self.printHeadContainer.frame = self.view.frame;
  self.printHeadContainer.y = self.firstContainer.maxY;
  self.printHeadContainer.height = self.view.height - self.firstContainer.maxY;
  
  self.fifthhContianer.y = kPadding/2;
  self.fifthhContianer.width = self.view.width - kPadding * 2;
  self.fifthhContianer.x = kPadding;
  self.fifthhContianer.height = kPadding * 7.5;
  
  [self.customCommandLabel moveToHorizontalCenterOfView:self.fifthhContianer];
  self.customCommandLabel.y = kPadding/3;
  
  [self.customCommandButton sizeToFit];
  [self.customCommandButton moveToVerticalCenterOfView:self.fifthhContianer];
  self.customCommandButton.x = self.fifthhContianer.width - self.customCommandButton.width - kPadding;
  
  self.customCommandTextField.width = self.fifthhContianer.width - self.customCommandButton.width - kPadding * 2.5;
  [self.customCommandTextField moveToLeftOfSiblingView:self.customCommandButton margin:kPadding/2];
  self.customCommandTextField.height = self.customCommandButton.height;
  [self.customCommandTextField moveToVerticalCenterOfView:self.fifthhContianer];
  
  [self.fourthContianer moveBelowSiblingView:self.fifthhContianer margin:kPadding/2];
  self.fourthContianer.width = self.view.width - kPadding * 2;
  self.fourthContianer.x = kPadding;
  self.fourthContianer.height = kPadding * 7.5;

  [self.homeLabel moveToHorizontalCenterOfView:self.fourthContianer];
  self.homeLabel.y = kPadding/2;
  [self.yHomeButton moveToCenterOfView:self.fourthContianer];
  self.yHomeButton.y+= kPadding/2;
  [self.xHomeButton moveToLeftOfSiblingView:self.yHomeButton margin:self.fourthContianer.width/4];
  [self.xHomeButton moveToVerticalCenterOfView:self.fourthContianer];
  self.xHomeButton.y+= kPadding/2;
  [self.zHomeButton moveToRightOfSiblingView:self.yHomeButton margin:self.fourthContianer.width/4];
  [self.zHomeButton moveToVerticalCenterOfView:self.fourthContianer];
  self.zHomeButton.y+= kPadding/2;

}

- (void)segmentChanged:(UISegmentedControl *)segmentedControl {
  if(segmentedControl.selectedSegmentIndex == 1) {
    [self animateTemperatureContainerOut:YES];
  }
  else {
    [self animateTemperatureContainerOut:NO];
  }
}

- (void)animateTemperatureContainerOut:(BOOL)outside {
  
  CGFloat tempTargetX = 0.0f;
  CGFloat headTargetX = 0.0f;

  if (outside) {
    tempTargetX = -self.view.width - kPadding;
    [self createPrintHeadView];
    self.printHeadContainer.x = - (tempTargetX);
  }
  else {
    headTargetX = self.view.width + kPadding;
    [self createTemperatureView];
    self.temperatureContainer.x = - (headTargetX);
  }
  [UIView animateWithDuration:0.2f animations:^{
    self.printHeadContainer.x = headTargetX;
    self.temperatureContainer.x = tempTargetX;
  } completion:^(BOOL finished) {
    if (outside) {
      [self.temperatureContainer removeFromSuperview];
    }else {
      [self.printHeadContainer removeFromSuperview];
    }
    [self.view setNeedsLayout];
  }];
}

#pragma mark - UISlider events

- (void)sliderValueChanged:(UISlider *)slider {
  if ([slider isEqual:self.hotEndSlider]) {
    if (!self.hotEndButton.enabled) {
      self.hotEndButton.enabled = YES;
      self.hotEndButton.alpha = 1.0f;
    }
    [self.hotEndButton setTitle:[NSString stringWithFormat:@"Set to %.0f ℃",slider.value] forState:UIControlStateNormal];

  }
  else {
    if (!self.bedButton.enabled) {
      self.bedButton.enabled = YES;
      self.bedButton.alpha = 1.0f;
    }
    [self.bedButton setTitle:[NSString stringWithFormat:@"Set to %.0f ℃",slider.value] forState:UIControlStateNormal];
  }
}

#pragma mark - button handlers

- (void)startButtonClicked {
  [self.printer postPrinterJobCommandWithType:PrinterJobTypeStart];
}

- (void)pauseButtonClicked {
  [self.printer postPrinterJobCommandWithType:PrinterJobTypePause];
}

- (void)stopButtonClicked {
  [self.printer postPrinterJobCommandWithType:PrinterJobTypeCancel];
}

- (void)hotEndButtonClicked {
  [self postCommandForTool:@"tool0" target:self.hotEndSlider.value];
}

- (void)bedButtonClicked {
  [self postCommandForTool:@"tool1" target:self.bedSlider.value];
}

- (void)postCommandForTool:(NSString *)tool target:(float)target {
  NSDictionary *params = @{@"command":@"target", @"targets": @{tool:@(target)}};
  [self.printer postToolCommandWithParams:params];
}

- (void)homeClicked:(UIButton *)button {
  NSString *axis = @"";
  if (button == self.xHomeButton) {
    axis = @"x";
  }
  if (button == self.yHomeButton) {
    axis = @"y";
  }
  if (button == self.zHomeButton) {
    axis = @"z";
  }
  NSDictionary *params = @{@"command":@"home", @"axes":@[axis]};
  [self.printer postPrinterHeadCommandWithParams:params];
}

- (void)sendCustomCommandClicked {
  if ([self.customCommandTextField.text isEqualToString:@""] || !self.customCommandTextField.text) {
    return;
  }
  if ([self.customCommandTextField isFirstResponder]) {
    [self.customCommandTextField resignFirstResponder];
  }
  
  [self.printer postCommandWithParams:@{@"command":self.customCommandTextField.text}];
}

- (void)homeButtonClicked {
  NSDictionary *params = @{@"command":@"home", @"axes":@[@"x",@"y"]};
  // z for z home
  [self.printer postPrinterHeadCommandWithParams:params];
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
  if ([self.customCommandTextField isFirstResponder]) {
    [self.customCommandTextField resignFirstResponder];
  }
}
#pragma mark - UITextField delegate 

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
  if ([self.customCommandTextField isFirstResponder]) {
    [self.customCommandTextField resignFirstResponder];
  }
  return YES;
}
@end
