//
//  ControlPannelViewController.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 5/9/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "ControlPannelViewController.h"
#import "UIView+Layout.h"
#import "KAProgressLabel.h"
#import "PrinterJobInfo.h"
#import "PrinterJob.h"
#import "PrinterJobProgress.h"
#import "PrinterState.h"
#import "MGFashionMenuView.h"
#import "CapabilitiesTableViewController.h"
#import "MenuView.h"
#import "FileExplorerViewController.h"
#import "CustomDrawingViewController.h"
#import "PrinterControlCenterViewController.h"
#import "UserDefaultsManager.h"
#import "Print3RUtilities.h"
#import "NewPrinterViewController.h"
#import "DiskManager.h"
#import <WebKit/WebKit.h>
#import "ImagePickerManager.h"
#import "WebViewController.h"

static CGFloat const kMaxRadius_iPhone = 120.0f;
static CGFloat const kPadding = 20.0f;
static CGFloat const kBorderWidth = 28.0f;

// degrees
static CGFloat const printCircleStart = 271.0f;
static CGFloat const printCircleEnd = 89.0f;
static CGFloat const hotEndCircleStart = 91.0f;
static CGFloat const hotEndCircleEnd = 179.0f;
static CGFloat const bedCircleStart = 181.0f;
static CGFloat const bedCircleEnd = 269.0f;

// stepSizes
static CGFloat const printCircleMaxValue = 100.0f;
static CGFloat const hotEndCirleMaxValue = 300.0f;
static CGFloat const bedCircleMaxValue = 300.0f;

// view tags
static int const printTag = 4;
static int const hotEndTag = 5;

static NSString * const kPrinterBedSettingsNotPresent = @"You need to configure printer bed settings first.";

typedef NS_ENUM(NSInteger, kPrintState) {
  kPrintStatePrinting,
  kPrintStatePaused,
  kPrintStateStopped
};

@interface ControlPannelViewController () <MenuViewProtocol, AddPrinterViewDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (nonatomic, strong)KAProgressLabel *printCircle;
@property (nonatomic, strong)KAProgressLabel *hotEndCircle;
@property (nonatomic, strong)KAProgressLabel *bedCircle;
@property (nonatomic, strong)KAProgressLabel *printCircleBack;
@property (nonatomic, strong)KAProgressLabel *hotEndCircleBack;
@property (nonatomic, strong)KAProgressLabel *bedCircleBack;
@property (nonatomic, assign)CGFloat radius;

@property (nonatomic, strong)PrinterState *printerState;
@property (nonatomic, strong)PrinterJobInfo *jobInfo;
@property (nonatomic, strong)UILabel *stateLabel;

@property (nonatomic, assign)BOOL isMenuShown;
@property (nonatomic, strong)MGFashionMenuView *menu;
@property (nonatomic, strong)MenuView *menuView;
@property (nonatomic, strong)UILabel *fileNameLabel;
@property (nonatomic, strong)UILabel *printPercentageLabel;
@property (nonatomic, strong)UILabel *hotEndLabel;
@property (nonatomic, strong)UILabel *hotEndTempLabel;
@property (nonatomic, strong)UILabel *bedLabel;
@property (nonatomic, strong)UILabel *bedTempLabel;
@property (nonatomic, strong)UILabel *totalPrintTimeLabel;
@property (nonatomic, strong)UILabel *totalPrintTimeValue;
@property (nonatomic, strong)UILabel *printTimeLeftLabel;
@property (nonatomic, strong)UILabel *printTimeLeftValue;
@property (nonatomic, strong)WKWebView *webView;
@property (nonatomic, strong)UITextView *terminal;
@property (nonatomic, weak) NSTimer *terminalTimer;
@property (nonatomic, assign)kPrintState printState;
@property (nonatomic, strong)UIView *backGround;
@property (nonatomic, strong)UIView *separatorView;
@property (nonatomic, strong)UIView *topContainer;
@end

@implementation ControlPannelViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  self.backGround = [UIView new];
  self.backGround.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.3f];
  [self.view addSubview:self.backGround];
  [self.view sendSubviewToBack:self.backGround];
  self.view.backgroundColor = [UIColor whiteColor];
  
  self.separatorView = [UIView new];
  [self.view addSubview:self.separatorView];
  self.separatorView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2f];
  self.separatorView.height = 1.0f;
  
  self.topContainer = [UIView newWithSuperview:self.view];
  self.topContainer.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.3f];
  
  self.fileNameLabel = [[UILabel alloc] init];
  self.fileNameLabel.font = [Print3RUtilities themeBoldFontWithSize:16.0f];
  [self.topContainer addSubview:self.fileNameLabel];
  
  self.printPercentageLabel = [[UILabel alloc] init];
  self.printPercentageLabel.font = [Print3RUtilities themeRegularFontWithSize:14.0f];
  [self.topContainer addSubview:self.printPercentageLabel];
  
  self.hotEndLabel = [[UILabel alloc] init];
  [self.view addSubview:self.hotEndLabel];
  self.hotEndLabel.font = [Print3RUtilities themeBoldFontWithSize:16.0f];
  self.hotEndLabel.text = @"Hotend";
  
  self.hotEndTempLabel = [[UILabel alloc] init];
  self.hotEndTempLabel.font = [Print3RUtilities themeRegularFontWithSize:14.0f];
  [self.view addSubview:self.hotEndTempLabel];
  
  self.bedLabel = [[UILabel alloc] init];
  [self.view addSubview:self.bedLabel];
  self.bedLabel.font = [Print3RUtilities themeBoldFontWithSize:16.0f];
  
  self.bedLabel.text = @"Bed";
  self.bedTempLabel = [[UILabel alloc] init];
  [self.view addSubview:self.bedTempLabel];
  self.bedTempLabel.font = [Print3RUtilities themeRegularFontWithSize:14.0f];
  
  self.totalPrintTimeLabel = [[UILabel alloc] init];
  self.totalPrintTimeLabel.text = @"Print Time Elapsed: ";
  [self.view addSubview:self.totalPrintTimeLabel];
  self.totalPrintTimeLabel.font = [Print3RUtilities themeBoldFontWithSize:14.0f];
  
  self.totalPrintTimeValue = [[UILabel alloc] init];
  self.totalPrintTimeValue.text = @"-- : --";
  [self.view addSubview:self.totalPrintTimeValue];
  self.totalPrintTimeValue.font = [Print3RUtilities themeRegularFontWithSize:12.0f];
  
  self.printTimeLeftLabel = [[UILabel alloc] init];
  self.printTimeLeftLabel.text = @"Print Time Left: ";
  [self.view addSubview:self.printTimeLeftLabel];
  self.printTimeLeftLabel.font = [Print3RUtilities themeBoldFontWithSize:14.0f];
  
  self.printTimeLeftValue = [[UILabel alloc] init];
  self.printTimeLeftValue.text = @"-- : --";
  [self.view addSubview:self.printTimeLeftValue];
  self.printTimeLeftValue.font = [Print3RUtilities themeRegularFontWithSize:12.0f];
  
  [self setupSubViews];
  [self getJobInfo];
  [self getCurrentState];
  
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  self.title = self.printer.nickName;
}

- (BOOL)shouldAutorotate {
  if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
    return NO;
  }
  return YES;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations {
  if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
    return UIInterfaceOrientationMaskPortrait;
  }
  return UIInterfaceOrientationMaskAll;
}

- (void)setupSubViews {
  self.printCircleBack = [KAProgressLabel new];
  [self.view addSubview:self.printCircleBack];
  self.hotEndCircleBack = [KAProgressLabel new];
  [self.view addSubview:self.hotEndCircleBack];
  self.bedCircleBack = [KAProgressLabel new];
  [self.view addSubview:self.bedCircleBack];
  
  self.printCircle = [KAProgressLabel new];
  [self.view addSubview:self.printCircle];
  self.printCircle.tag = printTag;
  self.hotEndCircle = [KAProgressLabel new];
  [self.view addSubview:self.hotEndCircle];
  self.hotEndCircle.tag = hotEndTag;
  self.bedCircle = [KAProgressLabel new];
  [self.view addSubview:self.bedCircle];
  
  [self drawCircles];
  
  self.stateLabel = [UILabel new];
  [self.view addSubview:self.stateLabel];
  [self.stateLabel setFont:[Print3RUtilities themeBoldFontWithSize:16.0f]];
  self.stateLabel.textAlignment = NSTextAlignmentCenter;
  self.stateLabel.text = @"Connecting";
  
  UIBarButtonItem *menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_more"] style:UIBarButtonItemStylePlain target:self action:@selector(menuPressed)];
  self.navigationItem.rightBarButtonItem = menuButton;
  
  self.menuView = [[[NSBundle mainBundle] loadNibNamed:@"MenuView" owner:self options:nil] firstObject];
  self.menuView.delegate = self;
  self.menu = [[MGFashionMenuView alloc] initWithMenuView:self.menuView animationType:MGAnimationTypeWave];
  [self.menu setBackgroundColor:[UIColor clearColor]];
  [self.view addSubview:self.menu];
  
  //  self.terminal = [[UITextView alloc] init];
  //  [self.view addSubview:self.terminal];
  //  [self.terminal setEditable:NO];
  //  self.terminal.contentInset = UIEdgeInsetsMake(0, 8, 0, 0);
  //  self.terminal.layer.borderWidth = 4.0f;
  //  self.terminal.layer.borderColor = [UIColor lightTextColor].CGColor;
  //  self.terminal.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.4f];
  //  self.terminal.text = @"Getting terminal output...";
  //  [self getTerminalOutput];
}

- (void)showOctoPrint {
  if ([self.printer isDemoPrinter]) {
    [Print3RUtilities showAlertWithTitle:kSuccessTitle message:@"Not avaialble for demo printer" sender:self];
    return;
  }
  WebViewController *vc = [[WebViewController alloc] initWithURL:self.printer.hostURL];
  UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
  [self presentViewController:nav animated:YES completion:nil];
}

- (void)getJobInfo {
  
  if ([self.printer isDemoPrinter]) {
    self.jobInfo = [[PrinterJobInfo alloc] initDemoPrinterJobInfo];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
      [self updateJob];
    });
    return;
  }
  __weak typeof(self) weakSelf = self;
  [self.printer getCurrentJobInfo:^(PrinterJobInfo *jobInfo) {
    __strong typeof(self) strongSelf = weakSelf;
    strongSelf.jobInfo = jobInfo;
    self.printer.jobInfo =  jobInfo;
    if(![strongSelf.jobInfo.job.file.name isKindOfClass:[NSNull class]]) {
      dispatch_async(dispatch_get_main_queue(), ^{
        [strongSelf updateJob];
      });
    }
    [strongSelf performSelector:@selector(getJobInfo) withObject:nil afterDelay:5.0f];
  }];
}

- (void)updateJob {
  self.fileNameLabel.text = self.jobInfo.job.file.name;
  self.printPercentageLabel.text = [NSString stringWithFormat:@"%.0f %%",self.jobInfo.progress.completion];
  [self.fileNameLabel sizeToFit];
  self.totalPrintTimeValue.text = [self.jobInfo.progress humanReadableTotalPrintTime];
  self.printTimeLeftValue.text = [self.jobInfo.progress humanReadablePrintTimeLeft];
  if (self.jobInfo.progress.completion == 100) {
    self.printTimeLeftValue.text = @"-- : --";
  }
  [self changePrintState:kPrintStatePrinting];
  [self animateProgressForCircle:self.printCircle toEndValue:printCircleStart + (self.jobInfo.progress.completion * ((printCircleStart - printCircleEnd)/printCircleMaxValue))];
  [self.view setNeedsLayout];
}

- (void)getCurrentState {
  if ([self.printer isDemoPrinter]) {
    self.printerState = [[PrinterState alloc] initDemoState];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
      [self updateCurrentState];
    });
    return;
  }
  __weak typeof(self) weakSelf = self;
  [self.printer getPrinterCurrentState:^(PrinterState *state) {
    __strong typeof(self) strongSelf = weakSelf;
    if (state) {
      strongSelf.printerState = state;
      self.printer.printerState = state;
      dispatch_async(dispatch_get_main_queue(), ^{
        [strongSelf updateCurrentState];
      });
    }
    [strongSelf performSelector:@selector(getCurrentState) withObject:nil afterDelay:5.0f];
  }];
}

- (void)updateCurrentState {
  self.stateLabel.text = self.printerState.stateString;
  self.hotEndTempLabel.text = [NSString stringWithFormat:@"%@ \u00B0C",self.printerState.tool0.actual];
  [self animateProgressForCircle:self.hotEndCircle toEndValue:hotEndCircleStart + ([self.printerState.tool0.actual floatValue] * ((hotEndCircleEnd - hotEndCircleStart)/hotEndCirleMaxValue)) ];
  if (!self.printerState.tool1) {
    self.bedTempLabel.text = @"Disconnected";
  }
  else {
    self.bedTempLabel.text = [NSString stringWithFormat:@"%@ \u00B0C",self.printerState.tool1.actual];
    [self animateProgressForCircle:self.bedCircle toEndValue:bedCircleStart + ([self.printerState.tool1.actual floatValue] * ((bedCircleEnd - bedCircleStart)/bedCircleMaxValue)) ];
  }
  [self.view setNeedsLayout];
}

- (void)viewWillLayoutSubviews {
  [super viewWillLayoutSubviews];
  
  CGFloat width = self.view.width;
  self.menu.width = width;
  self.menuView.width = width;
  self.menuView.height = 130;
  
  self.radius = (width - (kPadding * 2))/2;
  
  if(self.radius > kMaxRadius_iPhone) {
    self.radius = kMaxRadius_iPhone;
  }
  
  [self setFramesForCircle:self.printCircle];
  [self setFramesForCircle:self.hotEndCircle];
  [self setFramesForCircle:self.bedCircle];
  
  [self setFramesForBackCircle:self.printCircleBack];
  [self setFramesForBackCircle:self.hotEndCircleBack];
  [self setFramesForBackCircle:self.bedCircleBack];
  
  self.topContainer.width = self.view.width;
  [self.topContainer moveAboveSiblingView:self.printCircle margin:kPadding/2];
  self.topContainer.height = kPadding * 2.5;
  
  if(self.fileNameLabel.text && ![self.fileNameLabel.text isEqualToString:@""]) {
    [self.fileNameLabel sizeToFit];
    self.fileNameLabel.y = kPadding/3;
    [self.fileNameLabel moveToHorizontalCenterOfView:self.topContainer];
  }
  if(self.printPercentageLabel.text && ![self.printPercentageLabel.text isEqualToString:@""]) {
    [self.printPercentageLabel sizeToFit];
    [self.printPercentageLabel moveBelowSiblingView:self.fileNameLabel margin:0];
    [self.printPercentageLabel moveToHorizontalCenterOfView:self.topContainer];
  }
  [self.hotEndLabel sizeToFit];
  [self.bedLabel sizeToFit];
  [self.hotEndLabel moveBelowSiblingView:self.printCircle margin:kPadding/4];
  [self.bedLabel moveBelowSiblingView:self.printCircle margin:kPadding/4];
  self.bedLabel.x = kPadding;
  self.hotEndLabel.x = self.view.width - kPadding - self.hotEndLabel.width;
  if (self.hotEndTempLabel.text && ![self.hotEndTempLabel.text isEqualToString:@""]) {
    [self.hotEndTempLabel sizeToFit];
    [self.hotEndTempLabel moveBelowSiblingView:self.hotEndLabel margin:kPadding/5];
    self.hotEndTempLabel.x = self.view.width - kPadding - self.hotEndLabel.width;
  }
  if (self.bedTempLabel.text && ![self.bedTempLabel.text isEqualToString:@""]) {
    [self.bedTempLabel sizeToFit];
    [self.bedTempLabel moveBelowSiblingView:self.bedLabel margin:kPadding/5];
    self.bedTempLabel.x = kPadding;
  }
  self.separatorView.width = self.view.width - kPadding * 4;
  self.separatorView.x = kPadding * 2;
  [self.separatorView moveBelowSiblingView:self.bedLabel margin:kPadding * 2];
  
  [self.totalPrintTimeLabel sizeToFit];
  [self.totalPrintTimeLabel moveBelowSiblingView:self.separatorView margin:kPadding/2];
  self.totalPrintTimeLabel.x = kPadding;
  
  [self.totalPrintTimeValue sizeToFit];
  self.totalPrintTimeValue.y = self.totalPrintTimeLabel.y + 2;
  self.totalPrintTimeValue.x = self.totalPrintTimeLabel.maxX + kPadding/4;
  
  [self.printTimeLeftLabel sizeToFit];
  [self.printTimeLeftLabel moveBelowSiblingView:self.totalPrintTimeLabel margin:kPadding/4];
  self.printTimeLeftLabel.x = kPadding;
  
  [self.printTimeLeftValue sizeToFit];
  self.printTimeLeftValue.y = self.printTimeLeftLabel.y + 2;
  self.printTimeLeftValue.x = self.printTimeLeftLabel.maxX + kPadding/4;
  
  self.stateLabel.center = self.view.center;
  [self.stateLabel sizeToFit];
  self.stateLabel.width = self.radius * 2 - kPadding;
  
  self.terminal.width = self.view.width - kPadding * 2;
  [self.terminal moveBelowSiblingView:self.printCircle margin:kPadding * 2];
  [self.terminal alignHorizontallyWithSiblingView:self.printCircle];
  self.terminal.height = self.view.maxY - self.terminal.y - kPadding/2;
  self.backGround.frame = self.view.frame;
  
}

//- (void)willMoveToParentViewController:(UIViewController *)parent {
//  if(!parent) {
//    if(self.terminalTimer) {
//      [self.terminalTimer invalidate];
//      self.terminalTimer = nil;
//    }
//  }
//}

- (void)setFramesForCircle:(KAProgressLabel *)circle {
  circle.width = (self.radius * 2) - kPadding/2;
  circle.height = (self.radius * 2) - kPadding/2;
  circle.center = self.view.center;
}

- (void)setFramesForBackCircle:(KAProgressLabel *)backCircle {
  backCircle.width = self.radius * 2;
  backCircle.height = self.radius * 2;
  backCircle.center = self.view.center;
}

- (void)drawCircles {
  
  [self setupProgressCircle:self.printCircle withStartDegree:printCircleStart endDegree:printCircleStart color:[UIColor greenColor]];
  [self setupProgressCircle:self.hotEndCircle withStartDegree:hotEndCircleStart endDegree:hotEndCircleStart color:[UIColor redColor]];
  [self setupProgressCircle:self.bedCircle withStartDegree:bedCircleStart endDegree:bedCircleStart color:[UIColor orangeColor]];
  
  [self setupBackCircle:self.printCircleBack withStart:printCircleStart end:printCircleEnd color:[UIColor greenColor]];
  [self setupBackCircle:self.hotEndCircleBack withStart:hotEndCircleStart end:hotEndCircleEnd color:[UIColor redColor]];
  [self setupBackCircle:self.bedCircleBack withStart:bedCircleStart end:bedCircleEnd color:[UIColor orangeColor]];
  
}

- (void)setupProgressCircle:(KAProgressLabel *)circle withStartDegree:(CGFloat)start endDegree:(CGFloat)end color:(UIColor *)color {
  circle.backgroundColor = [UIColor clearColor];
  circle.startDegree = start;
  circle.endDegree = end;
  circle.trackWidth =  kBorderWidth - kPadding/2;
  circle.progressWidth = kBorderWidth - kPadding/2;
  circle.roundedCornersWidth = 0;
  circle.trackColor = [[UIColor blackColor] colorWithAlphaComponent:.0];
  circle.progressColor = color;
}

- (void)setupBackCircle:(KAProgressLabel *)circle withStart:(CGFloat)start end:(CGFloat)end color:(UIColor *)color{
  circle.backgroundColor = [UIColor clearColor];
  circle.startDegree = start;
  circle.endDegree = end;
  circle.trackWidth = kBorderWidth;
  circle.progressWidth = kBorderWidth;
  circle.roundedCornersWidth = 0;
  circle.trackColor = [UIColor clearColor];
  circle.progressColor = [color colorWithAlphaComponent:.2f];
}

- (void)menuPressed {
  self.isMenuShown = !self.isMenuShown;
  if(self.isMenuShown) {
    [self.menu show];
  }
  else {
    [self.menu hide];
  }
}

- (void)changePrintState:(kPrintState)state {
  self.printState = state;
}

- (void)animateProgressForCircle:(KAProgressLabel *)circle toEndValue:(CGFloat)endValue {
  
  if(circle == self.printCircle) {
    // -4 is to incorporate padding
    endValue = MIN(endValue, (printCircleStart + (printCircleStart - printCircleEnd) - 4));
  }
  else if (circle == self.hotEndCircle) {
    endValue = MAX(endValue, hotEndCircleStart);
    endValue = MIN(endValue, hotEndCircleEnd);
  }
  else if (circle == self.bedCircle) {
    endValue = MAX(endValue, bedCircleStart);
    endValue = MIN(endValue, bedCircleEnd);
  }
  
  [circle setEndDegree:endValue timing:TPPropertyAnimationTimingEaseInEaseOut duration:0.5f delay:0.0f];
  
}

#pragma mark - MenuViewDelegate
- (void)menuOptionPressedWithOption:(MenuOption)option {
  UIViewController *vc;
  [self.menu hide];
  self.isMenuShown=!self.isMenuShown;
  switch (option) {
    case MenuOptionFileExplorer:
      vc = [[FileExplorerViewController alloc] initWithPrinter:self.printer];
      break;
    case MenuOptionHeadControl:
      vc = [[PrinterControlCenterViewController alloc] initWithPrinter:self.printer];
      break;
    case MenuOptionCamera: {
      if(![[ImagePickerManager sharedManager] isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [Print3RUtilities showAlertWithTitle:kErrorTitle message:@"You need to have camera on device to take pictures." sender:self];
        return;
      }
      UIImagePickerController *picker = [[ImagePickerManager sharedManager] pickerforSourceType:UIImagePickerControllerSourceTypeCamera];
      picker.delegate = self;
      [self presentViewController:picker animated:YES completion:nil];
      
    }
      break;
    case MenuOptionOctoprint:
      [self showOctoPrint];
      break;
    case MenuOptionSettings:{
      
      NewPrinterViewController *vc = [[NewPrinterViewController alloc] initWithPrinter:self.printer];
      vc.addPrintViewDelegate = self;
      UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
      [nav setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
      [self presentViewController:nav animated:YES completion:nil];
      return;
    }
      break;
    default:
      break;
  }
  if (vc) {
    [self.navigationController pushViewController:vc animated:YES];
  }
}

//- (void)getTerminalOutput {
//  WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
//  WKUserContentController *controller = [[WKUserContentController alloc] init];
//  configuration.userContentController = controller;
//  
//  NSURL *jsbin = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",self.printer.hostURL ]];
//  self.webView = [[WKWebView alloc] initWithFrame:self.view.frame
//                                    configuration:configuration];
//  
//  [self.webView loadRequest:[NSURLRequest requestWithURL:jsbin]];
//  
//  self.terminalTimer = [NSTimer scheduledTimerWithTimeInterval:5.0f
//                                                        target:self selector:@selector(injectScript) userInfo:nil repeats:YES];
//}
//
//- (void)injectScript{
//#define STRINGIFY(js) #js
//  NSString *exec_template =  @STRINGIFY(
//                                        var script = function(){
//                                          var element = document.getElementById('terminal-output');
//                                          var elementText = element.innerText;
//                                          return elementText;
//                                        };
//                                        script();
//                                        );
//  
//  __weak typeof(self) weakSelf = self;
//  __block NSString *result;
//  
//  [self.webView evaluateJavaScript:exec_template completionHandler:^(id response, NSError *error) {
//    __strong typeof(self) strongSelf = weakSelf;
//    result = response;
//    if(result && ![result isEqualToString:@""] && ![result isEqualToString:@"\n"]) {
//      strongSelf.terminal.text = result;
//    }
//  }];
//}

# pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
  UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
  if (image) {
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    [Print3RUtilities showAlertWithTitle:kSuccessTitle message:@"Image saved to gallery." sender:self];
  }
  else {
    [Print3RUtilities showAlertWithTitle:kErrorTitle message:@"There was a problem saving the image to the gallery." sender:self];
  }
  [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - AddPrinterViewDelegate
- (void)savePrinter:(Printer *)printer {
  // This overwrites/edits an existing printer
  BOOL didReplace = [[DiskManager sharedManager] editPrinter:printer withOriginalHost:self.printer.hostURL];
  if(didReplace) {
    self.printer = printer;
  }
}

- (void)dealloc {
  
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

@end
