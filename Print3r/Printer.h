//
//  Printer.h
//  Print3r
//
//  Created by Vaibhav Aggarwal on 2/13/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PrinterFile.h"
#import "PrinterConnectionOptions.h"
#import "PrinterJobInfo.h"
#import "PrinterState.h"
#import "PrinterVersion.h"

static NSString* const kHostKey = @"hostURL";
static NSString* const kNickNameKey = @"nickName";
static NSString* const kApiKey = @"apiKey";
static NSString* const kImageKey = @"image";
static NSString* const kLengthKey = @"length";
static NSString* const kBreadthKey = @"breadth";
static NSString* const kDemoPrinterNickName = @"DEMO PRINTER";
static NSString* const kDemoPrinterHost = @"DEMO";

@interface Printer : NSObject <NSCoding>


typedef enum{
  PrinterJobTypeStart,
  PrinterJobTypeRestart,
  PrinterJobTypePause,
  PrinterJobTypeCancel
}PrinterJobType;

@property (nonatomic, strong) NSString *hostURL;
@property (nonatomic, strong) NSString *nickName;
@property (nonatomic, strong) NSString *apiKey;
@property (nonatomic, assign) BOOL isRunningJob;
@property (nonatomic, strong) NSString *octoprintApiVersion;
@property (nonatomic, strong) NSString *octoprintServer;
@property (nonatomic, strong) NSArray *printerFiles;
@property (nonatomic, strong) PrinterConnectionOptions *connectionOptions;
@property (nonatomic, strong) PrinterJobInfo *jobInfo;
@property (nonatomic, strong) NSData *image;
@property (nonatomic, strong) PrinterState *printerState;
@property (nonatomic, assign) BOOL didAskForPrinterState;
@property (nonatomic, strong) PrinterVersion *versionInfo;
@property (nonatomic, assign) BOOL isOnline;
@property (nonatomic, assign) NSNumber *length;
@property (nonatomic, assign) NSNumber *breadth;

-(Printer *)initWithData:(NSDictionary *)data;

- (instancetype)initDemoPrinter;

- (BOOL)isDemoPrinter;
/*
 *  GET version info for the Printer's Octoprint
 */
- (void)getVersionInfoWithCompletion:(void (^)(NSDictionary *version))completion;

/*
 *  GET information regarding all files currently available and regarding the disk space still available
 *  locally in the system
 *  @param : location where to check the files - local/sdcard are supported, checks the whole file system if no location is specified
 *  @return : returns an NSArray containing printer files
 */
- (void)getFilesWithLocation:(NSString *)location completion:(void (^)(NSArray *files))completion;

/*
 *  POST command to the printer
 *  @param : dict containing commands to send to the printer
 */
- (void)postCommandWithParams:(NSDictionary *)dict;

/*
 *  POST tool command to the printer
 *  @param : dict containing commands to send to the printer
 */
- (void)postToolCommandWithParams:(NSDictionary *)dict;

/*
 * POST a command to PrinterFile *file
 * currently only select command is supported, and we set print = true
 * 409 if the printer is not operational
 */
- (void)printFile:(PrinterFile *)printerFile completion:(void (^)(void))completion;

/*
 * POST a delete command to PrinterFile *file
 *  if delete is successfull 204 No content
 * 409 if the requested file is currently being printed
 */
- (void)deleteFile:(PrinterFile *)printerFile success:(void (^)(void))success failure:(void (^)(void))failure;

/*
 * GET connections settings for the printer
 * gets current connection settins and available connection options
 */
- (void)getConnectionSettingsWithCompletion:(void (^) (PrinterConnectionOptions *connectionOptions))completion;

/*
 *  POST a connection command to the printer - param contains value to indicate it should connect or disconnect
 *  @param NSDictionary *params - params to be sent with the POST to connect
 *
 */
- (void)postConnectionCommandWithParams:(NSDictionary *)params;

/*
 * POST printHead command to the printer to move the printer in all three axes depending on the paramters
 * either job to a new position or move back to "home" position. X,Y,Z cooridates are optional, but make sure
 * these values are less than maximum allowed, otherwise it will collide
 * @param NSDictionary *params - params to be sent with the POST to move printer head
 */
- (void)postPrinterHeadCommandWithParams:(NSDictionary *)params;

/*
 * POST a job command to the printer 
 * the param has key "command", values - start, restart, pause, cancel
 * @param PrinterJobType *type - Type of job to perform
 */
- (void)postPrinterJobCommandWithType:(PrinterJobType)type;

/*
 * GET current Job info for the printer
 * gets the file info plus the progress info for the current print job
 */

- (void)getCurrentJobInfo:(void (^) (PrinterJobInfo *jobInfo))completion;

/*
 * Upload File on Printer with specified Path
 *
 */
- (void)uploadFileWithData:(NSData *)fileData toPrinterDirectory:(NSString *)printerDirectory withName:(NSString *)withName withCompletion:(void (^) (void))completion;

/*
 * Upload File on Printer with specified Path
 *
 */

- (void)uploadFileAtPath:(NSString *)filePath toPrinterDirectory:(NSString *)printerDirectory withName:(NSString *)withName withCompletion:(void (^) (void))completion;

/*
 * GET current printer status
 */
- (void)getPrinterCurrentState:(void (^)(PrinterState *state))completion;
@end
