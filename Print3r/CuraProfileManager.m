//
//  CuraProfileManager.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 4/9/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "CuraProfileManager.h"
#import "NSData+Compression.h"

@implementation CuraProfileManager

- (NSString *)createProfileFromProfileString:(NSString *)hashString {


  NSString *decodeString = [NSData curaDecodedStringFromBase64String:hashString];
  
  NSRange range = [decodeString rangeOfString:@"\f"];
  if(range.location != NSNotFound) {
    
    NSString *profile = [decodeString substringToIndex:range.location];
    NSString *alteration = [decodeString substringFromIndex:range.location + 1];
    
    profile = [profile stringByReplacingOccurrencesOfString:@"=" withString:@" = "];
    profile = [profile stringByReplacingOccurrencesOfString:@"\b" withString:@"\n"];
    profile = [NSString stringWithFormat:@"[profile]\n%@",profile];
    
    NSArray *alterationChunks = [alteration componentsSeparatedByString:@"\b"];
    NSMutableArray *newChunks = [NSMutableArray new];
    for(NSString *chunk in alterationChunks) {
      NSString *temp = [NSString new];
      temp = [chunk stringByReplacingOccurrencesOfString:@"\n" withString:@"\n\t"];
      temp = [temp stringByReplacingOccurrencesOfString:@"=" withString:@" = "];
      [newChunks addObject:temp];
    }
    NSString *finalAlteration = [NSString stringWithFormat:@"[alteration]\n%@",[newChunks componentsJoinedByString:@"\n"]];
    
    return [NSString stringWithFormat:@"%@\n\n%@",profile, finalAlteration];
  }
  return nil;
}


- (NSString *)parseCuraProfileStringFromGCode:(NSString *)gcodeString {

  NSRange range = [gcodeString rangeOfString:@";CURA_PROFILE_STRING:" options:NSBackwardsSearch];
  
  if(range.location != NSNotFound) {
    NSString *curaString = [gcodeString substringFromIndex:range.location + range.length];
    return [curaString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
  }
  
  return nil;
}

@end
