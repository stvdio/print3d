//
//  GetStartedViewController.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 3/7/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "GetStartedViewController.h"
#import "UserDefaultsManager.h"
#import "Print3RUtilities.h"

@interface GetStartedViewController () <EAIntroDelegate>

@end

@implementation GetStartedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view
    self.view.backgroundColor = [Print3RUtilities defaultThemeColor];
    [self showIntroView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showIntroView {
  EAIntroPage *page1 = [EAIntroPage page];
  page1.title = @"3D Printing Manager is here!";
  page1.titleFont = [Print3RUtilities themeBoldFontWithSize:18.0f];
  page1.desc = @"Manage multiple printers at once";
  page1.descFont = [Print3RUtilities themeRegularFontWithSize:16.0f];
  page1.bgImage = [UIImage imageNamed:@"bkg"];
  page1.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"woodmark"]];
  
  EAIntroPage *page2 = [EAIntroPage page];
  page2.title = @"The amazing control pannel";
  page2.titleFont = [Print3RUtilities themeBoldFontWithSize:18.0f];
  page2.bgColor = [Print3RUtilities darkGreenColor];
  page2.desc = @"Get all the stats with this interactive control pannel";
  page2.descFont = [Print3RUtilities themeRegularFontWithSize:16.0f];
  page2.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title2"]];
  page2.titleIconPositionY = 70;
  
  EAIntroPage *page3 = [EAIntroPage page];
  page3.bgColor = [Print3RUtilities defaultThemeColor];
  page3.title = @"All your files";
  page3.titleFont = [Print3RUtilities themeBoldFontWithSize:18.0f];
  page3.desc = @"Browse,view,edit all your GCodes";
  page3.descFont = [Print3RUtilities themeRegularFontWithSize:16.0f];
  page3.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title3"]];
  page3.titleIconPositionY = 100;
  
  EAIntroPage *page4 = [EAIntroPage page];
  page4.bgColor = [Print3RUtilities darkGreenColor];
  page4.title = @"Introducing the print farm";
  page4.titleFont = [Print3RUtilities themeBoldFontWithSize:18.0f];
  page4.desc = @"Get an overview of all your connected printers";
  page4.descFont = [Print3RUtilities themeRegularFontWithSize:16.0f];
  page4.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"title4"]];
  
  EAIntroView *intro = [[EAIntroView alloc] initWithFrame:self.view.bounds andPages:@[page1,page2,page3,page4]];
  intro.backgroundColor = [Print3RUtilities defaultThemeColor];
  intro.swipeToExit = NO;
  intro.pageControlY = 80.f;
  
  UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
  [btn setFrame:CGRectMake(0, 0, 230, 40)];
  [btn setTitle:@"Get Started" forState:UIControlStateNormal];
  [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
  btn.layer.borderWidth = 2.f;
  btn.layer.cornerRadius = 10;
  btn.layer.borderColor = [[UIColor whiteColor] CGColor];
  intro.skipButton = btn;
  intro.skipButtonY = 60.f;
  intro.skipButtonAlignment = EAViewAlignmentCenter;
  
  [intro setDelegate:self];
  [intro showInView:self.view animateDuration:0.0];
  
}

#pragma mark EAIntroDelegate
- (void)introDidFinish:(EAIntroView *)introView {
  NSLog(@"introDidFinish callback");
  [[UserDefaultsManager sharedManager] setBoolValue:YES forKey:kHasSeenWelcomeScreen];
  [self performSegueWithIdentifier:@"allPrintersSegue" sender:nil];

}
@end
