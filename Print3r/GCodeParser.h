//
//  GCodeParser.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/26/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GCodeParser : NSObject


/*
 * Parses GCode
 * @param NSArray *conentArray, created from NSString after reading a .gcode file
 */

+ (NSArray *)parseGCodeFromStringArray:(NSArray *)contentArray;

/*
 *  Creates a GCode file from 2D points drawn on screen
 *  @param NSArray *points points representing a user drawing on the screen
 */

+ (NSString *)createGCodeFromPoints:(NSArray *)points;

@end
