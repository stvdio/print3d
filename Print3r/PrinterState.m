//
//  PrinterState.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 4/11/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "PrinterState.h"

@implementation PrinterState

- (instancetype)initWithData:(NSDictionary *)data {
  self = [super init];
  if(self) {
    if([data isKindOfClass:[NSDictionary class]]) {
      if(data[@"state"]) {
        
        if([data valueForKeyPath:@"state.text"]) {
          _stateString = [data valueForKeyPath:@"state.text"];
        }
        if([data valueForKeyPath:@"state.flags"]) {
          if([data valueForKeyPath:@"state.flags.operational"]) {
            _operational = [[data valueForKeyPath:@"state.flags.operational"] boolValue];
          }
          if([data valueForKeyPath:@"state.flags.paused"]) {
            _paused = [[data valueForKeyPath:@"state.flags.paused"] boolValue];
          }
          if([data valueForKeyPath:@"state.flags.printing"]) {
            _printing = [[data valueForKeyPath:@"state.flags.printing"] boolValue];
          }
          if([data valueForKeyPath:@"state.flags.sdReady"]) {
            _sdReady = [[data valueForKeyPath:@"state.flags.sdReady"] boolValue];
          }
          if([data valueForKeyPath:@"state.flags.error"]) {
            _error = [[data valueForKeyPath:@"state.flags.error"] boolValue];
          }
          if([data valueForKeyPath:@"state.flags.ready"]) {
            _ready = [[data valueForKeyPath:@"state.flags.ready"] boolValue];
          }
          if([data valueForKeyPath:@"state.flags.closedOrError"]) {
            _closedOrError = [[data valueForKeyPath:@"state.flags.closedOrError"] boolValue];
          }
        }
      }
      if ([data valueForKeyPath:@"temperature"]) {
        if([data valueForKeyPath:@"temperature.tool0"]) {
          _tool0 = [[PrinterToolTemperature alloc] initWithData:[data valueForKeyPath:@"temperature.tool0"]];
        }
        if([data valueForKeyPath:@"temperature.tool1"]) {
          _tool1 = [[PrinterToolTemperature alloc] initWithData:[data valueForKeyPath:@"temperature.tool1"]];
        }
      }

    }
    else {
      _error = YES;
    }
    
  }
  return self;
}

- (instancetype)initDemoState {
  self = [super init];
  if (self) {
    _stateString = @"printing";
    _operational = YES;
    _printing = YES;
    _tool0 = [[PrinterToolTemperature alloc] initDemoToolTemperature];
  }
  return self;
}

- (UIColor *)colorForCurrentState {
  if(self.printing) {
    return [UIColor colorWithHexString:@"27ae60"];
  }
  else if(self.paused) {
    return [UIColor colorWithHexString:@"f1c40f"];
  }
  
  return [UIColor blackColor];
}
@end
