//
//  CustomDrawingViewController.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 3/6/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "CustomDrawingViewController.h"
#import "GCodeParser.h"
#import "Print3RUtilities.h"
#import "CachedLinearInterpolation.h"

static NSString * const printerDirectory = @"local";
static NSString * const fileName = @"demo_print.gcode";
static NSString * const kErrorPrintingFile = @"There was an error in printing the file. Please redraw and try again.";

@interface CustomDrawingViewController ()
@property (weak, nonatomic) IBOutlet UIButton *clearButton;
@property (weak, nonatomic) IBOutlet UIButton *printButton;
@property (strong, nonatomic) UIView *containerView;
@property (strong, nonatomic) CachedLinearInterpolation *cachedLinearInterpolationView;
@end

@implementation CustomDrawingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
  [self.clearButton addTarget:self action:@selector(clearButtonClicked) forControlEvents:UIControlEventTouchUpInside];
  [self.printButton addTarget:self action:@selector(printButtonClicked) forControlEvents:UIControlEventTouchUpInside];
  self.containerView = [[UIView alloc] init];
  [self.view addSubview:self.containerView];
  self.containerView.layer.borderWidth = 4.0f;
  self.containerView.layer.borderColor = [Print3RUtilities defaultThemeColor].CGColor;
  
  self.cachedLinearInterpolationView = [[CachedLinearInterpolation alloc] init];
  self.cachedLinearInterpolationView.backgroundColor = [UIColor whiteColor];
  [self.containerView addSubview:self.cachedLinearInterpolationView];
  
}

- (void)viewWillLayoutSubviews {
  [super viewWillLayoutSubviews];
  self.containerView.frame = CGRectMake(0, 0, 200, 400);
  self.cachedLinearInterpolationView.frame = self.containerView.frame;
}

- (void)clearButtonClicked {
    [self.cachedLinearInterpolationView clearScreen];
}

- (void)printButtonClicked {
  // parse the points to a GCode file
  // send the print command to the printer with that file name
  CachedLinearInterpolation *cachedView = (CachedLinearInterpolation *)self.view;
  if(cachedView) {
    NSString *GCodeFileString = [GCodeParser createGCodeFromPoints:cachedView.dataPoints];
    if(GCodeFileString.length < 1) {
      [Print3RUtilities showAlertWithTitle:kErrorTitle message:kErrorPrintingFile sender:self];
      return;
    }
    
    NSData *fileData = [GCodeFileString dataUsingEncoding:NSUTF8StringEncoding];
      [self.printer uploadFileWithData:fileData toPrinterDirectory:printerDirectory withName:fileName withCompletion:nil];
  }
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
