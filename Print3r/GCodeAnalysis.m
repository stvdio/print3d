//
//  GCodeAnalysis.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/16/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "GCodeAnalysis.h"

@implementation GCodeAnalysis

- (GCodeAnalysis *)initWithData:(NSDictionary *)data {
  self = [super init];
  if(self) {
    if(data[@"estimatedPrintTime"]) {
      self.estimatedPrintTime = data[@"estimatedPrintTime"];
    }
    if(data[@"filament"]) {
      self.filament = [[Filament alloc] initWithData:data[@"filament"]];
    }
  }
  return self;
}
@end
