//
//  CapabilitiesTableViewController.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 4/4/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "CapabilitiesTableViewController.h"
#import "FileExplorerViewController.h"
#import "CustomDrawingViewController.h"
#import "PrinterControlCenterViewController.h"
#import "CaptureToPrintViewController.h"

@interface CapabilitiesTableViewController ()
@property (nonatomic, strong) NSArray *capabilities;
@property (nonatomic, strong) Printer *printer;
@end

@implementation CapabilitiesTableViewController

- (instancetype)initWithPrinter:(Printer *)printer {
  
  self = [super init];
  if(self) {
    self.printer = printer;
  }
  return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
  self.title = @"Capabilities";
  self.capabilities = @[@"File Explorer", @"Custom Drawing", @"Printer Head Control", @"Capture To Print"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  return 40.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"demoCell"];
  if(!cell) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"demoCell"];
  }
  cell.textLabel.text = [self.capabilities objectAtIndex:indexPath.row];
  return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  [tableView deselectRowAtIndexPath:indexPath animated:YES];
  
  UIViewController *vc;
  if(indexPath.row == 0) {
    
    vc = [[FileExplorerViewController alloc] initWithPrinter:self.printer];
    //SettingsViewController *vc = [[SettingsViewController alloc] init];
    //[vc addSettingsViewFor:self];
  }
  if(indexPath.row == 1) {
    vc = [[CustomDrawingViewController alloc] initWithPrinter:self.printer];
  }
  if(indexPath.row == 2) {
    vc = [[PrinterControlCenterViewController alloc] initWithPrinter:self.printer];
  }
  if(indexPath.row == 3) {
    vc = [[CaptureToPrintViewController alloc] init];
  }
  
  [self.navigationController pushViewController:vc animated:YES];
}
@end
