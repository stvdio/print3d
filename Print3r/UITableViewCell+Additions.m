//
//  UITableViewCell+Additions.m
//  Print3r
//
//  Created by Vaibhav Aggarwal on 2/14/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "UITableViewCell+Additions.h"
#import "UIView+Additions.h"
#import "SWTableViewCell.h"
#import "Print3RUtilities.h"

@implementation UITableViewCell (Additions)

- (NSArray *)leftButtonsForSwipeableCell
{
  NSMutableArray *leftUtilityButtons = [NSMutableArray new];
  
  [leftUtilityButtons sw_addUtilityButtonWithColor:[Print3RUtilities lightestGreenColor] icon:[UIImage imageNamed:@"ic_share"]];
  
//  [leftUtilityButtons sw_addUtilityButtonWithColor:
//   [UIColor colorWithRed:0.07 green:0.75f blue:0.16f alpha:1.0]
//                                              title:@"Print"];
  [leftUtilityButtons sw_addUtilityButtonWithColor:
   [Print3RUtilities lighterGreenColor]
                                              icon:[UIImage imageNamed:@"ic_more"]];
  [leftUtilityButtons sw_addUtilityButtonWithColor:
   [Print3RUtilities lightGreenColor]
                                              icon:[UIImage imageNamed:@"ic_edit"]];
  [leftUtilityButtons sw_addUtilityButtonWithColor:
   [Print3RUtilities defaultThemeColor]
                                             icon:[UIImage imageNamed:@"ic_delete"]];
  return leftUtilityButtons;
}
  
@end
