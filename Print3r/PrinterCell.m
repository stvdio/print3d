//
//  PrinterCell.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/14/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "PrinterCell.h"
#import "Print3RUtilities.h"
#import "UIView+Layout.h"

@interface PrinterCell()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) UIView *separator;

@end

@implementation PrinterCell

- (void)awakeFromNib {
    // Initialization code
  self.backgroundColor = [Print3RUtilities darkGreenColor];
  self.nameLabel.textColor = [UIColor whiteColor];
  self.separator = [[UIView alloc] init];
  self.separator.height = 1.0f;
  self.separator.backgroundColor = [Print3RUtilities lighterGreenColor];
  [self.contentView addSubview:self.separator];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
  
    UIView * selectedBackgroundView = [[UIView alloc] init];
    [selectedBackgroundView setBackgroundColor:[Print3RUtilities lighterGreenColor]];
    [self setSelectedBackgroundView:selectedBackgroundView];
}

- (void)layoutSubviews {
  [super layoutSubviews];
  self.separator.width = self.contentView.width;
  self.separator.x = 8.0f;
  self.separator.y = self.contentView.height - 1;
}

- (void)configForPrinter:(Printer *)printer {
  if(![printer.nickName isEqualToString:@""]) {
    self.nameLabel.text = printer.nickName;
  }
  else {
    self.nameLabel.text = printer.hostURL;
  }
}
@end
