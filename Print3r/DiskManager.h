//
//  DiskManager.h
//  Print3r
//
//  Created by Vaibhav Aggarwal on 2/13/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Printer.h"

@interface DiskManager : NSObject

+ (instancetype)sharedManager;

// save and fetch user preferences in nsuserdefaults
- (void)saveUserPreference:(NSString *)preference forKey:(NSString *)key;
- (NSString *)userPreferenceForKey:(NSString *)key;

// save and fetch printer objects from disk
- (NSArray *)getAllPrinters;
- (void)savePrinters:(NSArray *)printers;
- (BOOL)savePrinter:(Printer *)printer;
- (BOOL)editPrinter:(Printer *)printer withOriginalHost:(NSString *)originalHost;
- (void)deletePrinter:(Printer *)printer;
- (BOOL)doesPrinterExist:(Printer *)printer;
@end
