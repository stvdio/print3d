//
//  WebViewController.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 4/16/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()
@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, strong) NSString *url;
@end

@implementation WebViewController

- (instancetype)initWithURL:(NSString *)URL {
  self = [super init];
  _url = URL;
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  self.webView = [[UIWebView alloc] init];
  [self.view addSubview:self.webView];
  self.webView.scalesPageToFit = YES;
  UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Delete"] style:UIBarButtonItemStylePlain target:self action:@selector(cancelPressed)];
  self.navigationItem.leftBarButtonItem = cancel;
  self.title = @"OctoPrint";
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",self.url]]]];

}

- (void)viewDidLayoutSubviews {
  [super viewDidLayoutSubviews];
  self.webView.frame = self.view.frame;

}

- (void)cancelPressed {
  [self dismissViewControllerAnimated:YES completion:nil];

}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
