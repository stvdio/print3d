//
//  CuraProfileManager.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 4/9/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CuraProfileManager : NSObject

/*
 * Creates Cura profile from profile string hash
 * 
 * @param hashString - Cura Profile String
 *
 * @return cura profile string
 */
- (NSString *)createProfileFromProfileString:(NSString *)hashString;

/*
 * Parses cura profile string from Gcode
 *
 * @param gcodeString
 *
 * @return cura profile string hash
 */

- (NSString *)parseCuraProfileStringFromGCode:(NSString *)gcodeString;

@end
