//
//  QuoteViewController.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 4/4/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "QuoteViewController.h"

@interface QuoteViewController ()
@property (weak, nonatomic) IBOutlet UILabel *imageReceivedTick;
@property (weak, nonatomic) IBOutlet UILabel *processingRequestTick;
@property (weak, nonatomic) IBOutlet UILabel *preparingQuoteTick;

@end

@implementation QuoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  self.imageReceivedTick.alpha = 0.0f;
  self.processingRequestTick.alpha = 0.0f;
  self.preparingQuoteTick.alpha = 0.0f;
  [self startAnimating];
}


- (void)startAnimating {
  [UIView animateWithDuration:1.0 delay:0.0f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
    self.imageReceivedTick.alpha = 1.0;
  } completion:^(BOOL finished) {
    [UIView animateWithDuration:1.0 delay:10.0f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
      self.processingRequestTick.alpha = 1.0;
    } completion:^(BOOL finished) {
      [UIView animateWithDuration:1.0 delay:15.0f options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        self.preparingQuoteTick.alpha = 1.0;
      } completion:nil];
      
    }];
    
  }];
  
  UILocalNotification *notification = [[UILocalNotification alloc] init];
  notification.fireDate = [NSDate dateWithTimeIntervalSinceNow:10];
  notification.alertBody = @"Your Print estimate is ready. It will take 4 hours and $50. Slide to purchase.";
  notification.alertTitle = @"YAYY!!!";
  notification.soundName = UILocalNotificationDefaultSoundName;
  [[UIApplication sharedApplication] scheduleLocalNotification:notification];

 
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
