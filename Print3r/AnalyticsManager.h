//
//  AnalyticsManager.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/19/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AnalyticsManager : NSObject

typedef enum{
  AnalyticEventTypeSeeTutorial,
  AnalyticEventTypeAddPrinter,
  AnalyticEventTypePrint,
  AnalyticEventTypeGetFiles,
}AnalyticEventType;

+ (AnalyticsManager *)sharedManager;

- (void)logEvent:(AnalyticEventType)type withParams:(NSDictionary *)params;

@end
