//
//  PrintStats.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/16/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "PrintStats.h"

@implementation PrintStats
- (PrintStats *)initWithData:(NSDictionary *)data {
  self = [super init];
  if(self) {
    if(data[@"failure"]) {
      self.failure = data[@"failure"];
    }
    if(data[@"success"]) {
      self.success = data[@"success"];
    }
    if(![data[@"last"] isKindOfClass:[NSNull class]]) {
      self.lastRanDate = [data valueForKeyPath:@"last.date"];
        self.lastRanStatus = [[data valueForKeyPath:@"last.success"] boolValue];
    }
  }
  return self;
}
@end
