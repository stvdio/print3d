//
//  UserDefaultsManager.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 3/9/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "UserDefaultsManager.h"

@interface UserDefaultsManager()
@property (nonatomic, strong) NSUserDefaults *standardDefaults;
@end

@implementation UserDefaultsManager

static UserDefaultsManager *sharedManager;

+ (instancetype)sharedManager {
  
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedManager = [[UserDefaultsManager alloc] init];
    sharedManager.standardDefaults = [NSUserDefaults standardUserDefaults];
  });
  
  return sharedManager;
}

- (void)setBoolValue:(BOOL)value forKey:(NSString *)key {
  [self.standardDefaults setBool:value forKey:key];
  [self.standardDefaults synchronize];
}


- (void)setObjectValue:(id)value forKey:(NSString *)key {
  [self.standardDefaults setValue:value forKey:key];
  [self.standardDefaults synchronize];
}

- (BOOL)boolValueForKey:(NSString *)key {
  return [self.standardDefaults boolForKey:key];
}

- (NSString *)stringValueForKey:(NSString *)key {
  return [self.standardDefaults stringForKey:key];
}
@end
