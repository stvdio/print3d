//
//  Filament.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/16/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "Filament.h"

@implementation Filament
- (Filament *)initWithData:(NSDictionary *)data {
  self = [super init];
  if(self) {
    if([data valueForKeyPath:@"tool0.length"]) {
      self.length =[data valueForKeyPath:@"tool0.length"];
    }
    if([data valueForKeyPath:@"tool0.volume"]) {
      self.volume = [data valueForKeyPath:@"tool0.volume"];
    }
  }
  return self;
}
@end
