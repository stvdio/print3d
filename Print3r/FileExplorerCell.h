//
//  FileExplorerCell.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 3/14/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Printer.h"
#import "PrinterFile.h"

@interface FileExplorerCell : UITableViewCell
- (void)configForPrinter:(PrinterFile *)file;
@end
