//
//  Filament.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/16/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Filament : NSObject
@property(nonatomic, strong)NSNumber *length;
@property(nonatomic, strong)NSNumber *volume;

-(Filament *)initWithData:(NSDictionary *)data;

@end
