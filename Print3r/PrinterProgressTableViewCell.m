//
//  PrinterProgressTableViewCell.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 4/8/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "PrinterProgressTableViewCell.h"
#import "UIView+Layout.h"
#import "UIColor+HexColors.h"
#import "Print3RUtilities.h"

@interface PrinterProgressTableViewCell()

@property (weak, nonatomic) IBOutlet UIView *progressBarContainer;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) UIView *progressView;
@property (weak, nonatomic) IBOutlet UILabel *percentageLabel;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;
@property (nonatomic, assign) CGFloat percentage;
@end

@implementation PrinterProgressTableViewCell

- (void)awakeFromNib {
    // Initialization code
  [super awakeFromNib];
  self.progressView = [[UIView alloc] initWithFrame:CGRectZero];
  [self.progressBarContainer addSubview:self.progressView];
  [self.progressBarContainer bringSubviewToFront:self.percentageLabel];
  self.progressBarContainer.layer.cornerRadius = 8.0f;
  self.progressBarContainer.layer.masksToBounds = YES;
  self.stateLabel.textColor = [UIColor whiteColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configForPrinter:(Printer *)printer {
  [super configForPrinter:printer];
  self.progressView.width = 0;
  
  if(printer.printerState.error || (printer.didAskForPrinterState && !printer.printerState)) {
    self.stateLabel.text = @"Error!";
    self.percentageLabel.text = @"";
    self.percentage = 0;
    return;
  }
  self.stateLabel.text = printer.printerState.stateString ? : @"";
  if(printer.jobInfo && (printer.printerState.printing || printer.printerState.paused)) {
    NSString *completion = printer.jobInfo.progress.completion ? [NSString stringWithFormat:@"%.02f %%",printer.jobInfo.progress.completion] : @"0 %%";
    self.percentageLabel.text = completion;
    self.percentage = printer.jobInfo.progress.completion/100;
    self.progressView.backgroundColor = [printer.printerState colorForCurrentState];
  }
  else {
    self.percentageLabel.text = @"";
    self.percentage = 0.0f;
  }
  [UIView animateWithDuration:1.0 animations:^{
    self.progressView.width = self.progressBarContainer.width * self.percentage;
  }];
}

- (void)layoutSubviews {
  [super layoutSubviews];

  self.progressView.height = self.progressBarContainer.height;
  self.progressView.width = self.progressBarContainer.width * self.percentage;

}

@end
