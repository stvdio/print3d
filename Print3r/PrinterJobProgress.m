//
//  PrinterJobProgress.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/19/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "PrinterJobProgress.h"

@implementation PrinterJobProgress
- (PrinterJobProgress *)initWithData:(NSDictionary *)data {
  self = [super init];
  
  if(self) {
    if(data[@"completion"] && ![data[@"completion"] isMemberOfClass:[NSNull class]]) {
      self.completion = [[data valueForKey:@"completion"] floatValue];
    }
    
    if(data[@"filepos"]) {
      self.filePos = [data valueForKey:@"filepos"];
    }
    
    if(data[@"printTime"]) {
      self.printTime = [data valueForKey:@"printTime"];
    }
    
    if(data[@"printTimeLeft"]) {
      self.printTimeLeft = [data valueForKey:@"printTimeLeft"];
    }
  }
  return self;
}

- (instancetype)initDemoJobProgress {
  self = [super init];
  if (self) {
    self.completion = 75.0f;
    self.printTime = [NSNumber numberWithFloat:4012];
    self.printTimeLeft = [NSNumber numberWithFloat:1240];
  }
  return self;
}
- (NSString *)humanReadableTotalPrintTime {
  return [self humanReadableTimeForNumber:self.printTime];
}

- (NSString *)humanReadablePrintTimeLeft {
  return [self humanReadableTimeForNumber:self.printTimeLeft];
}

- (NSString *)humanReadableTimeForNumber:(NSNumber *)number {
  NSString *time = @" -";
  if(number && ![number isKindOfClass:[NSNull class]]) {
    NSUInteger h, m, s;
    h = ([number intValue] / 3600);
    m = ((NSUInteger)([number intValue] / 60)) % 60;
    s = ((NSUInteger) [number intValue]) % 60;
    time = [NSString stringWithFormat:@" %lu:%02lu:%02lu", (unsigned long)h, (unsigned long)m, (unsigned long)s];
  }
  return time;
}

@end
