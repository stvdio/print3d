//
//  PrinterJobProgress.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/19/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PrinterJobProgress : NSObject
@property (nonatomic, assign) float completion;
@property (nonatomic, strong) NSNumber *filePos;
@property (nonatomic, strong) NSNumber *printTime;
@property (nonatomic, strong) NSNumber *printTimeLeft;


- (PrinterJobProgress *)initWithData:(NSDictionary *)data;
- (instancetype)initDemoJobProgress;

- (NSString *)humanReadableTotalPrintTime;
- (NSString *)humanReadablePrintTimeLeft;
@end
