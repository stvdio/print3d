//
//  AnalyticsManager.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/19/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "AnalyticsManager.h"

@implementation AnalyticsManager

+ (AnalyticsManager *)sharedManager {
  static AnalyticsManager *sharedManaer;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedManaer = [[AnalyticsManager alloc] init];
  });
  
  return sharedManaer;
}

- (NSString *)valueForEventType:(AnalyticEventType)type {
  switch (type) {
    case AnalyticEventTypeSeeTutorial:
      return @"tutorial";
      break;
    case AnalyticEventTypeAddPrinter:
      return @"add_printer";
    default:
      return @"";
  }
}

- (void)logEvent:(AnalyticEventType)type withParams:(NSDictionary *)params {
  
  //[Flurry logEvent:[self valueForEventType:type] withParameters:params];
  
}

@end
