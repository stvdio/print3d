//
//  CachedLinearInterpolation.h
//  Draw3DPrint
//
//  Created by Vaibhav Aggarwal on 2/8/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CachedLinearInterpolation : UIView

@property(nonatomic,readonly)NSMutableArray *dataPoints;
/*
 * Clears the graphic context on the screen
*/
- (void)clearScreen;

@end
