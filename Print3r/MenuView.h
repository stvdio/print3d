//
//  MenuView.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 5/30/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, MenuOption) {
  MenuOptionFileExplorer,
  MenuOptionHeadControl,
  MenuOptionCamera,
  MenuOptionOctoprint,
  MenuOptionSettings,
  MenuOptionNone
};

@protocol MenuViewProtocol <NSObject>

- (void)menuOptionPressedWithOption:(MenuOption)option;

@end

@interface MenuView : UIView
@property (nonatomic, weak)id<MenuViewProtocol> delegate;
@end
