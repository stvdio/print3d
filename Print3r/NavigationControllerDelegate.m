//
//  NavigationControllerDelegate.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/23/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "NavigationControllerDelegate.h"
#import "TransitionAnimator.h"
#import "CustomDrawingViewController.h"

@implementation NavigationControllerDelegate

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC {
  TransitionAnimator *animator = [TransitionAnimator new];
  if(operation == UINavigationControllerOperationPush) {
    animator.isPush = YES;
  }
  else {
    animator.isPush = NO;
  }
  return animator;
}

- (UIInterfaceOrientationMask)navigationControllerSupportedInterfaceOrientations:(UINavigationController *)navigationController {
  return UIInterfaceOrientationMaskAll;
}

@end
