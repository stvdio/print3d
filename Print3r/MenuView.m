//
//  MenuView.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 5/30/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "MenuView.h"
#import "Print3RUtilities.h"

@interface MenuView()
@property (weak, nonatomic) IBOutlet UIButton *fileExplorerButton;
@property (weak, nonatomic) IBOutlet UIButton *controlPannelButton;
@property (weak, nonatomic) IBOutlet UIButton *cameraButton;
@property (weak, nonatomic) IBOutlet UIButton *octoPrintButton;
@property (weak, nonatomic) IBOutlet UIButton *settingsButton;

@end

@implementation MenuView

- (void)awakeFromNib {
  [super awakeFromNib];
  [self.fileExplorerButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
  [self.controlPannelButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
  [self.cameraButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
  [self.octoPrintButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
  [self.settingsButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
  self.backgroundColor = [Print3RUtilities darkGreenColor];
}

- (void)buttonPressed:(id)sender {
  MenuOption option = MenuOptionNone;
  if([sender isEqual:self.fileExplorerButton]) {
    option = MenuOptionFileExplorer;
  }
  else if([sender isEqual:self.controlPannelButton]) {
    option = MenuOptionHeadControl;
  }
  else if([sender isEqual:self.cameraButton]) {
    option = MenuOptionCamera;
  }
  else if([sender isEqual:self.octoPrintButton]) {
    option = MenuOptionOctoprint;
  }
  else if([sender isEqual:self.settingsButton]) {
    option = MenuOptionSettings;
  }
  if([self.delegate respondsToSelector:@selector(menuOptionPressedWithOption:)]){
    [self.delegate menuOptionPressedWithOption:option];
  }
}
@end
