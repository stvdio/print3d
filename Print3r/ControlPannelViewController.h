//
//  ControlPannelViewController.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 5/9/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractPrinterViewController.h"

@interface ControlPannelViewController : AbstractPrinterViewController

@end
