//
//  UserDefaultsManager.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 3/9/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString * const kHasSeenWelcomeScreen = @"has_seen_welcome_screen";

@interface UserDefaultsManager : NSObject

+ (instancetype)sharedManager;

- (void)setBoolValue:(BOOL)value forKey:(NSString *)key;

- (void)setObjectValue:(id)value forKey:(NSString *)key;

- (BOOL)boolValueForKey:(NSString *)key;

- (NSString *)stringValueForKey:(NSString *)key;



@end
