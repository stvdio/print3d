//
//  ParallaxViewController.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 3/8/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "ParallaxViewController.h"
#import "ParallaxCoverCell.h"
#import "UIView+Layout.h"

#define kParallaxAmount   30.0

// Change this to however big you want your cells to be.
#define kAlbumCoverHeight 82.0

// Leave this. The width will be the same as the height (a square)
// plus the parallax amount specified for each side.
#define kAlbumCoverWidth  (kAlbumCoverHeight + 2 * kParallaxAmount)
@interface ParallaxViewController ()

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UICollectionViewFlowLayout *collectionViewLayout;
@property (nonatomic, strong) NSTimer *scrollTimer;


@end

@implementation ParallaxViewController

- (void)viewDidLoad {
  
  [super viewDidLoad];
  
  [[self view] setBackgroundColor:[UIColor blackColor]];
  
  self.collectionViewLayout = [[UICollectionViewFlowLayout alloc] init];
  [self.collectionViewLayout setItemSize:CGSizeMake(kAlbumCoverWidth, kAlbumCoverHeight)];
  [self.collectionViewLayout setMinimumLineSpacing:-kParallaxAmount];
  [self.collectionViewLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
  
  self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.collectionViewLayout];
  
  [self.collectionView registerClass:[ParallaxCoverCell class] forCellWithReuseIdentifier:kParallaxCoverCellIdent];
  [self.collectionView setDataSource:self];
  [self.collectionView setDelegate:self];
  
  [[self view] addSubview:self.collectionView];
}

- (void)viewWillLayoutSubviews {
  [super viewWillLayoutSubviews];
  
  self.collectionView.center = self.view.center;
  self.collectionView.width = self.view.width;
  self.collectionView.x = 0;
  self.collectionView.height = self.view.height/4;
  
}

/*
 * Computes the location of a cell in the collectionView on a scale of -1 to 1
 *    -1 : The cell is all the way left
 *     0 : The cell is directly centered
 *     1 : The cell is all the way right
 */
- (CGFloat)parallaxPositionForCell:(UICollectionViewCell *)cell {
  
  CGRect frame = [cell frame];
  CGPoint point = [[cell superview] convertPoint:frame.origin toView:self.collectionView];
  
  const CGFloat minX = CGRectGetMinX([self.collectionView bounds]) - frame.size.width; // off screen to the left
  const CGFloat maxX = CGRectGetMaxX([self.collectionView bounds]);                    // off screen to the right
  const CGFloat minPos = -1.0f;
  const CGFloat maxPos = 1.0f;
  
  // Compute the position with a linear equation.
  return (maxPos - minPos) / (maxX - minX) * (point.x - minX) + minPos;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
  
  for (ParallaxCoverCell *cell in [self.collectionView visibleCells]) {
    
    // Update the parallax position for all visible cells as we
    // are scrolling.
    CGFloat position = [self parallaxPositionForCell:cell];
    [cell setParallaxPosition:position];
  }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView_ cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  
  ParallaxCoverCell *cell;
  cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:kParallaxCoverCellIdent forIndexPath:indexPath];
  UIImage *daftPunkCover = [UIImage imageNamed:@"cover"];
  UIImageView *frontImageView = [[cell imageViews] objectAtIndex:0];
  [frontImageView setImage:daftPunkCover];
  return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  
  return 10;
}

#pragma mark - Autoscroll

- (void)viewDidAppear:(BOOL)animated {
  
  [super viewDidAppear:animated];

}

- (void)startAutoScrolling {
  
  if (!self.scrollTimer) {
    self.scrollTimer = [NSTimer scheduledTimerWithTimeInterval:0.015
                                                    target:self
                                                  selector:@selector(scroll)
                                                  userInfo:nil
                                                   repeats:YES] ;
  }
}

- (void)scroll {
  
  CGSize contentSize = [self.collectionView contentSize];
  CGRect collectionRect = [self.collectionView frame];
  CGPoint offset = [self.collectionView contentOffset];
  CGPoint endOffset = CGPointMake(contentSize.width - collectionRect.size.width, 0);
  
  if (CGPointEqualToPoint(offset, endOffset)) {
    [self.scrollTimer invalidate];
    self.scrollTimer = nil;
  }
  
  [self.collectionView setContentOffset:CGPointMake(offset.x + 1.0, offset.y)];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
  
  // Stop the autoscrolling when touched
  if (self.scrollTimer) {
    [self.scrollTimer invalidate];
    self.scrollTimer = nil;
  }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
  
  return UIStatusBarStyleLightContent;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
