//
//  FileViewerViewController.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 3/15/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "FileViewerViewController.h"
#import "UIView+Layout.h"
#import "NetworkManager.h"
#import "Print3RUtilities.h"
#import "FileViewerMenuView.h"
#import "MGFashionMenuView.h"
#import "JGProgressHUD.h"

static NSString *const kAlertMessage = @"The requested file could not be downloaded";
static NSString *const kUpdateAlert = @"This will overwrite the original file, are you sure?";

@interface FileViewerViewController () <UITextViewDelegate, FileExplorerMenuViewProtocol, UIAlertViewDelegate>
@property (nonatomic, strong)PrinterFile *file;
@property (nonatomic, strong)UITextView *textView;
@property (nonatomic, assign)BOOL isEditing;
@property (nonatomic, assign)CGFloat keyboardHeight;
@property (nonatomic, assign)BOOL isMenuShown;
@property (nonatomic, strong)MGFashionMenuView *menu;
@property (nonatomic, strong)FileViewerMenuView *menuView;
@property (nonatomic, strong)UIBarButtonItem *moreButton;
@property (nonatomic, strong)JGProgressHUD *progressHud;
@end

@implementation FileViewerViewController

- (instancetype)initWithPrinter:(Printer *)printer File:(PrinterFile *)file {
  self = [super initWithPrinter:printer];
  if(self) {
    _file = file;
  }
  return  self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  self.title = [self.file.name stringByDeletingPathExtension];
  self.view.backgroundColor = [UIColor lightGrayColor];
  self.textView = [[UITextView alloc] init];
  self.textView.font = [UIFont fontWithName:@"Courier" size:14.0f];
  [self.view addSubview:self.textView];
  self.textView.delegate = self;
  self.textView.backgroundColor = [UIColor whiteColor];
  
  [self registerForKeyboardNotifications];
  self.progressHud = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
  self.progressHud.textLabel.text = @"Downloading File";
  [self.progressHud showInView:self.view animated:YES];

  [self downloadFile];
  
  self.moreButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_more"] style:UIBarButtonItemStylePlain target:self action:@selector(menuPressed)];
  self.navigationItem.rightBarButtonItem = self.moreButton;
  
  self.menuView = [[[NSBundle mainBundle] loadNibNamed:@"FileViewerMenuView" owner:self options:nil] firstObject];
  self.menuView.height = 120.0f;
  self.menuView.delegate = self;
  self.menu = [[MGFashionMenuView alloc] initWithMenuView:self.menuView animationType:MGAnimationTypeWave];
  [self.view addSubview:self.menu];
  
}

- (void)viewWillLayoutSubviews {
  [super viewWillLayoutSubviews];
  self.textView.frame = self.view.frame;
  self.textView.height = self.view.height - self.keyboardHeight;
  CGFloat width = self.view.width;
  self.menu.width = width;
  self.menuView.width = width;
}

- (void)downloadFile {
  __weak FileViewerViewController *weakSelf = self;
  [[NetworkManager sharedManager] downloadFileFor:self.file.downloadLocation params:nil success:^(id responseObject) {
    __strong FileViewerViewController *strongSelf = weakSelf;
    NSString *fileString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
    strongSelf.textView.text = fileString;
    [strongSelf.progressHud dismissAnimated:YES];
  } failure:^(NSError *error, AFHTTPRequestOperation *operation) {
    __strong FileViewerViewController *strongSelf = weakSelf;
    [Print3RUtilities showAlertWithTitle:kErrorTitle message:kAlertMessage sender:strongSelf];
    [strongSelf.progressHud dismissAnimated:YES];
  }];
}

- (void)menuPressed {
  self.isMenuShown = !self.isMenuShown;
  if(self.isMenuShown) {
    [self.menu show];
  }
  else {
    [self.menu hide];
  }
  
}

// pragma mark FileExplorerMenuViewProtocol
- (void)menuOptionPressedWithOption:(FileViewerMenuOption)option {
  
  switch (option) {
    case MenuOptionScrollTopTop:
      [self scrollToTop];
      break;
    case MenuOptionScrollToBottom:
      [self scrollToBottom];
      break;
    case MenuOptionSave:
      [self updateFilePressed];
      break;
    case MenuOptionDismissKeyboard:
      [self doneEditingPressed];
      break;
    case MenuOptionHelp:
      break;
    default:
      break;
  }
  [self.menu hide];
  self.isMenuShown=!self.isMenuShown;
}


-(void) scrollToBottom {
  [self.textView scrollRangeToVisible:NSMakeRange([self.textView.text length], 0)];
}

- (void)scrollToTop {
  [self.textView scrollRectToVisible:CGRectMake(0,0,1,1) animated:YES];
  
}

- (void)updateFilePressed {
  [self.textView resignFirstResponder];
  self.keyboardHeight = 0;
  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Alert" message:kUpdateAlert delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
  [alertView show];
}

- (void)doneEditingPressed {
  if([self.textView isFirstResponder]) {
    [self.textView resignFirstResponder];
    self.keyboardHeight = 0;
  }
  else {
    [self.textView becomeFirstResponder];
  }
  
}

- (void)registerForKeyboardNotifications {
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(keyboardWasShown:)
                                               name:UIKeyboardDidShowNotification object:nil];
  
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(keyboardWillBeHidden:)
                                               name:UIKeyboardWillHideNotification object:nil];
  
}

#pragma AlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  if(buttonIndex == 1) {
    self.progressHud.textLabel.text = @"Updating File";
    [self.progressHud showInView:self.view animated:YES];
    NSData *fileData = [self.textView.text dataUsingEncoding:NSUTF8StringEncoding];
    __weak typeof(self) weakself = self;
    [self.printer uploadFileWithData:fileData toPrinterDirectory:@"local" withName:self.file.name withCompletion:^{
      __strong typeof(self) strongSelf = weakself;
      //strongSelf.progressHud.textLabel.text = @"Updated Successfully";
      [strongSelf.progressHud dismissAfterDelay:2.0f animated:YES];
    }];
  }
}
#pragma UITextView delegate
- (void)textViewDidBeginEditing:(UITextView *)textView {
  self.navigationItem.rightBarButtonItem.enabled = YES;
  [self.navigationItem.rightBarButtonItem setTintColor: nil];
  
}
- (void)textViewDidEndEditing:(UITextView *)textView {
  self.keyboardHeight = 0;
  [self.view setNeedsLayout];
  
}

#pragma Keyboard notifications
- (void)keyboardWasShown:(NSNotification*)notification {
  NSDictionary* info = [notification userInfo];
  CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
  self.textView.height = self.textView.height - keyboardSize.height;
  self.keyboardHeight = keyboardSize.height;
}

- (void)keyboardWillBeHidden:(NSNotification*)notification {
  self.keyboardHeight = 0;
  [self.view setNeedsLayout];
  
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
  self.moreButton.enabled = NO;

}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
  self.moreButton.enabled = YES;
 
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
  if(!scrollView.dragging && !decelerate) {
    self.moreButton.enabled = YES;
  }
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

@end
