//
//  GCodeVisualizer.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 3/1/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "GCodeVisualizer.h"
#import <WebKit/WebKit.h>

@interface GCodeVisualizer()
@property (strong, nonatomic)WKWebView *webView;
@end

@implementation GCodeVisualizer

- (void)viewDidLoad {
  
//  self.webView = [[UIWebView alloc] init];
//  self.webView.frame = self.view.frame;
//  self.webView.scalesPageToFit = YES;
//  self.webView.backgroundColor = [UIColor clearColor];
//  [self.view addSubview:self.webView];
//  
//  NSURL *url = [[NSBundle mainBundle] URLForResource:@"index" withExtension:@"html"];
//  NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//  NSDictionary *headers = @{@"Access-Control-Allow-Origin" : @"*", @"Access-Control-Allow-Headers" : @"Content-Type"};
//  [request setAllHTTPHeaderFields:headers];
//  [self.webView loadRequest:request];
  
  [self getTerminalOutput];

}

- (void)getTerminalOutput {
  WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
  WKUserContentController *controller = [[WKUserContentController alloc] init];
  configuration.userContentController = controller;
  
  NSURL *jsbin = [[NSBundle mainBundle] URLForResource:@"index" withExtension:@"html"];
  self.webView = [[WKWebView alloc] initWithFrame:self.view.frame
                                    configuration:configuration];
  
  [self.webView loadRequest:[NSURLRequest requestWithURL:jsbin]];
  [self performSelector:@selector(injectScript) withObject:self afterDelay:5.0];
}

- (void)injectScript{
#define STRINGIFY(js) #js
  NSBundle *bundle = [NSBundle bundleForClass:[self class]];
  NSString *filePath = [bundle pathForResource:@"part" ofType:@"gcode"];
  
  NSFileManager *defaultManager = [NSFileManager defaultManager];
  NSString *newStr;
  if ([defaultManager fileExistsAtPath:filePath]) {
    NSError *error = NULL;
    
    NSData *data = [NSData dataWithContentsOfFile:filePath options:NSDataReadingUncached error:&error];
    newStr = [NSString stringWithUTF8String:[data bytes]];
  }
  
  NSString *exec_template =  @STRINGIFY( var vb = function(){
    openGCodeFromText("");
    return 4;
  };
                                        vb();
                                        );
  
  __weak typeof(self) weakSelf = self;
  __block NSString *result;
  [self.webView evaluateJavaScript:exec_template completionHandler:^(id response, NSError *error) {
    __strong typeof(self) strongSelf = weakSelf;
    result = response;
    if([result isEqualToString:@""] || !result){
      [strongSelf injectScript];
    }
    else {
      NSLog(@"YAYYY");
    }
  }];
}

@end
