//
//  ParallaxViewController.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 3/8/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParallaxViewController : UIViewController  <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, readonly) UICollectionView *collectionView;
@property (nonatomic, readonly) UICollectionViewFlowLayout *collectionViewLayout;
@property (nonatomic, readonly) NSTimer *scrollTimer;

@end
