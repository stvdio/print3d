//
//  Print3RUtilities.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/23/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "UIColor+HexColors.h"

static NSString *const kErrorTitle = @"Error";
static NSString *const kSuccessTitle = @"Alert";

@interface Print3RUtilities : NSObject

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message sender:(id)sender;

//fonts
+ (UIFont *)themeRegularFontWithSize:(CGFloat)size;
+ (UIFont *)themeLightFontWithSize:(CGFloat)size;
+ (UIFont *)themeBoldFontWithSize:(CGFloat)size;

// colors
+ (UIColor *)defaultThemeColor;
+ (UIColor *)defaultYellowColor;
+ (UIColor *)defaultOrangeColor;
+ (UIColor *)lightGreenColor;
+ (UIColor *)lighterGreenColor;
+ (UIColor *)lightestGreenColor;
+ (UIColor *)whiteGreenColor;
+ (UIColor *)themeBlackColor;
+ (UIColor *)darkGreenColor;

# pragma mark - hasSeen stuff

+ (BOOL)hasSeenSlideForOptions;
+ (void)setHasSeenSlideForOptions;
@end
