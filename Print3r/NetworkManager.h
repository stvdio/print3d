//
//  NetworkManager.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/14/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Printer.h"
#import "AFNetworking.h"

@interface NetworkManager : NSObject

typedef void (^ServiceFailureBlock)(NSError *error,AFHTTPRequestOperation *operation);
typedef void (^ServiceSuccessHandler)(id responseObject);


+ (NetworkManager *)sharedManager;
- (void)clearHeaderForKey:(NSString *)key;
- (void)updateManagerForPrinter:(Printer *)printer;

- (void)performGETOperationForPath:(NSString *)path
                            params:(NSDictionary *)params
                           success:(ServiceSuccessHandler)completion
                           failure:(ServiceFailureBlock)failure;

- (void)performPOSTOperationForPath:(NSString *)path
                             params:(NSDictionary *)params
                            success:(ServiceSuccessHandler)completion
                            failure:(ServiceFailureBlock)failure;


- (void)performGEToperationForPath:(NSString *)path
                            params:params
                         headerKey:(NSString *)headerKey
                       headerValue:(NSString *)headerValue
                           success:(ServiceSuccessHandler)completion
                           failure:(ServiceFailureBlock)failure;

- (void)performDeleteOperationForPath:(NSString *)path
                               params:(NSDictionary *)params
                              success:(ServiceSuccessHandler)completion
                              failure:(ServiceFailureBlock)failure;

- (void)uploadFileWithData:(NSData *)fileData atPath:(NSString *)path withName:(NSString *)name
                   success:(ServiceSuccessHandler)completion
                   failure:(ServiceFailureBlock)failure;
  
- (void)uploadFile:(NSString *)filePath atPath:(NSString *)path WithName:(NSString *)name
                 success:(ServiceSuccessHandler)completion
                 failure:(ServiceFailureBlock)failure;


- (void)downloadFileFor:(NSString *)path
                 params:(NSDictionary *)params
                success:(ServiceSuccessHandler)completion
                failure:(ServiceFailureBlock)failure;

@end
