//
//  LocalNotificationManager.m
//  TimeSynch
//
//  Created by Vaibhav Aggarwal on 3/22/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "LocalNotificationManager.h"

@implementation LocalNotificationManager

static LocalNotificationManager *sharedManager;

+ (instancetype)sharedManager {

  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedManager = [[LocalNotificationManager alloc] init];
  });
  return sharedManager;
  
}

- (void)setLocalNotificationForFile:(PrinterFile *)file {
  if(!file) {
    // alert notification could not be set
    return;
  }
  UILocalNotification *localNotification = [[UILocalNotification alloc]init];
  localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:[file.gcodeAnalysis.estimatedPrintTime floatValue]];
  localNotification.alertBody = [NSString stringWithFormat:@"Based on OctoPrint's estimate, %@ should be done printing", file.name];
  localNotification.soundName = UILocalNotificationDefaultSoundName;
  [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

@end
