//
//  CachedLinearInterpolation.m
//  Draw3DPrint
//
//  Created by Vaibhav Aggarwal on 2/8/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "CachedLinearInterpolation.h"

@interface CachedLinearInterpolation()

@property(nonatomic,strong)UIBezierPath *path;
@property(nonatomic,strong)UIImage *incrementalImage;
@property(nonatomic,strong)NSMutableArray *dataPoints;

@end


@implementation CachedLinearInterpolation

- (id)init {
  self = [super init];
  if(self) {
    [self setMultipleTouchEnabled:NO];
    [self setBackgroundColor:[UIColor whiteColor]];
    _path = [UIBezierPath bezierPath];
    [_path setLineWidth:2.0];
    _dataPoints = [NSMutableArray new];
  }
  return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
  if (self = [super initWithCoder:aDecoder])
  {
    [self setMultipleTouchEnabled:NO];
    [self setBackgroundColor:[UIColor whiteColor]];
    _path = [UIBezierPath bezierPath];
    [_path setLineWidth:2.0];
    _dataPoints = [NSMutableArray new];
  }
  return self;
}

- (void)drawRect:(CGRect)rect
{
  [self.incrementalImage drawInRect:rect];
  [self.path stroke];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
  
  // if already drawn and starting a new drawing, clear the data points and the screen
  if(self.dataPoints.count > 0) {
    [self.dataPoints removeAllObjects];
    [self clearScreen];
  }
  
  UITouch *touch = [touches anyObject];
  CGPoint p = [touch locationInView:self];
  [self.path moveToPoint:p];
  [self.dataPoints addObject:@[@(p.x),@(p.y)]];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
  UITouch *touch = [touches anyObject];
  CGPoint p = [touch locationInView:self];
  [self.path addLineToPoint:p];
  [self setNeedsDisplay];
  [self.dataPoints addObject:@[@(p.x),@(p.y)]];
  
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
  UITouch *touch = [touches anyObject];
  CGPoint p = [touch locationInView:self];
  
  [self.path addLineToPoint:p];
  [self drawBitmap];
  [self setNeedsDisplay];
  [self.path removeAllPoints];
  [self.dataPoints addObject:@[@(p.x),@(p.y)]];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
  [self touchesEnded:touches withEvent:event];
}

- (void)drawBitmap
{
  UIGraphicsBeginImageContextWithOptions(self.bounds.size, YES, 0.0);
  [[UIColor blueColor] setStroke];
  if (!self.incrementalImage)
  {
    UIBezierPath *rectpath = [UIBezierPath bezierPathWithRect:self.bounds];
    [[UIColor whiteColor] setFill];
    [rectpath fill];
  }
  [self.incrementalImage drawAtPoint:CGPointZero];
  [self.path stroke];
  self.incrementalImage = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
}

- (void)clearScreen {
  self.path   = nil;
  self.path   = [UIBezierPath bezierPath];
  self.incrementalImage = nil;
  [self setNeedsDisplay];
  [self.dataPoints removeAllObjects];
}

@end
