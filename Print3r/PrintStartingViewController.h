//
//  PrintStartingViewController.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 5/3/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PrinterFile.h"

@protocol PrintStartingProtocol <NSObject>

- (void)printFile:(PrinterFile *)file;

@end

@interface PrintStartingViewController : UIViewController

@property (nonatomic, weak)id <PrintStartingProtocol> delegate;
- (void)showForFileName:(PrinterFile *)file;

@end
