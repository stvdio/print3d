//
//  TSLocalNotificationManager.h
//  TimeSynch
//
//  Created by Vaibhav Aggarwal on 3/22/15.
//  Copyright (c) 2015 Paxcel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "PrinterFile.h"

@interface LocalNotificationManager : NSObject

+ (instancetype)sharedManager;

- (void)setLocalNotificationForFile:(PrinterFile *)file;

@end
