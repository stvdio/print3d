//
//  PrinterFile.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/16/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "PrinterFile.h"
#import <UIKit/UIKit.h>

@implementation PrinterFile
- (PrinterFile *)initWithData:(NSDictionary *)data {
  self = [super init];
  if(self) {
    if(data[@"name"]) {
      self.name = data[@"name"];
    }
    if(data[@"size"]) {
      self.size = data[@"size"];
    }
    if(data[@"date"]) {
      self.date = data[@"date"];
    }
    if(data[@"origin"]) {
      self.origin = data[@"origin"];
    }
    if(data[@"refs"]) {
      self.resourceLocation = [data valueForKeyPath:@"refs.resource"];
      self.downloadLocation = [data valueForKeyPath:@"refs.download"];
    }
    if(data[@"gcodeAnalysis"]) {
      self.gcodeAnalysis = [[GCodeAnalysis alloc] initWithData:data[@"gcodeAnalysis"]];
    }
    if(data[@"prints"]) {
      self.printStats = [[PrintStats alloc] initWithData:data[@"prints"]];
    }
    
  }
  return self;
}

- (instancetype)initDemoFile {
  self = [super init];
  if (self) {
    _name = @"Demo File";
    _size = [NSNumber numberWithInt:1000148];
  }
  return self;
}

- (NSAttributedString *)descriptionForPopUp {
  NSDictionary *attributes = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:16.0]};
  NSDictionary *lightAttributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14.0]};

  NSMutableAttributedString *description = [NSMutableAttributedString new];

  //name
  NSAttributedString *name = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n",self.name] attributes:attributes];
  [description appendAttributedString:name];
  
  // size
  NSAttributedString *size = [[NSAttributedString alloc] initWithString:@"Size:" attributes:attributes];
  [description appendAttributedString:size];
  NSAttributedString *sizeValue = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@\n",self.size ? [NSByteCountFormatter stringFromByteCount:[self.size longValue] countStyle:NSByteCountFormatterCountStyleMemory] : @"-"] attributes:lightAttributes];
  [description appendAttributedString:sizeValue];
  
  // upload Date
  NSAttributedString *uploaded = [[NSAttributedString alloc] initWithString:@"Uploaded:" attributes:attributes];
  [description appendAttributedString:uploaded];
  NSAttributedString *uploadDate = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@\n",self.date ? [NSDate dateWithTimeIntervalSince1970:[self.date longValue]] : @" -"] attributes:lightAttributes];
  [description appendAttributedString:uploadDate];
  
  // estimated print time
  NSString *time = @" -";
  if(self.gcodeAnalysis.estimatedPrintTime) {
  NSUInteger h, m, s;
  h = ([self.gcodeAnalysis.estimatedPrintTime intValue] / 3600);
  m = ((NSUInteger)([self.gcodeAnalysis.estimatedPrintTime intValue] / 60)) % 60;
  s = ((NSUInteger) [self.gcodeAnalysis.estimatedPrintTime intValue]) % 60;
  time = [NSString stringWithFormat:@" %lu:%02lu:%02lu", (unsigned long)h, (unsigned long)m, (unsigned long)s];
  }
  NSAttributedString *estimatedTime = [[NSAttributedString alloc] initWithString:@"Estimated Print Time:" attributes:attributes];
  [description appendAttributedString:estimatedTime];
  [description appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@\n",time] attributes:lightAttributes]];
  
  // filament
  NSString *length = @"-";
  if(self.gcodeAnalysis.filament.length){
    length = [NSString stringWithFormat:@"%.02fm", [self.gcodeAnalysis.filament.length floatValue]/1000 ];
  }
  NSString *volume = @"-";
  if(self.gcodeAnalysis.filament.volume) {
    volume = [NSString stringWithFormat:@"%.02fcm3", [self.gcodeAnalysis.filament.volume floatValue]];
  }
  NSAttributedString *filament = [[NSAttributedString alloc] initWithString:@"Filament:" attributes:attributes];
  [description appendAttributedString:filament];
  [description appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat: @" %@ / %@\n",length,volume] attributes:nil]];
  
  // Last Ran date
  NSAttributedString *lastPrint = [[NSAttributedString alloc] initWithString:@"Last Print:" attributes:attributes];
  [description appendAttributedString:lastPrint];
  [description appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@\n",self.printStats.lastRanDate ? [NSDate dateWithTimeIntervalSince1970:[self.printStats.lastRanDate longValue]] : @" -"] attributes:lightAttributes]];
  
  return description;
}
@end
