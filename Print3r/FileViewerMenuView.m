//
//  FileViewerMenu.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 6/6/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "FileViewerMenuView.h"
#import "Print3RUtilities.h"

@interface FileViewerMenuView ()
@property (weak, nonatomic) IBOutlet UIButton *topOfFileButton;
@property (weak, nonatomic) IBOutlet UIButton *endOfFileButton;
@property (weak, nonatomic) IBOutlet UIButton *saveFileButton;
@property (weak, nonatomic) IBOutlet UIButton *dismissButton;
@property (weak, nonatomic) IBOutlet UIButton *helpButton;

@end

@implementation FileViewerMenuView

- (void)awakeFromNib {
  [super awakeFromNib];
  [self.topOfFileButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
  [self.endOfFileButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
  [self.saveFileButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
  [self.dismissButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
  [self.helpButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    self.backgroundColor = [Print3RUtilities darkGreenColor];
}

- (void)buttonPressed:(id)sender {
  FileViewerMenuOption option = MenuOptionNone;
  if([sender isEqual:self.topOfFileButton]) {
    option = MenuOptionScrollTopTop;
  }
  else if([sender isEqual:self.endOfFileButton]) {
    option = MenuOptionScrollToBottom;
  }
  else if([sender isEqual:self.saveFileButton]) {
    option = MenuOptionSave;
  }
  else if([sender isEqual:self.dismissButton]) {
    option = MenuOptionDismissKeyboard;
  }
  else if([sender isEqual:self.helpButton]) {
    option = MenuOptionHelp;
  }
  if([self.delegate respondsToSelector:@selector(menuOptionPressedWithOption:)]){
    [self.delegate menuOptionPressedWithOption:option];
  }

}

@end
