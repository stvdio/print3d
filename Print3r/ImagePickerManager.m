//
//  ImagePickerManager.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 3/29/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "ImagePickerManager.h"
#import <MobileCoreServices/MobileCoreServices.h>

@interface ImagePickerManager()

@end

@implementation ImagePickerManager

static ImagePickerManager *sharedManager;

+ (instancetype)sharedManager {

  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedManager = [[ImagePickerManager alloc] init];
  });
  return sharedManager;
}

- (BOOL)isSourceTypeAvailable:(UIImagePickerControllerSourceType)sourceType {

  return [UIImagePickerController isSourceTypeAvailable:sourceType];
}

- (UIImagePickerController *)pickerforSourceType:(UIImagePickerControllerSourceType)sourceType {
  if(! [UIImagePickerController isSourceTypeAvailable:sourceType]) {
    return nil;
  }
  UIImagePickerController *picker = [[UIImagePickerController alloc] init];
  picker.sourceType = sourceType;
  [picker setAllowsEditing:YES];
  [picker setMediaTypes:@[(NSString *)kUTTypeImage]];
  
  return picker;
}

@end
