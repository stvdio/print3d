//
//  ParallaxCoverCell.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 3/8/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "ParallaxCoverCell.h"

@implementation ParallaxCoverCell
- (id)initWithFrame:(CGRect)frame {
  
  if ((self = [super initWithFrame:frame])) {
    
    const NSInteger numberImageViews = 5;
    const CGFloat   alphaDecrease    = 0.20;
    const CGFloat   deviceScale      = [[UIScreen mainScreen] scale];
    const CGFloat   insetAmount      = 2.0f;
    
    // We only use the height dimension for our image view. So the padding
    // on either side will be the difference in width divided by 2.
    self.parallaxAmount = (frame.size.width - frame.size.height) / 2.0;
    
    CGRect imageRect;
    imageRect = CGRectInset(frame, self.parallaxAmount, 0);
    imageRect.size.height = imageRect.size.width;
    imageRect.origin.x = 0.5 * (frame.size.width - imageRect.size.width);
    imageRect.origin.y = frame.size.height - imageRect.size.height;
    
    NSMutableArray *mutable = [NSMutableArray array];
    for (NSInteger i = 0; i < numberImageViews; i++) {
      
      if (i > 0) {
        imageRect.size.width -= insetAmount;
        imageRect.origin.y -= insetAmount;
      }
      
      UIImageView *imageView;
      imageView = [[UIImageView alloc] initWithFrame:imageRect];
      [imageView setAlpha:MAX(0.0, 1.0 - (alphaDecrease * i))];
      [[imageView layer] setBorderColor:[[UIColor whiteColor] CGColor]];
      [[imageView layer] setBorderWidth:1.0 / deviceScale];
      [[imageView layer] setShouldRasterize:YES];
      [[imageView layer] setRasterizationScale:deviceScale];
      
      [self insertSubview:imageView atIndex:0];
      [mutable addObject:imageView];
    }
    self.imageViews = [NSArray arrayWithArray:mutable];
    [self setParallaxPosition:-1];
  }
  return self;
}

- (void)setParallaxPosition:(CGFloat)position {
  
  const CGFloat minOffsetX  = -self.parallaxAmount;
  const CGFloat maxOffsetX  = self.parallaxAmount;
  
  const CGFloat minPosition = 1.0;
  const CGFloat maxPosition = -1.0;
  
  // Compute the total offset using a linear equation
  CGFloat offsetX = (maxOffsetX - minOffsetX) / (maxPosition - minPosition) * (position - minPosition) + minOffsetX;
  
  // Divide the total offset among the moving images
  offsetX /= ([self.imageViews count] - 1);
  
  // Apply the offsetX to each image relative to the first one
  CGRect fixedRect = [[self.imageViews objectAtIndex:0] frame];
  for (NSInteger i = 1; i < [self.imageViews count]; i++) {
    
    UIImageView *imageView = [self.imageViews objectAtIndex:i];
    CGRect imageRect = [imageView frame];
    CGFloat imageWidth = imageRect.size.width;
    imageRect.origin.x = CGRectGetMidX(fixedRect) - 0.5 * imageWidth + (offsetX * i);
    [imageView setFrame:imageRect];
  }
}


@end
