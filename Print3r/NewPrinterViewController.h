//
//  NewPrinterViewController.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/22/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractPrinterViewController.h"
#import "Printer.h"

@protocol AddPrinterViewDelegate <NSObject>
- (void)savePrinter:(Printer *)printer;
@end

@interface NewPrinterViewController : AbstractPrinterViewController
@property(nonatomic, weak)id <AddPrinterViewDelegate> addPrintViewDelegate;

@end
