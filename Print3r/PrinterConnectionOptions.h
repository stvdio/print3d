//
//  PrinterConnectionOptions.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/17/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PrinterConnectionOptions : NSObject
@property(strong, nonatomic) NSString *state;
@property(strong, nonatomic) NSString *currentPort;
@property(strong, nonatomic) NSNumber *currentBaudRate;
@property(strong, nonatomic) NSArray *ports;
@property(strong, nonatomic) NSArray *baudRates;
@property(strong, nonatomic) NSString *portPreference;
@property(strong, nonatomic) NSNumber *baudRatePreference;
@property(assign, nonatomic) BOOL autoConnect;


- (PrinterConnectionOptions *)initWithData:(NSDictionary *)data;

@end
