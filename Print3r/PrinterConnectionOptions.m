//
//  PrinterConnectionOptions.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/17/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "PrinterConnectionOptions.h"

@implementation PrinterConnectionOptions

- (PrinterConnectionOptions *)initWithData:(NSDictionary *)data {
  self = [super init];
  if(self) {
    if(data[@"current"]) {
      self.state = [data valueForKeyPath:@"current.state"];
      self.currentPort = [data valueForKeyPath:@"current.port"];
      self.currentBaudRate = [data valueForKeyPath:@"current.baudrate"];
    }
    if(data[@"options"]) {
      NSMutableArray *tempArray = [NSMutableArray new];
      for (NSString *port in [data valueForKeyPath:@"options.ports"]) {
        [tempArray addObject:port];
      }
      if(tempArray.count > 0) {
        self.ports = [tempArray copy];
      }
      [tempArray removeAllObjects];
      
      for (NSNumber *baudRate in [data valueForKeyPath:@"options.baudrates"]) {
        [tempArray addObject:baudRate];
      }
      if(tempArray.count > 0) {
        self.baudRates = [tempArray copy];
      }
      
      self.baudRatePreference = [data valueForKeyPath:@"options.baudratePreference"];
      self.autoConnect = [[data valueForKeyPath:@"options.autoconnect"] boolValue];
    }
  }
  return self;
}
@end
