//
//  PrinterProgressTableViewCell.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 4/8/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Printer.h"
#import "PrinterCell.h"

@interface PrinterProgressTableViewCell : PrinterCell

@end
