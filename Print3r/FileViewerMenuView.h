//
//  FileViewerMenu.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 6/6/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, FileViewerMenuOption) {
  MenuOptionScrollTopTop,
  MenuOptionScrollToBottom,
  MenuOptionSave,
  MenuOptionDismissKeyboard,
  MenuOptionHelp,
  MenuOptionNone
};

@protocol FileExplorerMenuViewProtocol <NSObject>
- (void)menuOptionPressedWithOption:(FileViewerMenuOption)option;

@end

@interface FileViewerMenuView : UIView
@property (nonatomic, strong)id <FileExplorerMenuViewProtocol> delegate;
@end
