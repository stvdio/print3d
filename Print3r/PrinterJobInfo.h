//
//  PrinterJobInfo.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/19/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PrinterJob.h"
#import "PrinterJobProgress.h"

@interface PrinterJobInfo : NSObject
@property(nonatomic,strong) PrinterJob *job;
@property(nonatomic, strong) PrinterJobProgress *progress;
@property (nonatomic, strong) NSString *state;

- (PrinterJobInfo *)initWithData:(NSDictionary *)data;
- (instancetype)initDemoPrinterJobInfo;
@end
