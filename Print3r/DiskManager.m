//
//  DiskManager.m
//  Print3r
//
//  Created by Vaibhav Aggarwal on 2/13/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "DiskManager.h"

const NSString *printerDirectory = @"printers";

@implementation DiskManager

+ (instancetype)sharedManager {
  
  static dispatch_once_t onceToken;
  static DiskManager *sharedManager;
  dispatch_once(&onceToken, ^{
    sharedManager = [[DiskManager alloc] init];
  });
  
  return sharedManager;
}

- (NSArray *)getAllPrinters {
  
  NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
  NSString *userPath = [documentsPath stringByAppendingPathComponent:@"user"];
  NSArray *printers = [NSKeyedUnarchiver unarchiveObjectWithFile:[NSString stringWithFormat:@"%@/%@",userPath,printerDirectory]];
  return printers;
}

- (BOOL)savePrinter:(Printer *)printer {
  
  NSMutableArray *allPrinters = [[[DiskManager sharedManager] getAllPrinters] mutableCopy];
  if(printer) {
    // make sure the printer is not same
    BOOL doesExist = NO;
    for(id savedPrinter in allPrinters) {
      Printer *p = (Printer *)savedPrinter;
      if([p.hostURL isEqualToString:printer.hostURL]) {
        doesExist = YES;
      }
    }
    if(!doesExist) {
      if(!allPrinters) {
        allPrinters = [NSMutableArray new];
      }
      [allPrinters addObject:printer];
      [[DiskManager sharedManager] savePrinters:allPrinters];
      return YES;
    }
    return NO;
  }
  return NO;
  
}

- (void)savePrinters:(NSArray *)printers {
  
  NSFileManager *fileManager = [NSFileManager defaultManager];
  NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
  NSString *userPath = [documentsPath stringByAppendingPathComponent:@"user"];
  
  if (![fileManager fileExistsAtPath:userPath]) {
    [fileManager createDirectoryAtPath:userPath withIntermediateDirectories:YES attributes:nil error:nil];
  }
  [NSKeyedArchiver archiveRootObject:printers toFile:[NSString stringWithFormat:@"%@/%@",userPath,printerDirectory]];
}


- (BOOL)editPrinter:(Printer *)printer withOriginalHost:(NSString *)originalHost{
  NSMutableArray *allPrinters = [[[DiskManager sharedManager] getAllPrinters] mutableCopy];
  
  BOOL didReplace = NO;
  for(id savedPrinter in allPrinters) {
    Printer *p = (Printer *)savedPrinter;
    if([p.hostURL isEqualToString:originalHost]) {
      NSUInteger index = [allPrinters indexOfObject:savedPrinter];
      [allPrinters replaceObjectAtIndex:index withObject:printer];
      didReplace = YES;
      break;
    }
  }
  if(didReplace) {
    [[DiskManager sharedManager] savePrinters:allPrinters];
  }
  return didReplace;
}

- (void)deletePrinter:(Printer *)printer {
  NSArray *printers = [[[DiskManager sharedManager] getAllPrinters] mutableCopy];
  NSMutableArray *allPrinters = [printers mutableCopy];
  for(Printer *savedPrinter in printers) {
    if ([savedPrinter.hostURL isEqualToString:printer.hostURL] && [savedPrinter.apiKey isEqualToString:printer.apiKey]) {
      [allPrinters removeObject:savedPrinter];
    }
  }
  [[DiskManager sharedManager] savePrinters:allPrinters];
  
}

- (BOOL)doesPrinterExist:(Printer *)printer {
  
  NSMutableArray *allPrinters = [[[DiskManager sharedManager] getAllPrinters] mutableCopy];
  
  for(id savedPrinter in allPrinters) {
    Printer *p = (Printer *)savedPrinter;
    if([p.hostURL isEqualToString:printer.hostURL]) {
      return YES;
    }
  }
  
  return NO;
}

- (void)saveUserPreference:(NSString *)preference forKey:(NSString *)key {
  NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
  [userDefaults setObject:preference forKey:key];
  [userDefaults synchronize];
  
}

- (NSString *)userPreferenceForKey:(NSString *)key {
  NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
  if([userDefaults objectForKey:key]) {
    return [userDefaults objectForKey:key];
  }
  return nil;
}

@end
