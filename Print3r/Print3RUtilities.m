//
//  Print3RUtilities.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/23/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "Print3RUtilities.h"

static NSString* const kSlideToSeeOptions = @"slide_to_see_options";

@implementation Print3RUtilities

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message sender:(id)sender{

  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:sender cancelButtonTitle:@"Ok" otherButtonTitles:nil];
  [alertView show];
}

+ (UIFont *)themeRegularFontWithSize:(CGFloat)size {
  return [UIFont fontWithName:@"OpenSans" size:size];
}

+ (UIFont *)themeLightFontWithSize:(CGFloat)size {
  return [UIFont fontWithName:@"OpenSans-Light" size:size];
}

+ (UIFont *)themeBoldFontWithSize:(CGFloat)size {
  return [UIFont fontWithName:@"OpenSans-Bold" size:size];
}

+ (UIColor *)defaultThemeColor {
  return [UIColor colorWithHexString:@"2BB673"];
}

+ (UIColor *)defaultYellowColor {
  return [UIColor colorWithHexString:@"F9ED32"];
}

+ (UIColor *)defaultOrangeColor {
  return [UIColor colorWithHexString:@"F28705"];
}

+ (UIColor *)lightGreenColor {
  return [UIColor colorWithHexString:@"67C18C"];
}

+ (UIColor *)lighterGreenColor {
  return [UIColor colorWithHexString:@"8FCEA5"];
}

+ (UIColor *)lightestGreenColor {
  return [UIColor colorWithHexString:@"B4DDC0"];
}

+ (UIColor *)whiteGreenColor {
  return [UIColor colorWithHexString:@"D8EDDD"];
}

+ (UIColor *)themeBlackColor {
  return [UIColor colorWithHexString:@"231F20"];
}

+ (UIColor *)darkGreenColor {
  return [UIColor colorWithHexString:@"155A3A"];
}

+ (BOOL)isLandScape {
  return UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation]);
}

# pragma mark hasSeen Stuff
+ (BOOL)hasSeenSlideForOptions {
  return [[NSUserDefaults standardUserDefaults] boolForKey:kSlideToSeeOptions];
}

+ (void)setHasSeenSlideForOptions {
  [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kSlideToSeeOptions];
  [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
