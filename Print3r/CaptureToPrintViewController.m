//
//  CaptureToPrintViewController.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 4/4/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "CaptureToPrintViewController.h"
#import "ImagePickerManager.h"
#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "Print3RUtilities.h"
#import "QuoteViewController.h"

@interface CaptureToPrintViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *getAQuote;
@property (weak, nonatomic) IBOutlet UIButton *reTake;
@property (strong, nonatomic) UIImage *image;
@property (assign, nonatomic) NSInteger cameraPhotoIndex;
@property (assign, nonatomic) NSInteger galleryPhotoIndex;
@end

@implementation CaptureToPrintViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
  [self.reTake addTarget:self action:@selector(takeImageClicked) forControlEvents:UIControlEventTouchUpInside];
  [self.getAQuote addTarget:self action:@selector(getAQuoteClicked) forControlEvents:UIControlEventTouchUpInside];
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getAQuoteClicked {
//  if(!self.image) {
//    [Print3RUtilities showAlertWithTitle:kErrorTitle message:@"You gotta have an image!" sender:self];
//    return;
//  }
  QuoteViewController *vc = [[QuoteViewController alloc] init];
  [self.navigationController pushViewController:vc animated:YES];
}

- (void)takeImageClicked {
  UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose an image from" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles: nil];
  actionSheet.delegate = self;
  self.cameraPhotoIndex = self.galleryPhotoIndex = -1;
  if([[ImagePickerManager sharedManager] isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
    self.cameraPhotoIndex = [actionSheet addButtonWithTitle:@"Camera"];
  }
  if([[ImagePickerManager sharedManager] isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
    self.galleryPhotoIndex = [actionSheet addButtonWithTitle:@"Gallery"];
  }
  [actionSheet setCancelButtonIndex:[actionSheet addButtonWithTitle:@"Cancel"]];
  
  if(self.cameraPhotoIndex != -1 || self.galleryPhotoIndex != -1) {
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet showInView:self.view];
  }
}

#pragma mark UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
  
  if(buttonIndex == -1 || buttonIndex == [actionSheet cancelButtonIndex]) {
    return;
  }
  
  UIImagePickerController *picker;
  
  if(buttonIndex == self.cameraPhotoIndex) {
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if( authStatus != AVAuthorizationStatusAuthorized) {
      [Print3RUtilities showAlertWithTitle:kErrorTitle message:@"You need to grant access to the camera" sender:self];
      return;
    }
    picker = [[ImagePickerManager sharedManager] pickerforSourceType:UIImagePickerControllerSourceTypeCamera];
  }
  else if(buttonIndex == self.galleryPhotoIndex) {
    picker = [[ImagePickerManager sharedManager] pickerforSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
  }
  picker.delegate = self;
  picker.allowsEditing = NO;
  [self presentViewController:picker animated:YES completion:nil];
  
}

#pragma mark UIImagePickerControllerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
  
  NSString *mediaType = info[UIImagePickerControllerMediaType];
  UIImage *image = nil;
  
  if (CFStringCompare((__bridge CFStringRef)mediaType, kUTTypeImage, 0) == kCFCompareEqualTo) {
    image = (UIImage *)info[UIImagePickerControllerEditedImage];
    if (!image)
      image = (UIImage *)info[UIImagePickerControllerOriginalImage];
  }
  self.imageView.image = image;
  self.image = image;
  [self dismissViewControllerAnimated:YES completion:nil];
  
}

@end
