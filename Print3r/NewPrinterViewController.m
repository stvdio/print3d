//
//  NewPrinterViewController.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/22/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "NewPrinterViewController.h"
#import "UIView+Additions.h"
#import "UIView+Layout.h"
#import "Printer.h"
#import "Print3RUtilities.h"
#import "ImagePickerManager.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>
#import "UIColor+HexColors.h"
#import "AnalyticsManager.h"
#import "PrinterVersion.h"
#import "JFMinimalNotification.h"
#import "DiskManager.h"
#import "WebViewController.h"

static CGFloat const kImageDimension = 180.0f;
static CGFloat const kTextFielHeight = 30.0f;
static CGFloat const kMargin = 24.0f;
static NSString * const kVerifying = @"Verifying...";
static NSString * const kVerified = @"Verified";
static NSString * const kCouldNotVerify = @"Could not verify printer";
static NSString * const kHostApiCannotBeBlank = @"The Host URL or API Key cannot be blank";
static NSString * const kPrinterAlreadyExists = @"A Printer with same host already exists";

@interface NewPrinterViewController () <UITextFieldDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, JFMinimalNotificationDelegate>
@property (strong, nonatomic) UITextField *nameTextField;
@property (strong, nonatomic) UITextField *webHostTextField;
@property (strong, nonatomic) UITextField *apiKeyTextField;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (weak, nonatomic) UITextField *activeField;
@property (assign, nonatomic) NSInteger cameraPhotoIndex;
@property (assign, nonatomic) NSInteger galleryPhotoIndex;
@property (strong, nonatomic) UIImage *printerImage;
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UILabel *hostNameLabel;
@property (strong, nonatomic) UILabel *apiKeyLabel;
@property (assign, nonatomic) BOOL isWebHostValid;
@property (strong, nonatomic) PrinterVersion *printerVersion;
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) JFMinimalNotification *minimalNotification;
@property (strong, nonatomic) UIButton *octoprintButton;
@property (strong, nonatomic) UILabel *printerBedSettingsLabel;
@property (strong, nonatomic) UILabel *crossLabel;
@property (strong, nonatomic) UITextField *lengthTextField;
@property (strong, nonatomic) UITextField *breadthTextField;
@end

@implementation NewPrinterViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  [self setup];
  [self curveAllTextFields];
  [self registerForKeyboardNotifications];
  self.title = @"Add a printer";
  UIBarButtonItem *add = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Check"] style:UIBarButtonItemStylePlain target:self action:@selector(donePressed)];
  
  self.navigationItem.rightBarButtonItem = add;
  UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Delete"] style:UIBarButtonItemStylePlain target:self action:@selector(cancelPressed)];
  self.navigationItem.leftBarButtonItem = cancel;
}

- (void)setup {
  
  self.view.backgroundColor = [Print3RUtilities darkGreenColor];
  self.scrollView = [UIScrollView newWithSuperview:self.view];
  self.scrollView.backgroundColor = [UIColor clearColor];
  
  self.imageView = [UIImageView newWithSuperview:self.scrollView];
  self.imageView.clipsToBounds = YES;
  CAShapeLayer *layer = [CAShapeLayer layer];

  UIBezierPath *path = [UIBezierPath bezierPath];
  [path moveToPoint:CGPointMake(0, kImageDimension)]; // bottom left corner
  CGFloat maxWidth = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 1024 : 700;
  [path addLineToPoint:CGPointMake(maxWidth, kImageDimension)]; // bottom right
  [path addLineToPoint:CGPointMake(maxWidth, kImageDimension - kMargin * 2)]; // top right
  [path closePath];
  
  layer.path = path.CGPath;
  layer.fillColor = [Print3RUtilities darkGreenColor].CGColor;
  layer.strokeColor = nil;

  [self.imageView.layer addSublayer:layer];
  self.imageView.backgroundColor = [Print3RUtilities defaultYellowColor];
  [self.imageView setImage:[UIImage imageNamed:@"Camera"]];
  self.imageView.contentMode = UIViewContentModeCenter;
  self.imageView.userInteractionEnabled = YES;
  UITapGestureRecognizer *imageTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(chooseImageClicked)];
  imageTap.numberOfTapsRequired = 1;
  [self.imageView setUserInteractionEnabled:YES];
  [self.imageView addGestureRecognizer:imageTap];
  
  
  self.nameTextField = [UITextField newWithSuperview:self.scrollView];
  self.nameTextField.textColor = [Print3RUtilities lighterGreenColor];
  self.nameTextField.placeholder = @"Nickname";
  self.nameTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Nickname" attributes:@{NSForegroundColorAttributeName: [Print3RUtilities lighterGreenColor]}];
  self.nameTextField.delegate = self;
  
  self.hostNameLabel = [UILabel newWithSuperview:self.scrollView];
  self.hostNameLabel.text = @"Host Name";
  self.hostNameLabel.textColor = [Print3RUtilities lightestGreenColor];
  self.webHostTextField = [UITextField newWithSuperview:self.scrollView];
  self.webHostTextField.textColor = [Print3RUtilities lighterGreenColor];
  self.webHostTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
  self.webHostTextField.delegate = self;
  self.webHostTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Hostname/URL" attributes:@{NSForegroundColorAttributeName: [Print3RUtilities lighterGreenColor]}];

  self.apiKeyLabel = [UILabel newWithSuperview:self.scrollView];
  self.apiKeyLabel.text = @"API Key";
  self.apiKeyLabel.textColor = [Print3RUtilities lightestGreenColor];
  self.apiKeyTextField = [UITextField newWithSuperview:self.scrollView];
  self.apiKeyTextField.textColor = [Print3RUtilities lighterGreenColor];
  self.apiKeyTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
  self.apiKeyTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"API Key" attributes:@{NSForegroundColorAttributeName: [Print3RUtilities lighterGreenColor]}];
  self.apiKeyTextField.delegate = self;

  self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
  [self.scrollView addSubview:self.activityIndicator];
  
  UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTouchOutside)];
  [self.scrollView addGestureRecognizer:recognizer];
  
  self.octoprintButton = [UIButton newWithSuperview:self.scrollView];
  [self.octoprintButton setImage:[UIImage imageNamed:@"octoprint"] forState:UIControlStateNormal];
  self.octoprintButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
  [self.octoprintButton addTarget:self action:@selector(showOctoPrint) forControlEvents:UIControlEventTouchUpInside];
  self.minimalNotification = [JFMinimalNotification notificationWithStyle:JFMinimalNotificationStyleDefault title:kVerifying subTitle:@"" dismissalDelay:0.0 touchHandler:^{
    if([self.minimalNotification.titleLabel.text isEqualToString:kCouldNotVerify]) {
      [self savePrinter];
    }
  }];
  [self.view addSubview:self.minimalNotification];
  self.minimalNotification.delegate = self;
  
  if(self.printer) {
    if (self.printer.image)  {
      self.imageView.image = [UIImage imageWithData:self.printer.image];
      self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    self.nameTextField.text = self.printer.nickName;
    self.webHostTextField.text = self.printer.hostURL;
    self.apiKeyTextField.text = self.printer.apiKey;
    
    self.printerBedSettingsLabel = [UILabel newWithSuperview:self.scrollView];
    self.printerBedSettingsLabel.text = @"Printer Bed Settings";
    self.printerBedSettingsLabel.textColor = [Print3RUtilities lightestGreenColor];
    
    self.lengthTextField = [UITextField newWithSuperview:self.scrollView];
    self.lengthTextField.textColor = [Print3RUtilities lighterGreenColor];
    self.lengthTextField.keyboardType = UIKeyboardTypeDecimalPad;
    self.lengthTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Length" attributes:@{NSForegroundColorAttributeName: [Print3RUtilities lighterGreenColor]}];
    self.lengthTextField.delegate = self;
    self.lengthTextField.text = [self.printer.length stringValue];
    
    self.breadthTextField = [UITextField newWithSuperview:self.scrollView];
    self.breadthTextField.textColor = [Print3RUtilities lighterGreenColor];
    self.breadthTextField.keyboardType = UIKeyboardTypeDecimalPad;
    self.breadthTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Breadth" attributes:@{NSForegroundColorAttributeName: [Print3RUtilities lighterGreenColor]}];
    self.breadthTextField.delegate = self;
    self.breadthTextField.text = [self.printer.breadth stringValue];
  }
  
}

- (void)curveAllTextFields {
  
  NSMutableArray *textFields = [NSMutableArray arrayWithArray:@[self.nameTextField, self.apiKeyTextField, self.webHostTextField]];
  UIView *lineContainer = [[UIView alloc] initWithFrame:CGRectZero];
  lineContainer.backgroundColor = [Print3RUtilities darkGreenColor];
  lineContainer.height = kTextFielHeight;
  lineContainer.width = self.view.width - 2 * kMargin;
  UIView *lineView = [[UIView alloc] initWithFrame:CGRectZero];
  lineView.backgroundColor = [Print3RUtilities lightestGreenColor];
  lineView.height = 1.0f;
  // max possible width
  lineView.width = self.view.width - kMargin * 2;
  lineView.y = lineContainer.maxY - 1;
  [lineContainer addSubview:lineView];
  UIImage *image = [self imageWithView:lineContainer];
  if(self.printer) {
    [textFields addObject:self.breadthTextField];
    [textFields addObject:self.lengthTextField];
  }
  for(UITextField *field in textFields) {
    //field.textAlignment = NSTextAlignmentCenter;
    field.background = image;
  }
  
}

- (UIImage *) imageWithView:(UIView *)view {
  UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
  [view.layer renderInContext:UIGraphicsGetCurrentContext()];
  
  UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
  
  UIGraphicsEndImageContext();
  
  return img;
}

- (BOOL)shouldAutorotate {
  return NO;
}

- (void)didTouchOutside {
  if(self.activeField) {
    [self.activeField resignFirstResponder];
  }
  [self.minimalNotification dismiss];
}
- (void)viewWillLayoutSubviews {
  
  self.scrollView.frame = self.view.frame;

  self.imageView.width = self.view.width;
  self.imageView.height = kImageDimension;
  
  self.nameTextField.width = self.scrollView.width - 2 * kMargin;
  self.nameTextField.height = kTextFielHeight;
  [self.nameTextField moveBelowSiblingView:self.imageView margin:kMargin];
  self.nameTextField.x = kMargin;
  
  [self.hostNameLabel sizeToFit];
  self.hostNameLabel.x = kMargin;
  [self.hostNameLabel moveBelowSiblingView:self.nameTextField margin:kMargin];

  self.octoprintButton.width = 25;
  self.octoprintButton.height = 25;
  [self.octoprintButton moveToRightOfSiblingView:self.hostNameLabel margin:kMargin/2];
  [self.octoprintButton alignVerticallyWithSiblingView:self.hostNameLabel];
  
  self.webHostTextField.width = self.scrollView.width - 2 * kMargin;
  self.webHostTextField.height = kTextFielHeight;
  [self.webHostTextField moveBelowSiblingView:self.hostNameLabel margin:kMargin/2];
  self.webHostTextField.x = kMargin;
  
  [self.apiKeyLabel sizeToFit];
  self.apiKeyLabel.x = kMargin;
  [self.apiKeyLabel moveBelowSiblingView:self.webHostTextField margin:kMargin];
  
  self.apiKeyTextField.width = self.scrollView.width - 2 * kMargin;
  self.apiKeyTextField.height = kTextFielHeight;
  [self.apiKeyTextField moveBelowSiblingView:self.apiKeyLabel margin:kMargin/2];
  self.apiKeyTextField.x = kMargin;
  
  [self.printerBedSettingsLabel sizeToFit];
  self.printerBedSettingsLabel.x = kMargin;
  [self.printerBedSettingsLabel moveBelowSiblingView:self.apiKeyTextField margin:kMargin];
  
  self.lengthTextField.width = self.scrollView.width/2 - 2 * kMargin;
  self.lengthTextField.height = kTextFielHeight;
  [self.lengthTextField moveBelowSiblingView:self.printerBedSettingsLabel margin:kMargin];
  self.lengthTextField.x = kMargin;
  
  self.breadthTextField.width = self.scrollView.width/2 - 2 * kMargin;
  self.breadthTextField.height = kTextFielHeight;
  [self.breadthTextField moveBelowSiblingView:self.printerBedSettingsLabel margin:kMargin];
  self.breadthTextField.x = self.lengthTextField.maxX + kMargin;
  
  [self.activityIndicator moveToCenterOfView:self.scrollView];
  [self.activityIndicator moveBelowSiblingView:self.apiKeyTextField margin:15.0];
  
}

- (void)viewDidLayoutSubviews {
  [super viewDidLayoutSubviews];
  self.scrollView.contentSize = CGSizeMake(self.view.width, self.breadthTextField ? self.breadthTextField.maxY : self.apiKeyTextField.maxY + kMargin * 2);
}

- (void)showOctoPrint {

  WebViewController *vc = [[WebViewController alloc] initWithURL:self.webHostTextField.text];
  UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
  [self presentViewController:nav animated:YES completion:nil];
}

- (void)registerForKeyboardNotifications
{
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(keyboardWasShown:)
                                               name:UIKeyboardDidShowNotification object:nil];
  
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(keyboardWillBeHidden:)
                                               name:UIKeyboardWillHideNotification object:nil];
  
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
  NSDictionary* info = [aNotification userInfo];
  CGRect keyPadFrame=[[UIApplication sharedApplication].keyWindow convertRect:[[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue] fromView:self.view];
  CGSize kbSize =keyPadFrame.size;
  CGRect activeRect=[self.view convertRect:self.activeField.frame fromView:self.activeField.superview];
  CGRect aRect = self.view.bounds;
  aRect.size.height -= (kbSize.height);
  
  CGPoint origin =  activeRect.origin;
  origin.y -= self.scrollView.contentOffset.y;
  if (!CGRectContainsPoint(aRect, origin)) {
    CGPoint scrollPoint = CGPointMake(0.0,CGRectGetMaxY(activeRect)-(aRect.size.height) + 20);
    [self.scrollView setContentOffset:scrollPoint animated:YES];
  }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
  UIEdgeInsets contentInsets = UIEdgeInsetsZero;
  self.scrollView.contentOffset = CGPointZero;
  self.scrollView.scrollIndicatorInsets = contentInsets;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
  if([textField isEqual:self.webHostTextField]) {
    textField.rightView = nil;
  }
  self.activeField = textField;
  [self.minimalNotification dismiss];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
  
  [textField resignFirstResponder];
  return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
  self.activeField = nil;
}

- (void)donePressed {
  if(self.activeField) {
    [self.activeField resignFirstResponder];
  }
  NSString *host = self.webHostTextField.text;
  NSString *apiKey = self.apiKeyTextField.text;
  if([host isEqualToString:@""] || [apiKey isEqualToString:@""]) {
    [self.minimalNotification setTitle:kHostApiCannotBeBlank withSubTitle:@""];
    [self.minimalNotification setStyle:JFMinimalNotificationStyleInfo animated:YES];
    [self.minimalNotification show];
    return;
  }
  [self.minimalNotification show];
  
  // verify printer connection and complete adding
  Printer *testPrinter = [[Printer alloc] initWithData:@{kHostKey:host, kApiKey:apiKey}];
  __weak typeof(self) weakSelf = self;
  [testPrinter getVersionInfoWithCompletion:^(NSDictionary *version) {
    __strong typeof(self) strongSelf = weakSelf;
    if(version) {
      strongSelf.printerVersion = [[PrinterVersion alloc] initWithData:version];
      if(self.printerVersion) {
        NSMutableDictionary *params = [NSMutableDictionary new];
        [params setObject:self.printerVersion.apiVersion ? : @"" forKey:@"api"];
        [params setObject:self.printerVersion.serverVersion ? : @"" forKey:@"server"];
        [params setObject:self.webHostTextField.text ? : @"" forKey:@"host"];
        [[AnalyticsManager sharedManager] logEvent:AnalyticEventTypeAddPrinter withParams:params];
      }
      [self.minimalNotification setTitle:kVerified withSubTitle:@""];
      [self.minimalNotification setStyle:JFMinimalNotificationStyleSuccess animated:YES];
      [self savePrinter];
      
    }
    else {
      [self.minimalNotification setTitle:kCouldNotVerify withSubTitle:@"Tap to continue"];
      [self.minimalNotification setStyle:JFMinimalNotificationStyleError animated:YES];
      
    }
  }];
  
}

- (void)savePrinter {
  
  NSString *host = self.webHostTextField.text;
  NSString *nickName = self.nameTextField.text;
  NSString *apiKey = self.apiKeyTextField.text;
  
  if([host isEqualToString:@""] || [apiKey isEqualToString:@""]) {
    [self.minimalNotification setTitle:kHostApiCannotBeBlank withSubTitle:@""];
    [self.minimalNotification setStyle:JFMinimalNotificationStyleInfo animated:YES];
    [self.minimalNotification show];
    return;
  }
  
  NSData * jpegData;
  if(self.printerImage) {
    jpegData = UIImageJPEGRepresentation(self.printerImage, 0.8);
  }
  
  NSMutableDictionary *printerDict = [@{kHostKey:host,
                                        kNickNameKey:nickName,
                                        kApiKey:apiKey,
                                        } mutableCopy];
  
  if(jpegData) {
    [printerDict setObject:jpegData forKey:kImageKey];
  }
  NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
  f.numberStyle = NSNumberFormatterDecimalStyle;
  
  if(self.lengthTextField.text) {
    [printerDict setObject:[f numberFromString:self.lengthTextField.text] ? : @(0) forKey:kLengthKey];
  }
  if(self.breadthTextField.text) {
    [printerDict setObject:[f numberFromString:self.breadthTextField.text] ? : @(0) forKey:kBreadthKey];
  }
  
  Printer *printer = [[Printer alloc] initWithData:printerDict];
  // check if printer already exists
  if(!self.printer && [[DiskManager sharedManager] doesPrinterExist:printer]) {
    [self.minimalNotification setTitle:kPrinterAlreadyExists withSubTitle:@""];
    [self.minimalNotification setStyle:JFMinimalNotificationStyleError animated:YES];
    [self.minimalNotification show];
    return;
  }
  
  if(printer && [self.addPrintViewDelegate respondsToSelector:@selector(savePrinter:)]) {
    [self.addPrintViewDelegate savePrinter:printer];
  }
  [self performSelector:@selector(hideNotification) withObject:nil afterDelay:1.0f];
  
}

- (void)chooseImageClicked {
  UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose an image from" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles: nil];
  actionSheet.delegate = self;
  self.cameraPhotoIndex = self.galleryPhotoIndex = -1;
  if([[ImagePickerManager sharedManager] isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
    self.cameraPhotoIndex = [actionSheet addButtonWithTitle:@"Camera"];
  }
  if([[ImagePickerManager sharedManager] isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
    self.galleryPhotoIndex = [actionSheet addButtonWithTitle:@"Gallery"];
  }
  [actionSheet setCancelButtonIndex:[actionSheet addButtonWithTitle:@"Cancel"]];
  
  if(self.cameraPhotoIndex != -1 || self.galleryPhotoIndex != -1) {
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [actionSheet showInView:self.view];
  }
}

- (void)didDismissNotification:(JFMinimalNotification *)notification {
  [self.minimalNotification setTitle:kVerifying withSubTitle:@""];
  [self.minimalNotification setStyle:JFMinimalNotificationStyleDefault animated:YES];
}

#pragma mark UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
  
  if(buttonIndex == -1 || buttonIndex == [actionSheet cancelButtonIndex]) {
    return;
  }
  
  UIImagePickerController *picker;
  
  if(buttonIndex == self.cameraPhotoIndex) {
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if( authStatus == AVAuthorizationStatusDenied || authStatus == AVAuthorizationStatusRestricted) {
      [Print3RUtilities showAlertWithTitle:kErrorTitle message:@"You need to grant access to the camera" sender:self];
      return;
    }
    picker = [[ImagePickerManager sharedManager] pickerforSourceType:UIImagePickerControllerSourceTypeCamera];
  }
  else if(buttonIndex == self.galleryPhotoIndex) {
    picker = [[ImagePickerManager sharedManager] pickerforSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
  }
  picker.delegate = self;
  [self presentViewController:picker animated:YES completion:nil];
  
}

#pragma mark UIImagePickerControllerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
  
  NSString *mediaType = info[UIImagePickerControllerMediaType];
  UIImage *image = nil;
  
  if (CFStringCompare((__bridge CFStringRef)mediaType, kUTTypeImage, 0) == kCFCompareEqualTo) {
    image = (UIImage *)info[UIImagePickerControllerEditedImage];
    if (!image)
      image = (UIImage *)info[UIImagePickerControllerOriginalImage];
  }
  self.imageView.image = image;
  self.imageView.contentMode = UIViewContentModeScaleAspectFill;
  self.printerImage = image;
  [self dismissViewControllerAnimated:YES completion:nil];
  
}

- (void)cancelPressed {
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)hideNotification {
  [self.minimalNotification dismiss];
  [self dismissViewControllerAnimated:YES completion:nil];
  
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}


@end
