//
//  AboutUsViewController.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 6/14/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "AboutUsViewController.h"
#import "Print3RUtilities.h"

@interface AboutUsViewController ()
@property (nonatomic, strong)UIImageView *backgroundImage;
@end

@implementation AboutUsViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  self.backgroundImage = [UIImageView new];
  self.backgroundImage.image =[UIImage imageNamed:@"bkg"];
  [self.view addSubview:self.backgroundImage];
  [self.view sendSubviewToBack:self.backgroundImage];
  
  
}

- (void)viewWillLayoutSubviews {
  [super viewWillLayoutSubviews];
  self.backgroundImage.frame = self.view.frame;
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

@end
