//
//  GCodeParser.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/26/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "GCodeParser.h"

static NSString * const gcodeParserStartKey = @";LAYER:0";
static NSString * const gcodeParserEndKey = @";-- END GCODE --";


@implementation GCodeParser

+ (NSArray *)parseGCodeFromStringArray:(NSArray *)contentArray {
  
  if(contentArray.count < 1)
  {
    return nil;
  }
  
  __block NSUInteger startGCode, endGCode = 0;
  [contentArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
    if([(NSString *)obj isEqualToString:gcodeParserStartKey]) {
      startGCode = idx;
    }
    if([(NSString *)obj isEqualToString:gcodeParserEndKey]) {
      endGCode = idx;
    }
    if( startGCode > 0 && endGCode > 0) {
      *stop = YES;
    }
  }];
  
  // if startGcode and endGcode were same or individually 0, nothing to process, exit
  if((startGCode == endGCode) || startGCode == 0 || endGCode == 0) {
    return nil;
  }
  
  NSMutableArray *matrixArray = [NSMutableArray new];
  float tempX = 0;
  float tempY = 0;
  float tempZ = 0;
  float tempE = 0;
  
  NSArray *trimmedArray = [contentArray subarrayWithRange:NSMakeRange(startGCode, endGCode - startGCode + 1)];
  for(NSString *element in trimmedArray) {
    if([[element substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"G"]) {
      NSArray *elementStringsArray = [element componentsSeparatedByString:@" "];
      for(NSString *subElement in elementStringsArray) {
        if([[subElement substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"X"]) {
          tempX = [[subElement substringFromIndex:1] floatValue];
        }
        if([[subElement substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"Y"]) {
          tempY = [[subElement substringFromIndex:1] floatValue];

        }
        if([[subElement substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"Z"]) {
          tempZ = [[subElement substringFromIndex:1] floatValue];

        }
        if([[subElement substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"E"]) {
          tempE = [[subElement substringFromIndex:1] floatValue];

        }
      }
      [matrixArray addObject:@[@(tempX),@(tempY),@(tempZ),@(tempE)]];
    }
  
  }

  return matrixArray;
}

+ (NSString *)createGCodeFromPoints:(NSArray *)points {

  if(points.count < 1) {
    return @"";
  }
  
  float maxX = 0;
  float maxY = 0;
  
  for(NSArray *point in points) {
    if(maxX < [[point objectAtIndex:0] floatValue]) {
      maxX = [[point objectAtIndex:0] floatValue];
    }
    
    if(maxY < [[point objectAtIndex:1] floatValue]) {
      maxY = [[point objectAtIndex:1] floatValue];
    }
  }
  
  // Build plate is 300:300
  // We want to normalize data points using 200, so image is centered
  float xRatio = 200/maxX;
  float yRatio = 200/maxY;
  NSMutableString *gCodeFileString = [NSMutableString new];
  [gCodeFileString appendString:[NSString stringWithFormat:@"G28 X Y\n"]];

  for (NSArray *point in points){
    float x = [[point objectAtIndex:0] floatValue] * xRatio;
    float y = [[point objectAtIndex:1] floatValue] * yRatio;
    [gCodeFileString appendString:[NSString stringWithFormat:@"G0 X%f Y%f F6000\n",roundf(x),roundf(y)]];
    //[gCodeFileString appendString:[NSString stringWithFormat:@"%f %f\n",roundf(x),roundf(y)]];

  }
  
  return gCodeFileString;
}
@end
