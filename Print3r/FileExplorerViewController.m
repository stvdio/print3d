//
//  FileExplorerViewController.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 3/14/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//


#import "FileExplorerViewController.h"
#import "FileExplorerCell.h"
#import "SWTableViewCell.h"
#import "UITableViewCell+Additions.h"
#import "AMPopTip.h"
#import "UIView+Layout.h"
#import "NetworkManager.h"
#import "FileViewerViewController.h"
#import "Print3RUtilities.h"
#import "LocalNotificationManager.h"
#import "UITableView+Additions.h"
#import "CNPPopupController.h"
#import "PrintStartingViewController.h"
#import "JGProgressHUD.h"
#import "MailComposerManager.h"
#import "JFMinimalNotification.h"

static NSString *const kErrorMessage = @"The files info could not be loaded";
static NSString *const kNoFilesMessage = @"You do not have any files on this printer \ue058\n";
static NSString *const kDeleteAlert = @"Are you sure you want to delete %@";
static CGFloat const kHeaderHeight = 60.0f;


@interface FileExplorerViewController () <SWTableViewCellDelegate, UIAlertViewDelegate, PrintStartingProtocol, MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) Printer *printer;
@property (nonatomic, strong) NSMutableArray *files;
@property (nonatomic, weak) SWTableViewCell *currentActiveCell;
@property (nonatomic, assign) BOOL didTryReachingServer;
@property (nonatomic, strong) CNPPopupController *popup;
@property (nonatomic, strong)JGProgressHUD *progressHud;
@property (nonatomic, assign) BOOL shouldShowSuggestion;

@end

@implementation FileExplorerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  self.tableView.backgroundColor = [Print3RUtilities darkGreenColor];
  [self.navigationController setNavigationBarHidden:NO];
  self.navigationController.navigationBar.topItem.title = @"";
  CNPPopupButtonItem *cancelButton = [CNPPopupButtonItem defaultButtonItemWithTitle:[[NSAttributedString alloc] initWithString:@"Close"] backgroundColor:[Print3RUtilities defaultThemeColor]];
  self.popup = [[CNPPopupController alloc] initWithTitle:nil contents:nil buttonItems:nil destructiveButtonItem:cancelButton];
  self.popup.theme = [CNPPopupTheme defaultTheme];
  
  self.didTryReachingServer = NO;
  self.progressHud = [JGProgressHUD progressHUDWithStyle:JGProgressHUDStyleDark];
  self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
  [self getFiles];
  [self.tableView setSeparatorColor:[Print3RUtilities lighterGreenColor]];
  if([self.tableView respondsToSelector:@selector(setCellLayoutMarginsFollowReadableWidth:)])
  {
    self.tableView.cellLayoutMarginsFollowReadableWidth = NO;
  }
  
  self.shouldShowSuggestion = ![Print3RUtilities hasSeenSlideForOptions];
}

- (void)getFiles {
  [self.progressHud showInView:self.view animated:YES];
  __weak FileExplorerViewController *weakself = self;
  [self.printer getFilesWithLocation:@"local" completion:^(NSArray *files) {
    __strong FileExplorerViewController *strongSelf = weakself;
    strongSelf.didTryReachingServer = YES;
    [strongSelf.progressHud dismissAnimated:YES];
    if(!files) {
      [Print3RUtilities showAlertWithTitle:kErrorTitle message:kErrorMessage sender:self];
    }
    else{
      strongSelf.files = [files mutableCopy];
      [strongSelf.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
  }];

}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [self setTitle:@"File Explorer"];
  self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
}

- (void)viewWillLayoutSubviews {
  [super viewWillLayoutSubviews];
}

- (void)viewDidLayoutSubviews {
  [super viewDidLayoutSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
  [Print3RUtilities setHasSeenSlideForOptions];
}

- (instancetype)initWithPrinter:(Printer *)printer {
  self = [super init];
  
  if(self) {
    self.printer = printer;
  }
  return self;
}

- (void)sectionHeaderTapped {
  self.shouldShowSuggestion = NO;
  [Print3RUtilities setHasSeenSlideForOptions];
  [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.files.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
  if(section == 0) {
    if((self.files.count < 1 && self.didTryReachingServer) || self.shouldShowSuggestion) {
      return kHeaderHeight;
    }
  }
  return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
  if(section == 0) {
    if(self.files.count < 1 && self.didTryReachingServer) {
      UIView *headerView = [tableView headerViewForTableView:tableView text:kNoFilesMessage height:kHeaderHeight];
      headerView.backgroundColor = [UIColor grayColor];
      return headerView;
    }
    else if (self.shouldShowSuggestion) {
      UIView *headerView = [tableView headerViewForTableView:tableView text:@"You can slide the rows to see more options. Tap to dismiss" height:kHeaderHeight];
      headerView.backgroundColor = [UIColor grayColor];
      UIButton *hiddenButton = [UIButton new];
      [headerView addSubview:hiddenButton];
      hiddenButton.height = headerView.height;
      hiddenButton.width = headerView.width;
      hiddenButton.backgroundColor = [UIColor clearColor];
      [hiddenButton addTarget:self action:@selector(sectionHeaderTapped) forControlEvents:UIControlEventTouchUpInside];
      return headerView;
    }
  }
  return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  return 60.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

  SWTableViewCell *cell = (SWTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"fileCell"];
  
  if (cell == nil) {
    cell = [[SWTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"fileCell"];
    cell.leftUtilityButtons = [cell leftButtonsForSwipeableCell];
    cell.rightUtilityButtons = [cell leftButtonsForSwipeableCell];
    cell.delegate = self;
  }
  cell.backgroundColor = [Print3RUtilities darkGreenColor];
  PrinterFile *file = [self.files objectAtIndex:indexPath.row];
  cell.textLabel.text = [file.name stringByDeletingPathExtension];
  cell.textLabel.textColor = [UIColor whiteColor];
  [cell setNeedsLayout];
  return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  [tableView deselectRowAtIndexPath:indexPath animated:YES];

  PrinterFile *file = [self.files objectAtIndex:indexPath.row];
  PrintStartingViewController *vc = [PrintStartingViewController new];
  [self addChildViewController:vc];
  [self.view addSubview:vc.view];
  vc.view.frame = self.view.frame;
  vc.view.y = self.tableView.contentOffset.y;
  [vc didMoveToParentViewController:self];
  [vc showForFileName:file];
  vc.delegate = self;
}

#pragma SWTableViewDelegate

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell {
  return YES; 
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didScroll:(UIScrollView *)scrollView {
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index{
  [self cell:cell didTriggerWithIndex:index];
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index {
  [self cell:cell didTriggerWithIndex:index];
}

- (void)cell:(SWTableViewCell *)cell didTriggerWithIndex:(NSInteger)index{
  self.currentActiveCell = cell;
  NSInteger cellIndex = [self.tableView indexPathForCell:cell].row;
  PrinterFile *file = [self.files objectAtIndex:cellIndex];
  // 0 share
  if(index == 0) {
    if(![[MailComposerManager sharedManager] canSendEmail]) {
      // alert
      [Print3RUtilities showAlertWithTitle:kErrorTitle message:@"Device not capable of sending emails" sender:self];
      return;
    }
    __weak typeof(self) weakSelf = self;
    [self.progressHud showInView:self.view animated:YES];
    if(!file.downloadLocation) {
      [Print3RUtilities showAlertWithTitle:kErrorTitle message:@"File could not be downloaded for sharing" sender:self];
      [self.progressHud dismissAnimated:YES];
    }
    [[NetworkManager sharedManager] downloadFileFor:file.downloadLocation params:nil success:^(id responseObject) {
      __strong typeof(self) strongSelf = weakSelf;
      NSString *fileString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
      NSData *attachment = [fileString dataUsingEncoding:NSUTF8StringEncoding];
      [strongSelf.progressHud dismissAnimated:YES];
      MFMailComposeViewController *mailComposer = [[MailComposerManager sharedManager] mailComposerControllerForSubject:@"I have shared a GCode with you" body:@"Shared Via Thirmensio" attachment:attachment attachmentName:[NSString stringWithFormat:@"%@",file.name] mimeType:@"text/rtf"];
      mailComposer.mailComposeDelegate = self;
      [strongSelf presentViewController:mailComposer animated:YES completion:nil];
    } failure:^(NSError *error, AFHTTPRequestOperation *operation) {
      __strong typeof(self) strongSelf = weakSelf;
      [strongSelf.progressHud dismissAnimated:YES];
      [Print3RUtilities showAlertWithTitle:kErrorTitle message:@"File could not be downloaded for sharing" sender:strongSelf];
    }];
    
  }
  // 1 info
  if(index == 1) {
    
    self.popup.contents = @[[file descriptionForPopUp]] ;
    [self.popup presentPopupControllerAnimated:YES];
  }
  // 2 view file
  if(index == 2) {
    FileViewerViewController *vc = [[FileViewerViewController alloc] initWithPrinter:self.printer File:file];
    [self.navigationController pushViewController:vc animated:YES];
  }
  // 3 Delete
  if(index == 3) {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Wait!" message:[NSString stringWithFormat:kDeleteAlert, [file.name stringByDeletingPathExtension]] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
    [alertView show];
  }

}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
  [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma AlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  if(buttonIndex == 1) {
    if(self.currentActiveCell) {
      NSInteger cellIndex = [self.tableView indexPathForCell:self.currentActiveCell].row;
      PrinterFile *file = [self.files objectAtIndex:cellIndex];
      __weak FileExplorerViewController *weakself = self;
      [self.printer deleteFile:file success:^{
        __strong FileExplorerViewController *strongSelf = weakself;
        [strongSelf.files removeObjectAtIndex:cellIndex];
        [strongSelf.tableView beginUpdates];
        [strongSelf.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:cellIndex inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
        [strongSelf.tableView endUpdates];
      } failure:^{
        
      }];
    }
  }
}


#pragma mark printStartingProtocol 
- (void)printFile:(PrinterFile *)file {
  [self.printer printFile:file completion:^{
    [[LocalNotificationManager sharedManager] setLocalNotificationForFile:file];
  }];
}

@end
