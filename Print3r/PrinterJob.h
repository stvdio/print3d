//
//  PrinterJob.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/19/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PrinterFile.h"
#import "Filament.h"

@interface PrinterJob : NSObject
@property(nonatomic, strong) PrinterFile *file;
@property(nonatomic, strong) Filament *filament;
@property(nonatomic, strong) NSNumber *estimatedPrintTime;

- (PrinterJob *)initWithData:(NSDictionary *)data;
- (instancetype)initDemoPrinterJob;
- (NSString *)humanReadableEstimatedPrintTime;
@end
