//
//  FileViewerViewController.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 3/15/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractPrinterViewController.h"
#import "PrinterFile.h"

@interface FileViewerViewController : AbstractPrinterViewController

- (instancetype)initWithPrinter:(Printer *)printer File:(PrinterFile *)file;

@end
