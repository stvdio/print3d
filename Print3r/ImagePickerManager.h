//
//  ImagePickerManager.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 3/29/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ImagePickerManager : NSObject

+ (instancetype)sharedManager;

/*
 * Returns BOOL value indicating weather a particular sourcetype is available or not - Camera/Photo Library
 * @param UIImagePickerControllerSourceType sourceType
 * 
 * @return BOOL YES if the param source type exists for current device
 */
- (BOOL)isSourceTypeAvailable:(UIImagePickerControllerSourceType)sourceType;

/*
 * Creates an instance of UIImagePickerController for requested source type
 * NOTE - Make sure to set the caller as the delegate
 * 
 * @param UIImagePickerControllerSourceType sourceType
 * 
 * @return UIImagePickerController *
 */
- (UIImagePickerController *)pickerforSourceType:(UIImagePickerControllerSourceType)sourceType;
@end
