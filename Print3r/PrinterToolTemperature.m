//
//  PrinterStateTemperature.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 4/12/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "PrinterToolTemperature.h"

@implementation PrinterToolTemperature

- (instancetype)initWithData:(NSDictionary *)data {
  
  self = [super init];
  if(self) {
    if(data[@"actual"]) {
      _actual = [data[@"actual"] stringValue];
    }
    if(data[@"target"]) {
      _target = data[@"target"];
    }
    if(data[@"offset"]) {
      _offset = data[@"offset"];
    }
  }
  
  return self;
}

- (instancetype) initDemoToolTemperature {
  self = [super init];
  if (self) {
    _actual = @"120";
  }
  return self;
}
@end
