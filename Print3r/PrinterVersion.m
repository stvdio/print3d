//
//  PrinterVersion.m
//  Print3D
//
//  Created by Vaibhav Aggarwal on 4/12/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import "PrinterVersion.h"

@implementation PrinterVersion

- (instancetype)initWithData:(NSDictionary *)data {
  
  self = [super init];
  if(self) {
    if(data[@"api"]) {
      _apiVersion = data[@"api"];
    }
    if(data[@"server"]) {
      _serverVersion = data[@"server"];
    }
  }
  
  return self;
}
@end
