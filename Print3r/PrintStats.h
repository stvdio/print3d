//
//  PrintStats.h
//  Print3D
//
//  Created by Vaibhav Aggarwal on 2/16/15.
//  Copyright (c) 2015 Vaibhav Aggarwal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PrintStats : NSObject
@property(nonatomic, strong)NSNumber *failure;
@property(nonatomic, strong)NSNumber *success;
@property(nonatomic, strong)NSNumber *lastRanDate;
@property(nonatomic, assign)BOOL lastRanStatus;

- (PrintStats *)initWithData:(NSDictionary *)data;

@end
